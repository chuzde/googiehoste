<?php
if(isset($_POST['inputEmail'])) {

	// CHANGE THE TWO LINES BELOW
	$email_to = "sales@youstable.com";

	$email_subject = "GoogieHost Online Infomation Desk";


	function died($error) {
		// your error code can go here
		echo "We're sorry, but there's errors found with the form you submitted.<br /><br />";
		echo $error."<br /><br />";
		echo "Please mail your query at .<br /><br />";
		die();
	}

	// validation expected data exists
	if(!isset($_POST['inputName']) ||
		!isset($_POST['inputEmail']) ||
		!isset($_POST['inputPhone']) ||
		!isset($_POST['inputMessage'])) {
		died('We are sorry, but there appears to be a problem with the form you submitted.');
	}

	$name = $_POST['inputName']; // required
	$email_from = $_POST['inputEmail']; // required
	$phone = $_POST['inputPhone']; // required
    $message = $_POST['inputMessage']; // required

	$error_message = "";
	$email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
  if(!preg_match($email_exp,$email_from)) {
  	$error_message .= 'The Email Address you entered does not appear to be valid.<br />';
  }
	$string_exp = "/^[A-Za-z .'-]+$/";
  if(!preg_match($string_exp,$name)) {
  	$error_message .= 'The First Name you entered does not appear to be valid.<br />';
  }
  if(strlen($message) < 2) {
  	$error_message .= 'Write the description in more than two letters.<br />';
  }
  if(strlen($error_message) > 0) {
  	died($error_message);
  }
	$email_message = "Form details below.\n\n";

	function clean_string($string) {
	  $bad = array("content-type","bcc:","to:","cc:","href");
	  return str_replace($bad,"",$string);
	}

	$email_message .= "Name: ".clean_string($name)."\n";
	$email_message .= "Email: ".clean_string($email_from)."\n";
	$email_message .= "Phone: ".clean_string($phone)."\n";
	$email_message .= "Message: ".clean_string($message)."\n";


// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
if (mail($email_to, $email_subject, $email_message, $headers)) {
    
     echo "<script>
             alert('Thank you for contacting GoogieHost. We will be in touch with you very soon.'); 
            window.history.go(-1);
     </script>";
     
    }
    else {
       echo "<script>
             alert('Failed'); 
            window.history.go(-1);
     </script>";
    }
}
?>

<!-- place your own success html below 

Thank you for contacting GoogieHost. We will be in touch with you very soon.</br>
<b>PLEASE WAIT: You will redirected to Home Page within 3 seconds</b>
<meta http-equiv="refresh" content="3; url=https://googiehost.com/" />
$first_name='';
    $email_from='';
    $comments='';

  -->  


