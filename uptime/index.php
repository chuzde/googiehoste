<?php
include_once("config.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Server Uptime</title>
    <META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>styles.css">
</head>
<body>
<div class="serverStatusContainer">
	<div align="center">
<?php
// width of the table displayed on screen (in pixels)
$tableWidth		= 700;
$firstCellWidth	= 250;
$dataCellWidth 	= number_format(($tableWidth-$firstCellWidth)/2);

echo "<table id='serverStatusTable' cellspacing='0' style='width: ".$tableWidth."px;'>";

// header
echo "<tr>";
echo "<th class='nobg' style='width:".$firstCellWidth."px;'></th>";
echo "<th>Webserver</th><th>Database</th>";

// main table rows
$tracker = 0;
foreach($serverList AS $serverNum=>$server)
{
	$rowClass = "";
	if($tracker == 1)
	{
		$rowClass = "alt";
		$tracker = 0;
	}
	else
	{
		$tracker++;
	}
	
	echo "<tr>";
	echo "<td class='".$rowClass." taLeft'><strong>Server ".$serverNum."</strong></td>";
	
	$uniqueTracker = md5($server.$serverNum.date("YmdHis"));
	echo "<td id=\"tdContainer_".$uniqueTracker."\" class=\"".$rowClass."\" style=\"width:".$dataCellWidth."px; background-image: url('images/loading.gif'); background-repeat: no-repeat; background-position:center;\">";
	echo "<img id='image_".$uniqueTracker."' src='".$url."check_status.php?s=".$server."&r=".$uniqueTracker."' alt='' onLoad=\"document.getElementById('tdContainer_".$uniqueTracker."').style.backgroundImage = ''; document.getElementById('image_".$uniqueTracker."').alt='Status of server ".$serverNum."';\"/>";
	echo "</td>";
	
	if ($serverNum % 2 != 0){
		$dbNum = ($serverNum + 1) / 2;
	} else {
		$dbNum = $serverNum / 2;
	}
	$server = $dbList[$dbNum];
	$uniqueTracker = md5($server.$serverNum.date("YmdHis"));
	echo "<td id=\"tdContainer_".$uniqueTracker."\" class=\"".$rowClass."\" style=\"width:".$dataCellWidth."px; background-image: url('images/loading.gif'); background-repeat: no-repeat; background-position:center;\">";
	echo "<img id='image_".$uniqueTracker."' src='".$url."check_status.php?s=".$server."&r=".$uniqueTracker."' alt='' onLoad=\"document.getElementById('tdContainer_".$uniqueTracker."').style.backgroundImage = ''; document.getElementById('image_".$uniqueTracker."').alt='Status of SQL ".$dbNum."';\"/>";
	echo "</td>";

	echo "</tr>";
}
echo "</table>";

?>

	</div>
</div>
</body>
</html>
