/********* onload action script here *********/
jQuery(document).ready(function(){
	jQuery(document).on("change","input[name='domainradio']",function(){
		var radioVals = jQuery(this).val();
		jQuery("#domainname").val('');
		var depId = jQuery("#dpid").val();
		jQuery("#search-icon-box").attr("searchfor",radioVals);
		jQuery("#tldDropdown").removeClass("hidden");
		jQuery("#tldsearchText").addClass("hidden");
		if(jQuery("#search-icon-box").find("i").hasClass("fa-plus")){
			jQuery("#search-icon-box").find("i").removeClass("fa-plus");
			jQuery("#search-icon-box").find("i").addClass("fa-search");				
		}
		if(radioVals == 'exist'){
			jQuery("#tldDropdown").addClass("hidden");
			jQuery("#tldDropdownSubDomain").addClass("hidden");
			jQuery("#tldsearchText").removeClass("hidden");
			jQuery("#search-icon-box").find("i").removeClass("fa-search");
			jQuery("#search-icon-box").find("i").addClass("fa fa-share");
		}
		if(radioVals == 'subdomain'){
			jQuery("#tldDropdown").addClass("hidden");
			jQuery("#tldDropdownSubDomain").removeClass("hidden");
		}
		jQuery(".domain-result-box").addClass("hidden");
		var domainInSummary = jQuery(".wgs-domain-row-section").length;
		if(radioVals == 'register' && depId != '' && domainInSummary == 0){
			jQuery(".domain-result-box").removeClass("hidden");	
		}
	});
	// version V1.0.8
	jQuery(document).on("change","input[name='billcustomwgsbody']",function(){
		var billingCycleVal = jQuery(this).val();
		var pidGetRadio = jQuery(this).attr("pidvalue");
		var icountGetOrder = jQuery("#wgs-opc-for-i-"+pidGetRadio).attr("icount");
		wgsCallAjaxForConfigureProductBillingCycle(icountGetOrder,billingCycleVal);
	});
	// version 1.0.2
	jQuery(document).on('keypress','#domainname',function (e) {
		 var key = e.which;
		 if(key == 13){
			jQuery('#search-icon-box').click();
			return false;  
		  }
	});
	// version 1.0.2 end
	// version 1.0.3
	jQuery(document).on("change",".wgsQuantityInputBox input[type='number']",function(){
		jQuery("#wgssummaryloaders").removeClass("hidden");
		var prodId = jQuery(this).closest(".accordion-group").attr("datapid");
		var formIdNum = 'form-update-product-quantity'+jQuery(this).attr("inum");
		jQuery.post("cart.php", 'token='+csrfToken+'&ajax=1&a=view&'+jQuery("#"+formIdNum).serialize(),
			function (data) {
			   if (data != '') {
				    cartsummary();
			   } else {
				  cartsummary();
			   }
		});		
	});
	jQuery(document).on("keyup",".wgsQuantityInputBox input[type='number']",function(){
		jQuery("#wgssummaryloaders").removeClass("hidden");
		var prodId = jQuery(this).closest(".accordion-group").attr("datapid");
		var formIdNum = 'form-update-product-quantity'+jQuery(this).attr("inum");
		jQuery.post("cart.php", 'token='+csrfToken+'&ajax=1&a=view&'+jQuery("#"+formIdNum).serialize(),
		   function (data) {
			   if (data != '') {
				   cartsummary();
			   } else {
				   cartsummary();
			   }
		});
	});
	// WhmcsV8.X.X
	jQuery(document).on("keyup",".wgsQuantityInputBoxAddon input[type='number']",function(){
		jQuery("#wgssummaryloaders").removeClass("hidden");
		var prodId = jQuery(this).closest(".accordion-group").attr("datapid");
		var formIdNum = 'form-update-product-quantity-addon'+jQuery(this).attr("inum");
		jQuery.post("cart.php", 'token='+csrfToken+'&ajax=1&a=view&'+jQuery("#"+formIdNum).serialize(),
		   function (data) {
			   if (data != '') {
				   cartsummary();
			   } else {
				   cartsummary();
			   }
		});
	});
	jQuery(document).on("change",".wgsQuantityInputBoxAddon input[type='number']",function(){
		jQuery("#wgssummaryloaders").removeClass("hidden");
		var prodId = jQuery(this).closest(".accordion-group").attr("datapid");
		var formIdNum = 'form-update-product-quantity-addon'+jQuery(this).attr("inum");
		jQuery.post("cart.php", 'token='+csrfToken+'&ajax=1&a=view&'+jQuery("#"+formIdNum).serialize(),
		   function (data) {
			   if (data != '') {
				   cartsummary();
			   } else {
				   cartsummary();
			   }
		});
	});
    // version 1.0.3 end
	jQuery(document).on("click","#btnAlreadyRegistered",function(){
		jQuery("#inputCustType").val('existing');
		jQuery("#btnNewUserSignup").removeClass("hidden");
		jQuery(this).addClass("hidden");
		jQuery(".panel-group.newUser").addClass("hidden");
		jQuery(".panel-group.existingUser").removeClass("hidden");
		jQuery(".panel-group.existingUser").find("a").click();
		jQuery("#collapseeight").removeClass("in");
	});
	jQuery(document).on("click","#btnNewUserSignup",function(){
		jQuery("#inputCustType").val('new');
		jQuery("#btnAlreadyRegistered").removeClass("hidden");
		jQuery(this).addClass("hidden");
		jQuery(".panel-group.existingUser").addClass("hidden");
		jQuery(".panel-group.newUser").removeClass("hidden");
		jQuery(".panel-group.newUser").find("a").click();
		jQuery("#collapseseven").removeClass("in");
		// WhmcsV8.X.X
		jQuery("#containerNewUserSignup").css("display","");
	});
	jQuery(document).on("click",".bttn",function(){
	  jQuery(".oredr-summary,.right-side").toggleClass("main");
	  jQuery('body').toggleClass('wgsoverlay');
	});
	jQuery(document).on("click",".clsResp",function(){
	  jQuery(".oredr-summary,.right-side").toggleClass("main");
	  jQuery('body').toggleClass('wgsoverlay');
	});
	jQuery(document).on("click","p.hosting-wgs",function(){
		jQuery('html,body').animate({
				scrollTop: jQuery(".choose-plan-wrapper").offset().top},
				'slow');
	});
	jQuery(document).on("change","#inputDomainContact",function(){
		var getValue = jQuery(this).val();
		if(getValue == 'addingnew'){
			jQuery('#domainRegistrantInputFields').removeClass("hidden");
		}else{
			jQuery('#domainRegistrantInputFields').addClass("hidden");
		}
	});
	jQuery(document).on("keyup","#wgs-customs-fields input[type='text'], #wgs-customs-fields input[type='password']",function(){
       wgsCallAjaxForConfigureProduct();
	});
	jQuery(document).on("change","#wgs-customs-fields select",function(){
		wgsCallAjaxForConfigureProduct();
	});
	jQuery(document).on("click","#wgs-customs-fields input[type='radio'],#wgs-customs-fields input[type='checkbox']",function(){
		wgsCallAjaxForConfigureProduct();
	});
	jQuery(document).on("click",".wgs-domain-custom-field input[type='radio'],.wgs-domain-custom-field input[type='checkbox']",function(){	
		wgsConfigDomainOption();
	});
	jQuery(document).on("change",".wgs-domain-custom-field select",function(){
		wgsConfigDomainOption();
	});
	jQuery(document).on("keyup",".wgs-domain-custom-field input[type='text'], .wgs-domain-custom-field input[type='password']",function(){		
		wgsConfigDomainOption();
	});
	//V1.0.5
	jQuery(document).on("click",".wgs-opc-dns-email-idp input[type='checkbox']",function(){	
		jQuery(this).parent().parent().toggleClass("active-image-opc");
	});
	jQuery(document).on("click",".wgs-opc-link-container",function(){
		var getClickDivOuterHeight = jQuery(this).next(".wgs-opc-li-bill-container").outerHeight();
		var mainPlanWraperHeight = jQuery(".choose-pla-wrapper").outerHeight()-295;
		if(!jQuery(this).hasClass("wgs-opc-link-container-active")){
			jQuery(".wgs-opc-link-container").next(".wgs-opc-li-bill-container").css("display","none");
			jQuery(".wgs-opc-link-container").removeClass("wgs-opc-link-container-active");	
			jQuery(this).addClass("wgs-opc-link-container-active");
			jQuery(this).next(".wgs-opc-li-bill-container").css("display","block");
			if(getClickDivOuterHeight > mainPlanWraperHeight){
				var marginBottom = parseInt(getClickDivOuterHeight)-parseInt(mainPlanWraperHeight);
				jQuery(".slick-track").css("margin-bottom",marginBottom+"px");
			}
		}else{
			jQuery(".wgs-opc-link-container").next(".wgs-opc-li-bill-container").css("display","none");
			jQuery(".wgs-opc-link-container").removeClass("wgs-opc-link-container-active");
			jQuery(".slick-track").css("margin-bottom","0px");			
		}
	});
	jQuery(document).on("click",".wgs-opc-bill-cycle-row",function(){
		var cycleValueGet = jQuery(this).attr("wgs-data-billcycle");
		var saveTextShow = jQuery(this).find(".wgs-save-price").text();
		var billCycleTextShow = jQuery(this).find(".wgs-billing-duration").text();
		var strikePriceTextShow = jQuery(this).find(".wgs-price-striked").find("div").text();
		var strikePriceCycleShow = jQuery(this).find(".wgs-price-striked").find("span").text();
		var finalPriceTextShow = jQuery(this).find(".wgs-price-cycle-final").find("div").text();
		var finalPriceCycleTextShow = jQuery(this).find(".wgs-price-cycle-final").find("span").text();
		jQuery(".wgs-opc-link-container-active").find(".wgs-opc-save-wraper").text(saveTextShow);
		jQuery(".wgs-opc-link-container-active").find(".wgs-opc-price-duration").text(billCycleTextShow);
		jQuery(".wgs-opc-link-container-active").find(".wgs-opc-price-kl-strike").text(strikePriceTextShow);
		jQuery(".wgs-opc-link-container-active").find(".wgs-opc-price-strike").find("span").text(strikePriceCycleShow);
		if(strikePriceTextShow == ''){
			jQuery(".wgs-opc-link-container-active").find(".wgs-opc-price-strike").css("display","none");
		}else{
			jQuery(".wgs-opc-link-container-active").find(".wgs-opc-price-strike").css("display","flex");	
		}
		jQuery(".wgs-opc-link-container-active").find(".wgs-opc-price-kl-discount").text(finalPriceTextShow);
		jQuery(".wgs-opc-link-container-active").find(".wgs-opc-price-final").find("span").text(finalPriceCycleTextShow);
		jQuery(".wgs-opc-link-container").next(".wgs-opc-li-bill-container").css("display","none");
		jQuery(".wgs-opc-link-container").removeClass("wgs-opc-link-container-active");
		jQuery(".slick-track").css("margin-bottom","0px");
		jQuery(this).parent().parent().prev(".wgs-opc-link-container").attr("billcycle",cycleValueGet);	
	});
});	
/****** search button clicked **********/
function wgsSearchdomain(obj){
	jQuery(".domain-result-box").addClass("hidden");
	jQuery(obj).html('<i class="fa fa-spinner fa-spin"></i>');
	jQuery("#loaderSpins").removeClass("hidden");
	var tokenget = csrfToken;
	var domainName = jQuery("#domainname").val();
	if(jQuery.trim(domainName) == ''){
		jQuery("#domainname").focus();
		jQuery("#loaderSpins").addClass("hidden");
		jQuery("#search-icon-box").html('<i class="fa fa-search"></i>');		
	}else{
		var tldlist = jQuery("#tldDropdown").val();
		var fullDomainName = domainName+tldlist;
		var searchFor = jQuery(obj).attr("searchfor");
		if(searchFor == 'register'){
			jQuery(".domain-suggestion-main").addClass("hidden");
			wgsCallAjaxDomain(tokenget,fullDomainName,'domain','register');
			//wgsCallAjaxDomain(tokenget,fullDomainName,'spotlight','register');
			wgsCallAjaxDomain(tokenget,fullDomainName,'suggestions','register');
		}else if(searchFor == 'transfer'){
			jQuery(".domain-suggestion-main").addClass("hidden");
			jQuery(".domain-available-container").removeClass("errorUnavail");
			jQuery(".domain-available-container").html('');
			jQuery(".domain-suggestion").html('');
			wgsCallAjaxDomainTransfer(tokenget,fullDomainName,'transfer',domainName,tldlist,'transfer');
		}else if(searchFor == 'exist'){
			var tlsearchtxt = jQuery("#tldsearchText").val();
			if(jQuery.trim(tlsearchtxt) == ''){
				jQuery("#tldsearchText").focus();
				jQuery("#loaderSpins").addClass("hidden");
				jQuery("#search-icon-box").html('Add This');
			}else{
				wgsDomainAddOwnDomain(domainName,tlsearchtxt,tokenget);
			}
		}else if(searchFor == 'subdomain'){
			var tldlist = jQuery("#tldDropdownSubDomain").val();
			var fullDomainName = domainName+tldlist;
			wgsCallAjaxDomainSubDomain(tokenget,fullDomainName,'subdomain',domainName,tldlist,'subdomain');
		}
	}
}
/********** Function call for sub domain feature V1.0.6 **********/
function wgsCallAjaxDomainSubDomain(tokenget,fullDomainName,domaintype,domainsld,domaintld,domainresponseFor){
	jQuery.ajax({
		type: "POST",
		url: 'index.php?rp=/domain/check',
		data:{
			'token':tokenget,
			'domain':fullDomainName,
			'type':domaintype,
			'sld':domainsld,
			'tld':domaintld,
			'source': 'cartAddDomain',
		},
		success:function (data){
			var decodeJson = jQuery.parseJSON(JSON.stringify(data));
			if(decodeJson.result[0]){
				if(decodeJson.result[0].status === true && decodeJson.result[0].num > 0){
					wgsDomainAddSubDomainDomain(fullDomainName);
				}else{
					jQuery(".domain-available-container").addClass("errorUnavail");
					jQuery(".domain-available-container").html('');
					var subDomainError = '<div class="cong-massage"><p>'+fullDomainName+' '+wgsOnePageSubDomainError+'</p></div>';
					jQuery(".domain-available-container").html(subDomainError);
					jQuery(".domain-available-container").removeClass("hidden");
					jQuery(".domain-result-box").removeClass("hidden");						
				}
			}else if(decodeJson.result.error){
				var errorDefined = decodeJson.result.error;
				jQuery(".domain-available-container").addClass("errorUnavail");
				jQuery(".domain-available-container").html('');
				var subDomainError = '<div class="cong-massage"><p>'+errorDefined+'</p></div>';
				jQuery(".domain-available-container").html(subDomainError);
				jQuery(".domain-available-container").removeClass("hidden");
				jQuery(".domain-result-box").removeClass("hidden");	
			}
			jQuery("#loaderSpins").addClass("hidden");
			jQuery("#search-icon-box").html('<i class="fa fa-search"></i>');
		}, 
		
	});
}
/**** function for add domain when sub domain use V1.0.6 *******/
function wgsDomainAddSubDomainDomain(subDomainName){
	var pidGet = jQuery("#dpid").val();
	var pidcount = jQuery("#pidcnt").val();
	jQuery.ajax({
		type: "POST",
		url: '',
		data:{
			'actionOnePage':'callAjaxMethod',
			'methodOnePage':'subDomainAdd',
			'fulldomain':subDomainName,
			'pidAdd':pidGet,
			'pidCount':pidcount,
		},
		success:function (data) {
			if(data == 0){
				jQuery("#domainErrorD").html('<p>'+wgsOnePageOwnDomain+'</p>');
				jQuery("#domainErrorD").removeClass("hidden");
				jQuery('html, body').animate({
					scrollTop: jQuery("#domainErrorD").offset().top
				}, 1000);
				setTimeout(function() { 
                    jQuery("#domainErrorD").addClass("hidden");
					jQuery("#domainErrorD").html('');	
                }, 5000); 
			}else{
				jQuery("#subDomainAdded").removeClass("hidden");	
			}
			jQuery("#loaderSpins").addClass("hidden");
			setTimeout(function() { 
				jQuery("#subDomainAdded").addClass("hidden");
			}, 3000);
			jQuery("#search-icon-box").html('<i class="fa fa-search"></i>');
			cartsummary();
		},                                 		         
	});
}
/********** Function call for domain transfer feature **********/
function wgsCallAjaxDomainTransfer(tokenget,fullDomainName,domaintype,domainsld,domaintld,domainresponseFor){
	jQuery.ajax({
		type: "POST",
		url: 'index.php?rp=/domain/check',
		data:{
			'token':tokenget,
			'domain':fullDomainName,
			'type':domaintype,
			'sld':domainsld,
			'tld':domaintld,
			'source': 'cartAddDomain',
		},
		success:function (data) {
			var responseArray = createResponseArray(data,domainresponseFor);
			if(responseArray.domainlegecystatus == 'unavailable'){
				jQuery(".domain-available-container").removeClass("errorUnavail");
				jQuery(".domain-available-container").html('');
				var availableDomain = '<div class="cong-massage"><p>'+eligibleTransfer+'</p><p>'+eligibleTransferMsg+'</p><p  class="transferWg">'+eligibleTransferForUs+' '+responseArray.domainprice+'</p><div class="wgs-auth-code-form"><div class="row"><div class="col-md-6"><label for="inputAuthCode" style="width:100%;">'+authCodeLabelTitle+'<a href="javascript:void(0);" data-toggle="tooltip" data-placement="left" title="'+authCodeToolTip+'" class="pull-right"><i class="fas fa-question-circle"></i>'+authCodeHelpLabel+'</a></label><input type="text" class="form-control" id="inputAuthCodeWgsOpc" placeholder="'+authCodePlacehoder+'" data-toggle="tooltip" data-placement="top" data-trigger="manual" title="'+authCodeRequiredTitle+'"/></div><div class="col-md-6"><div class="wgs-btn-prt"><button class="add-to-cart-btn" onclick="wgsDomainAddToCart(this,\''+responseArray.domainname+'\',\''+domainresponseFor+'\',\''+addCart+'\');">'+addCart+'</button><button class="add-to-cart-btn removeBtnCat hidden" data-domain-name="'+responseArray.domainname+'" onclick="wgsDomainRemoveToCart(this,\''+responseArray.domainname+'\',\''+domainresponseFor+'\',\''+removeCart+'\');">'+removeCart+'</button></div></div></div></div></div>';
				jQuery(".domain-available-container").html(availableDomain);
				jQuery(".domain-available-container").removeClass("hidden");
				jQuery(".domain-result-box").removeClass("hidden");	
			}else if(responseArray.domainlegecystatus == 'available'){
				jQuery(".domain-available-container").addClass("errorUnavail");
				jQuery(".domain-available-container").html('');
				var unavailableDomain = '<div class="cong-massage"><p>'+notEligibleTransfer1+'</p><p>'+notEligibleTransfer2+'</p><p>'+notEligibleTransfer3+'</p><p>'+notEligibleTransfer4+'</p></div>';
				jQuery(".domain-available-container").html(unavailableDomain);
				jQuery(".domain-available-container").removeClass("hidden");
				jQuery(".domain-result-box").removeClass("hidden");				
			}
			jQuery("#loaderSpins").addClass("hidden");
			jQuery("#search-icon-box").html('<i class="fa fa-search"></i>');
		},                                 		         
	});
}
/********** Function call for domain register,suggestion,spotlight feature **********/		
function wgsCallAjaxDomain(tokenget,fullDomainName,domaintype,domainresponseFor){
	jQuery("#idnLanguageSelector").addClass("hidden");
	jQuery.ajax({
		type: "POST",
		url: 'index.php?rp=/domain/check',
		data:{
			'token':tokenget,
			'a':'checkDomain',
			'domain':fullDomainName,
			'type':domaintype,
		},
		success:function (data) {
			if(domaintype == 'domain'){
				var responseArray = createResponseArray(data,domainresponseFor);
				if(responseArray.domainerror){
					var errorsn = '<p>'+responseArray.domainerror+contactAdminstartor+'</p>';
					jQuery("#domainErrorD").html('');
					jQuery("#domainErrorD").html(errorsn);
					jQuery("#domainErrorD").removeClass("hidden");
					jQuery('html, body').animate({
						scrollTop: jQuery("#domainErrorD").offset().top
					}, 1000);
					setTimeout(function() { 
						jQuery("#domainErrorD").addClass("hidden");
						jQuery("#domainErrorD").html('');	
					}, 5000);					
				}else if(responseArray.domainlegecystatus == 'available'){
					jQuery("#domainErrorD").html('');
					jQuery("#domainErrorD").addClass("hidden");
					var availableDomain = '<div class="cong-massage"><p><span>'+domainavailable1+'</span> '+domainavailable2+'</p><div class="row wgs-domain-res"><div class="col-md-4"><h3>'+responseArray.domainname+'</h3></div><div class="col-md-4"><h2>'+responseArray.domainprice+'</h2></div><div class="col-md-4"><button class="add-to-cart-btn" onclick="wgsDomainAddToCart(this,\''+responseArray.domainname+'\',\''+domainresponseFor+'\',\''+addCart+'\');">'+addCart+'</button><button class="add-to-cart-btn removeBtnCat hidden" data-domain-name="'+responseArray.domainname+'" onclick="wgsDomainRemoveToCart(this,\''+responseArray.domainname+'\',\''+domainresponseFor+'\',\''+removeCart+'\');">'+removeCart+'</button></div></div></div>';
					jQuery(".domain-available-container").removeClass("errorUnavail");
					jQuery(".domain-available-container").html('');
					jQuery(".domain-available-container").html(availableDomain);
					jQuery(".domain-available-container").removeClass("hidden");
					jQuery(".domain-result-box").removeClass("hidden");
					if(responseArray.domainname !== responseArray.domainIdnName){
						jQuery("#idnLanguageSelector").removeClass("hidden");
					}else{
						jQuery("#idnLanguageSelector").addClass("hidden");
					}
				}else{
					jQuery("#domainErrorD").html('');
					jQuery("#domainErrorD").addClass("hidden");
					var unavailableDomain = '<div class="cong-massage"><p><span>'+responseArray.domainname+'</span> '+domainUnavailable+'</p></div>';
					jQuery(".domain-available-container").addClass("errorUnavail");
					jQuery(".domain-available-container").html('');
					jQuery(".domain-available-container").html(unavailableDomain);
					jQuery(".domain-available-container").removeClass("hidden");
					jQuery(".domain-result-box").removeClass("hidden");
				}
				jQuery("#loaderSpins").addClass("hidden");
			    jQuery("#search-icon-box").html('<i class="fa fa-search"></i>');
			}else if(domaintype == 'spotlight'){
				//jQuery.each(data.result, function(t,n){
				//});
			}else if(domaintype == 'suggestions'){
					jQuery(".domain-suggestion").html('');
					var suggest = '';
					var counter = 0;
					var classHidden = '';
					jQuery.each(data.result, function(t,n){
						if(counter > 4){
							classHidden = 'hidden';
							jQuery(".domain-name-btn-container").removeClass("hidden");
						}
						var priceGet = n['pricing'][1].register;
						priceGet = priceGet.split(' ');
						var suffixs = '';
						if(typeof(priceGet[1]) != "undefined"){
							suffixs = priceGet[1];
						}
						suggest += '<div class="domain-name-container '+classHidden+'"><h6 class="hot-tag '+n.group+'">'+n.group+'</h6><h3>'+n.domainName+'</h3><h2> '+priceGet[0]+'<sub>'+suffixs+'</sub></h2><button class="add-to-cart-btn" onclick="wgsDomainAddToCart(this,\''+n.domainName+'\',\''+domainresponseFor+'\',\''+addCart+'\');">'+addCart+'</button><button class="add-to-cart-btn removeBtnCat hidden" data-domain-name="'+n.domainName+'" onclick="wgsDomainRemoveToCart(this,\''+n.domainName+'\',\''+domainresponseFor+'\',\''+removeCart+'\');">'+removeCart+'</button></div>';
						counter = counter+1;
					});
					jQuery(".domain-suggestion").html(suggest);
					if(counter > 0){
						jQuery(".domain-suggestion-main").removeClass("hidden");		
					}
			}
		},                                 		         
	});
}
/*********** function for create array and return data with domain info ************/		
// modified in V1.0.4
function createResponseArray(domainArr,domainType){
	var decodeJsons = jQuery.parseJSON(JSON.stringify(domainArr));
	var domainParams = {};
	if(decodeJsons.result['error']){
		domainParams["domainerror"] = decodeJsons.result['error'];
	}else if(decodeJsons.result[0].preferredTLDNotAvailable){
		domainParams["domainerror"] = preferedTlds;
	}else{
		var jsonDecodeRes = decodeJsons.result[0];
		var domainStatus = jsonDecodeRes.isAvailable;
		var domainName = jsonDecodeRes.domainName;
		var lagecyStatus = jsonDecodeRes.legacyStatus;
		var idnDomainName = jsonDecodeRes.idnDomainName;
		if(domainType == 'register'){
			if (jsonDecodeRes['pricing'][1]){
				var domainPriceRaw = jsonDecodeRes['pricing'][1].register;
			}else if(jsonDecodeRes['pricing'][2]){
				var domainPriceRaw = jsonDecodeRes['pricing'][2].register;
			}else if(jsonDecodeRes['pricing'][3]){
				var domainPriceRaw = jsonDecodeRes['pricing'][3].register;
			}else if(jsonDecodeRes['pricing'][4]){
				var domainPriceRaw = jsonDecodeRes['pricing'][4].register;
			}else if(jsonDecodeRes['pricing'][5]){
				var domainPriceRaw = jsonDecodeRes['pricing'][5].register;
			}else if(jsonDecodeRes['pricing'][6]){
				var domainPriceRaw = jsonDecodeRes['pricing'][6].register;
			}else if(jsonDecodeRes['pricing'][7]){
				var domainPriceRaw = jsonDecodeRes['pricing'][7].register;
			}else if(jsonDecodeRes['pricing'][8]){
				var domainPriceRaw = jsonDecodeRes['pricing'][8].register;
			}else if(jsonDecodeRes['pricing'][9]){
				var domainPriceRaw = jsonDecodeRes['pricing'][9].register;
			}else if(jsonDecodeRes['pricing'][10]){
				var domainPriceRaw = jsonDecodeRes['pricing'][10].register;
			}
		}else if(domainType == 'transfer'){
			if (jsonDecodeRes['pricing'][1]){
				var domainPriceRaw = jsonDecodeRes['pricing'][1].transfer;
			}else if(jsonDecodeRes['pricing'][2]){
				var domainPriceRaw = jsonDecodeRes['pricing'][2].transfer;
			}else if(jsonDecodeRes['pricing'][3]){
				var domainPriceRaw = jsonDecodeRes['pricing'][3].transfer;
			}else if(jsonDecodeRes['pricing'][4]){
				var domainPriceRaw = jsonDecodeRes['pricing'][4].transfer;
			}else if(jsonDecodeRes['pricing'][5]){
				var domainPriceRaw = jsonDecodeRes['pricing'][5].transfer;
			}else if(jsonDecodeRes['pricing'][6]){
				var domainPriceRaw = jsonDecodeRes['pricing'][6].transfer;
			}else if(jsonDecodeRes['pricing'][7]){
				var domainPriceRaw = jsonDecodeRes['pricing'][7].transfer;
			}else if(jsonDecodeRes['pricing'][8]){
				var domainPriceRaw = jsonDecodeRes['pricing'][8].transfer;
			}else if(jsonDecodeRes['pricing'][9]){
				var domainPriceRaw = jsonDecodeRes['pricing'][9].transfer;
			}else if(jsonDecodeRes['pricing'][10]){
				var domainPriceRaw = jsonDecodeRes['pricing'][10].transfer;
			}
		}else if(domainType == 'renew'){
			if (jsonDecodeRes['pricing'][1]){
				var domainPriceRaw = jsonDecodeRes['pricing'][1].renew;
			}else if(jsonDecodeRes['pricing'][2]){
				var domainPriceRaw = jsonDecodeRes['pricing'][2].renew;
			}else if(jsonDecodeRes['pricing'][3]){
				var domainPriceRaw = jsonDecodeRes['pricing'][3].renew;
			}else if(jsonDecodeRes['pricing'][4]){
				var domainPriceRaw = jsonDecodeRes['pricing'][4].renew;
			}else if(jsonDecodeRes['pricing'][5]){
				var domainPriceRaw = jsonDecodeRes['pricing'][5].renew;
			}else if(jsonDecodeRes['pricing'][6]){
				var domainPriceRaw = jsonDecodeRes['pricing'][6].renew;
			}else if(jsonDecodeRes['pricing'][7]){
				var domainPriceRaw = jsonDecodeRes['pricing'][7].renew;
			}else if(jsonDecodeRes['pricing'][8]){
				var domainPriceRaw = jsonDecodeRes['pricing'][8].renew;
			}else if(jsonDecodeRes['pricing'][9]){
				var domainPriceRaw = jsonDecodeRes['pricing'][9].renew;
			}else if(jsonDecodeRes['pricing'][10]){
				var domainPriceRaw = jsonDecodeRes['pricing'][10].renew;
			}
		}
		domainParams["domainname"] = domainName;
		domainParams["domainprice"] = domainPriceRaw;
		domainParams["domainstatus"] = domainStatus;
		domainParams["domainlegecystatus"] = lagecyStatus;
		domainParams["domainIdnName"] = idnDomainName;
	}
	return domainParams;
}
/********** function called for more suggestion in case of domain register ***********/
function wgsMoreSuggestion(obj){
	var counter = 0;
	jQuery("#lesssuggestion").removeClass("hidden");
	jQuery(".domain-suggestion").find(".domain-name-container.hidden").each(function(){
		if(counter < 5){
			jQuery(this).removeClass("hidden");
			jQuery(this).addClass("sl");
		}
		counter = counter+1;
	});
}
/********** function called for reduce suggestion functionality ************/
function wgsLessSuggestion(obj){
	var counter = 1;
	var lengthCount = jQuery(".domain-suggestion").find(".domain-name-container.sl").length-5;
	jQuery(".domain-suggestion").find(".domain-name-container.sl").each(function(){
		if(counter > lengthCount ){
			jQuery(this).removeClass("sl");
			jQuery(this).addClass("hidden");
		}
		counter = counter+1;
	});
	var lengthCount = jQuery(".domain-suggestion").find(".domain-name-container.sl").length;
	if(lengthCount == 0){
		jQuery(obj).addClass("hidden");
	}
	jQuery('html, body').animate({
        scrollTop: jQuery(".domain-suggestion").offset().top
    }, 1000);
}
/********** function called for product group change functionality *******/
function wgsSelectProductGroup(obj){
	var groupId = jQuery(obj).val();
	jQuery(".choose-pla-wrapper").addClass("hidden");
	jQuery('.choose-billing-wrapper').addClass("hidden");
	jQuery('.Add-ons-wrapper').addClass("hidden");
	jQuery('.wgs_product_configuration').addClass("hidden");
	jQuery("#productBoxWgs").html('');
	jQuery('#wgsBillingCycleDiv').html('');
	jQuery('#wgsProductAddons').html('');
	jQuery('.wgs_product_configuration').html('');
	if(groupId != ''){ 
		jQuery("#loaderSpinsProduct").removeClass("hidden");
		jQuery.ajax({
			type: "POST",
			url: '',
			data:{
				'actionOnePage':'callAjaxMethod',
				'methodOnePage':'getProduct',
				'gid':groupId,
			},
			success:function (data) {
				jQuery("#loaderSpinsProduct").addClass("hidden");
				var decodeJson = jQuery.parseJSON(data);
				var lengthGet = jQuery(decodeJson).length;
				var productString = '';
				productString += '<div class="prodWgs">';
				jQuery.each(decodeJson,function(t,n){
					var productDescription = n.pdescp;
					var domainRequired = '';
					var quantityAvail = '';
					if(n.pdomainoption == 1){
						domainRequired = '<div class="chk-note">'+wgsOnePageDomainRequired+'</div>';
					}
					if(n.stockcntrl == 1 && n.stockqty == 0){
						var dropDownsAndButton = '<div class="wgs-opc-btn-wrapr"><a class="wgs-opc-btn-red wgs-opc-btn-cls wgsopcevent" href="javascript:void(0);" id="addProdBtn' + n.pid + '">' + wgsOnePageOutStockBtn + '</a></div>';
					}else{
						if (jQuery(n.pbillcyclehtml).find("#billContainerOpc" + n.pid).html() == ''){
							var dropDownsAndButton = '<div class="wgs-opc-select-bill-cycle">' + wgsOnePageBillingCycle + '</div><div class="wgs-opc-main-bill">' + n.activebillcyclehtml + '' + n.pbillcyclehtml + '</div><div class="wgs-opc-btn-wrapr"><a class="wgs-opc-btn-blue wgs-opc-btn-cls" href="javascript:void(0);" id="addProdBtn' + n.pid + '" onclick="wgsOnePageAddProduct(this,\'' + n.pid + '\',\'listoff\');">' + addCart + ' <i class="fas fa-plus-circle"></i></a></div>';
						}else{
							var dropDownsAndButton = '<div class="wgs-opc-select-bill-cycle">'+wgsOnePageBillingCycle+'</div><div class="wgs-opc-main-bill">'+n.activebillcyclehtml+''+n.pbillcyclehtml+'</div><div class="wgs-opc-btn-wrapr"><a class="wgs-opc-btn-blue wgs-opc-btn-cls" href="javascript:void(0);" id="addProdBtn'+n.pid+'" onclick="wgsOnePageAddProduct(this,\''+n.pid+'\',\'liston\');">'+addCart+' <i class="fas fa-plus-circle"></i></a></div>';
						}						
					}
					if(n.stockcntrl == 1){
						quantityAvail = '<p class="stockOpc">'+n.stockqtyhtml+'</p>';
					}
					productString += '<div class="col-sm-12 col-md-4 intel_box"><div class="check_box" id="prod-check-'+n.pid+'"><input type="radio" id="prod'+n.pid+'" value="'+n.pid+'" name="productRadio"><label for="prod-'+n.pid+'">'+domainRequired+'<span class="xeon"><div class="wgs-upper-part"><strong>'+n.pname+'</strong>'+quantityAvail+' '+dropDownsAndButton+'</div><span class="ram-box">'+productDescription+'</span></span></label></div></div>';
				});
				productString += '</div>';
				jQuery("#productBoxWgs").html('');
				jQuery("#productBoxWgs").html(productString);
				jQuery(".choose-pla-wrapper").removeClass("hidden"); 
				if(lengthGet > 1){
					jQuery(".prodWgs").not('.slick-initialized').slick({
					   dots: false,
					   infinite: true,
					   slidesToShow: 3,
					   slidesToScroll: 3,
					   autoplay: false,
					   autoplaySpeed: false,
					   variableWidth: false,
					   rtl: rtlVar,
					   responsive: [
					    {breakpoint: 1024,settings: {slidesToShow: 3,slidesToScroll: 3,}},
						{breakpoint: 700,settings: {slidesToShow: 2,slidesToScroll: 2}},
						{breakpoint: 600,settings: {slidesToShow: 1,slidesToScroll: 1}},
						{breakpoint: 480,settings: {slidesToShow: 1,slidesToScroll: 1}},
						{breakpoint: 280,settings: {slidesToShow: 1,slidesToScroll: 1}}
						]
					});
				}
				var checkIfProductaddedFromQuery = jQuery("#productIdFirstTime").val();
				var checkIfBillingaddedFromQuery = jQuery("#productBillCycle").val();
				if(checkIfBillingaddedFromQuery != ''){
					jQuery("#productBillCycle").val('');
					jQuery("#linkContainOpc"+checkIfProductaddedFromQuery).trigger('click');
					jQuery("#billContainerOpc"+checkIfProductaddedFromQuery).find(".wgs-opc-bill-cycle-row").each(function(){
						if(jQuery(this).attr("wgs-data-billcycle") == checkIfBillingaddedFromQuery){
							jQuery(this).trigger('click');
						}
					});
				}
				if(checkIfProductaddedFromQuery != ''){
					if(jQuery("#opId"+checkIfProductaddedFromQuery).length == 0){
						jQuery("#productIdFirstTime").val('');
						jQuery("#addProdBtn"+checkIfProductaddedFromQuery).click();
						var getSlideCount = jQuery("#prod"+checkIfProductaddedFromQuery).parent().parent().attr("data-slick-index");
						if(getSlideCount > 2){
						   var clickSlideNo = Math.ceil(parseFloat(parseInt(getSlideCount) + 1)/3);
							jQuery('ul.slick-dots li:nth-child('+clickSlideNo+')').trigger('click');
						}
					}//V1.0.4
					else{
						jQuery("#opId"+checkIfProductaddedFromQuery).find(".wgs-edit-pencil").trigger('click');
					}
				}
			},                                 		         
		});
	} // V1.0.4 condition for hidden product
	else{  
		var checkIfProductaddedFromQuery = jQuery("#productIdFirstTime").val();
		var checkIfBillingaddedFromQuery = jQuery("#productBillCycle").val();
		var checkHasClassHidden = jQuery(".choose-pla-wrapper").hasClass("hidden");
		if(checkIfProductaddedFromQuery != ''){
			setTimeout(function() { 
				if(jQuery("#opId"+checkIfProductaddedFromQuery).length == '0'){
					jQuery("#productIdFirstTime").val('');
					jQuery("#productBillCycle").val('');
					wgsAddProductGetProductRelatedInfo(obj,checkIfProductaddedFromQuery,checkIfBillingaddedFromQuery);
				}else{
					jQuery("#opId"+checkIfProductaddedFromQuery).find(".wgs-edit-pencil").trigger('click');
				}
			}, 4000);
		}
	}
}
/******* function for add product and get product info *********/
function wgsAddProductGetProductRelatedInfo(obj,pid,billcycle){
	jQuery("#loaderSpinsBilling").removeClass("hidden");
	jQuery(".wgs_product_configuration").addClass("hidden");
	jQuery(".wgs_product_configuration").html('');
	jQuery("#dpid").val('');
	jQuery("#pidcnt").val('');
	jQuery("#domainname").val('');
	//jQuery(".wgs-choose-dms").addClass("hidden");
	//jQuery(".registerDm").addClass("hidden");
	jQuery(".ownDm").addClass("hidden");
	//jQuery(".transferDm").addClass("hidden");
	jQuery(".domain-result-box").addClass('hidden');
	jQuery("#btnDomainManages").prop("disabled",false);
	jQuery("#wgs_set_pid").val(pid);
	var staticPid = jQuery("#staticProductId").val();
	if(staticPid != ''){
		wgsRemoveStaticProductFromSession(obj,staticPid);
	}
	jQuery.ajax({
		type: "POST",
		url: '',
		data:{
			'actionOnePage':'callAjaxMethod',
			'methodOnePage':'getProductRelatedInfo',
			'pid':pid,
			'cycle':billcycle,
		},
		success:function (data) {
			var decodeJson = jQuery.parseJSON(data);
			wgsGetProductConfigOption(decodeJson.sessioncount);
			if(decodeJson.domainrequired == 1){
				// V1.0.4
				var fromQuery = jQuery("#dmtype").val();
				if(fromQuery == ""){
					wgsManagePidDomain(pid,decodeJson.sessioncount,decodeJson.subdomains);
				}
			    wgsDomainConfigureAddDomain();
			}
		},
	});
}
/**** config product function ************/
function wgsGetProductConfigOption(data){
	jQuery.post("cart.php", 'ajax=1&a=confproduct&i='+data,
	function (data) {
			if(data != 'An Error Occurred. Please Try Again.' && data != 'An Error Occurred. Try Again.') {
			   jQuery('.wgs_product_configuration').html(data);
			   cartsummary();
			}else{
			   jQuery('.wgs_product_configuration').html('');
			   cartsummary();
			}
		// for version 1.0.3
		wgsPromoAddonReplace();
		jQuery("#loaderSpinsBilling").addClass("hidden");
		jQuery('.wgs_product_configuration').removeClass("hidden");
		//V1.0.4
		jQuery("#panelHeadOpens").trigger("click");
	});	
}
/**********function for replacement promo addon ********/
function wgsPromoAddonReplace(){
	var panelHtml = '';
	var promoCounter = 0;
	jQuery(".wgs_promo_data").find(".addon-promo-container").each(function(){
		var lihtml = '';
		var imagesrc = jQuery(this).find('.description').find('.logo').find('img').attr('src');
		var heading = jQuery(this).find('.description').find('h3').text();
		var description = jQuery(this).find('.description').find('p').html();
		panelHtml += '<div class="panel-group"><div class="panel">';
		panelHtml += '<div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" href="#collapsepromo'+promoCounter+'" aria-expanded="false" class="collapsed"><img src="'+imagesrc+'"> '+heading+' <i class="fa fa-chevron-down" aria-hidden="true"></i></a></h4></div>';
		panelHtml += '<div id="collapsepromo'+promoCounter+'" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">';
		panelHtml += '<div class="panel-body"><p>'+description+'</p>';
		jQuery(this).find('label.radio-inline').each(function(){
			var radioHtml = jQuery(this).html();
			lihtml += '<li><div class="myCheckbox"><label class="lab-text radio-inline" onclick="wgsCallAjaxForConfigureProduct();">'+radioHtml+'</label></div></li>';
		});
		panelHtml += '<ul>'+lihtml+'</ul>';
		panelHtml +='</div></div></div></div>';
		promoCounter = promoCounter+1;
	});
	jQuery("#addonPromos").html('');
	jQuery("#wgs_promo_wraper").html('');
	jQuery("#wgs_promo_wraper").html(panelHtml);	
}

/********* function to call ajax on configure page ********/
function wgsCallAjaxForConfigureProduct(){
  jQuery.post("cart.php", 'ajax=1&a=confproduct&' + jQuery("#frmConfigureProduct").serialize(),
   function (data) {
	   if (data != '') {
		   jQuery('#containerProductValidationErrorsList').removeClass('hidden').html(data);
		    cartsummary();
	   } else {
		   jQuery('#containerProductValidationErrorsList').addClass('hidden').html('');
		   cartsummary();
	   }
   });
}

/******** function to call cart summary ********/
function cartsummary() {
   jQuery("#wgssummaryloaders").removeClass("hidden");
   jQuery.post("cart.php", 'ajax=1&a=view',
           function (data) {
			   var staticProductId = jQuery("#staticProductId").val();
               jQuery("#wgs-right-cart").html(data);
			   jQuery("#wgs-right-cart").find("a.accordion-toggle").trigger('click');
			   jQuery("#wgs-right-cart").find(".wgs-accordian-cls").each(function(){
				   if(staticProductId != jQuery(this).attr("datapid")){
						jQuery("#manageSummary").removeClass("hidden");
				   }else{
						jQuery("#manageSummary").addClass("hidden");  
				   }
			   });
			   var addonsGroup = jQuery("#wgs-right-cart").find(".addons-group").length;
			   var domainsGroup = jQuery("#wgs-right-cart").find(".domains-group").length;
			   var domainsRenewlGroup = jQuery("#wgs-right-cart").find(".domain-renewl-group").length;
			    if(addonsGroup > 0 || domainsGroup > 0 || domainsRenewlGroup > 0){
					jQuery("#manageSummary").removeClass("hidden");   
			    }
			   if(jQuery("#wgsCartCount").val() > 0){
				   jQuery("#wgs-extra-btn-checkout").removeClass("hidden");
				   jQuery("span.wgs-custom-label-cart-one-page").addClass("wgsblink");
				   jQuery("span.wgs-custom-label-cart-one-page").text(jQuery("#wgsCartCount").val());
			   }else{
				   jQuery("#wgs-extra-btn-checkout").addClass("hidden");   
			   }
               jQuery("#wgssummaryloaders").addClass("hidden");
           });
		var checkIfPromoaddedFromQuery = jQuery("#productPromocode").val();
		if(checkIfPromoaddedFromQuery != ''){
			setTimeout(function () {
				if(jQuery('.apply-box').length > 0){
					jQuery("#inputPromotionCode").val('');
					jQuery("#inputPromotionCode").val(checkIfPromoaddedFromQuery);
					jQuery("#btnPromoApply").trigger('click');
				}
			}, 1000);
		}
}
/***** function remove product from cart *******/
function wgsRemoveProductFromCart(obj){
  jQuery(obj).attr('disabled', true);
  jQuery("#productBoxWgs").find(".check_box").removeClass("active-bill");
  var removeType = jQuery("#inputRemoveItemType").val();
  jQuery.post("cart.php", 'ajax=1&' + jQuery("#form-remove-product").serialize(),
   function (data) {
	   if (data != '') {
		   cartsummary();
	   } else {
		   cartsummary();
		   jQuery('#containerProductValidationErrors').addClass('hidden').html('');
	   }
		if(removeType == 'd'){
			var domainNamesd = jQuery(obj).attr('remove-data-d');
			jQuery(".domain-result-box").find("button.removeBtnCat").each(function(){
				if(jQuery(this).attr("data-domain-name") == domainNamesd){
					jQuery(this).addClass("hidden");
					jQuery(this).prev("button").removeClass("hidden");
				}
			});
		}
		if(removeType == 'p'){
			var iValue = jQuery("#inputRemoveItemRef").val();
			var formClass = jQuery("#frmConfigureProduct").attr("class");
			var classmatch = 'pidConf'+iValue;
			var productIds = jQuery(obj).attr('remove-data-p');	
			jQuery("#addProdBtn"+productIds).html(''+addCart+' <i class="fas fa-plus-circle"></i>');
			jQuery("#addProdBtn"+productIds).removeClass("billCycleHit").css("pointer-events", "auto");
			jQuery("#linkContainOpc"+productIds).removeAttr("billcycle");
			jQuery(".wgs_product_configuration").addClass("hidden");
			jQuery(".wgs_product_configuration").html('');
		}
		// V1.0.4
		if(removeType == 'r'){
			var iValueRenew = jQuery("#inputRemoveItemRef").val();
			jQuery("#renewDomain"+iValueRenew).html(jQuery("#renewDomain"+iValueRenew).attr("button-renew-name"));
			jQuery("#renewDomain"+iValueRenew).attr('disabled', false);
		}
		wgsDomainConfigureAddDomain();
		jQuery(obj).attr('disabled', false);
		jQuery("#modalRemoveItem").modal("hide");
   });
  jQuery('body').removeClass("modal-open");
  jQuery('body').removeAttr("style");
}
/********* function for remove all product from cart ******/
function wgsRemoveProductFromCartAll(obj){
  jQuery(obj).attr('disabled', true);
  jQuery('body').removeClass("modal-open");
  jQuery('body').removeAttr("style");
  jQuery.post("cart.php", 'ajax=1&' + jQuery("#form-remove-all-product").serialize(),
   function (data) {
	   window.location = 'cart.php';
   });	
}
/***** function for promocode apply ******/
function wgsApplyPromo(btntxt) {
   if (jQuery("#inputPromotionCode").val() == '') {
       jQuery("#inputPromotionCode").focus();
       jQuery("#inputPromotionCode").css('border-bottom', '1px solid #ff0000');
       return false;
   }
   jQuery("#inputPromotionCode").css('border-bottom', '1px solid #d2d2d2');
   jQuery('#btnPromoApply').html(btntxt + ' <i class="fa fa-spinner fa-spin"></i>');
   jQuery.post("cart.php", {a: "applypromo", promocode: jQuery("#inputPromotionCode").val(),ajax:1},
   function(data){
       if (data) {
			jQuery("#mdl-body-text").html(data);
			jQuery("#modalPromoCodeAlert").modal('show');
       } else {
           cartsummary();
       }
       jQuery('#btnPromoApply').html(btntxt);
   });
}
/******* function for promocode remove *****/
function wgsRemovePromo(btntxt){
   var response = confirm(removePromoCode);
   if (response) {
       jQuery('#btnRemovePromo').html(btntxt + ' <i class="fa fa-spinner fa-spin"></i>');
       jQuery.post("cart.php", {a: "removepromo", ajax: 1},
       function (data) {
		   jQuery("#productPromocode").val('');
           cartsummary();
           jQuery('#btnRemovePromo').html(btntxt);
       });
   }
}
/**** function for trigger gateway ********/
function triggerPaymentMethod(obj, id){
	jQuery('.pymntmthd').each(function(){
		jQuery(this).removeClass('active');
	});
	jQuery(obj).addClass('active');
    jQuery("#iCheck-"+id).iCheck('check');
    jQuery("#iCheck-"+id).iCheck('update');
}
/**** function for add domain in cart *******/
function wgsDomainAddToCart(obj,domainName,domaintype,btntxt){
	if(domaintype == 'transfer'){
		var valueGet = jQuery.trim(jQuery("#inputAuthCodeWgsOpc").val());
		if(valueGet == ''){
			jQuery("#inputAuthCodeWgsOpc").css('border-color','#fa7373');
			jQuery("#inputAuthCodeWgsOpc").focus();
			return false;
		}
	}
	var ajaxEpp = '';
	if(domaintype == 'transfer'){
		ajaxEpp = valueGet;
	}
	var domainPeriodQuery = jQuery.trim(jQuery("#wgs_domain_period").val());
	var period = 1;
	if(domainPeriodQuery != ''){
		period = domainPeriodQuery;
	}
	jQuery("#inputAuthCodeWgsOpc").css('border-color','transparnet');
	jQuery(obj).attr('disabled', true);
	var pidGet = jQuery("#dpid").val();
	var pidcount = jQuery("#pidcnt").val();
	var staticPid = jQuery("#staticProductId").val();
	jQuery(obj).html(btntxt + ' <i class="fa fa-spinner fa-spin"></i>');
	jQuery.ajax({
		type: "POST",
		url: "cart.php?a=checkout",
		data:{
			'actionOnePage':'callAjaxMethod',
			'methodOnePage':'addDomainToCart',
			'domain':domainName,
			'type':domaintype,
			'pidAdd':pidGet,
			'pidCount':pidcount,
			'eppcodes':ajaxEpp,
			'period':period,
		},
		success:function (data) {
			if(data == 0){
				jQuery("#domainErrorD").html('<p>'+domainAlreadyExist+'</p>');
				jQuery("#domainErrorD").removeClass("hidden");
				jQuery(obj).html(btntxt);
				jQuery(obj).attr('disabled', false);
				jQuery('html, body').animate({
					scrollTop: jQuery("#domainErrorD").offset().top
				}, 1000);
				setTimeout(function() { 
                    jQuery("#domainErrorD").addClass("hidden");
					jQuery("#domainErrorD").html('');	
                }, 5000); 
			}else{
				jQuery(obj).addClass("hidden");
				jQuery(obj).next("button").removeClass("hidden");
				jQuery(obj).html(btntxt);
				jQuery(obj).attr('disabled', false);
			}
			if(staticPid != ''){
				wgsRemoveStaticProductFromSession(obj,staticPid);
			}
			wgsDomainConfigureAddDomain();
		},                                 		         
	});	
}
/**** function for add domain when own domain use *******/
function wgsDomainAddOwnDomainProduct(sld,tld){
	var pidGet = jQuery("#dpid").val();
	var pidcount = jQuery("#pidcnt").val();
	jQuery.ajax({
		type: "POST",
		url: '',
		data:{
			'actionOnePage':'callAjaxMethod',
			'methodOnePage':'ownDomainAdd',
			'tld':tld,
			'sld':sld,
			'pidAdd':pidGet,
			'pidCount':pidcount,
		},
		success:function (data) {
			if(data == 0){
				jQuery("#domainErrorD").html('<p>'+wgsOnePageOwnDomain+'</p>');
				jQuery("#domainErrorD").removeClass("hidden");
				jQuery('html, body').animate({
					scrollTop: jQuery("#domainErrorD").offset().top
				}, 1000);
				setTimeout(function() { 
                    jQuery("#domainErrorD").addClass("hidden");
					jQuery("#domainErrorD").html('');	
                }, 5000); 
			}else{
				jQuery("#ownDomainAdded").removeClass("hidden");	
			}
			jQuery("#loaderSpins").addClass("hidden");
			setTimeout(function() { 
				jQuery("#ownDomainAdded").addClass("hidden");
			}, 3000);
			jQuery("#search-icon-box").html('<i class="fa fa-share" aria-hidden="true"></i>');
			cartsummary();
		},                                 		         
	});
}
/**** V1.0.8 Added for own domain check case *******/
function wgsDomainAddOwnDomain(sld,tld,tokenget){
	var res = tld.charAt(0);
	if(res == '.'){
		var fullDomainName = sld+tld;		
	}else{
		var fullDomainName = sld+'.'+tld;	
	}
	jQuery.ajax({
		type: "POST",
		url: 'index.php?rp=/domain/check',
		data:{
			'token':tokenget,
			'a':'checkDomain',
			'domain':fullDomainName,
			'type':'owndomain',
		},
		success:function(data) {
		   var decodeJsons = jQuery.parseJSON(JSON.stringify(data));
		    if(decodeJsons.result['error']){
				var errorsn = '<p>'+decodeJsons.result['error']+'</p>';
				var unavailableDomain = '<div class="cong-massage"><p>'+errorsn+'</p></div>';
				jQuery(".domain-available-container").addClass("errorUnavail");
				jQuery(".domain-available-container").html('');
				jQuery(".domain-available-container").html(unavailableDomain);
				jQuery(".domain-available-container").removeClass("hidden");
				jQuery(".domain-result-box").removeClass("hidden");
				setTimeout(function() {
					jQuery(".domain-available-container").removeClass("errorUnavail");
					jQuery(".domain-available-container").html('');
					jQuery(".domain-available-container").addClass("hidden");
					jQuery(".domain-result-box").addClass("hidden");
				}, 5000); 
				jQuery("#loaderSpins").addClass("hidden");
				jQuery("#search-icon-box").html('<i class="fa fa-share" aria-hidden="true"></i>');
		    }else{
				wgsDomainAddOwnDomainProduct(sld,tld);
			}
		},
	});
}
/**** function for add domain in cart *******/
function wgsDomainRemoveToCart(obj,domainName,domaintype,btntxt){
	jQuery(obj).attr('disabled', true);
	var pidGet = jQuery("#dpid").val();
	var pidcount = jQuery("#pidcnt").val();
	jQuery(obj).html(btntxt + ' <i class="fa fa-spinner fa-spin"></i>');
	jQuery.ajax({
		type: "POST",
		url: '',
		data:{
			'actionOnePage':'callAjaxMethod',
			'methodOnePage':'removeDomainToCart',
			'domain':domainName,
			'type':domaintype,
			'pidAdd':pidGet,
			'pidCount':pidcount,
		},
		success:function (data) {
			jQuery(obj).addClass("hidden");
			jQuery(obj).prev("button").removeClass("hidden");
			jQuery(obj).html(btntxt);
			jQuery(obj).attr('disabled', false);
			wgsDomainConfigureAddDomain();
		},                                 		         
	});
}
/***** function for domain configure *********/
function wgsDomainConfigureAddDomain(){
  jQuery(".domain-configration-container").addClass("hidden");
  jQuery(".domain-configration-container").html('');
  jQuery.post("cart.php", 'ajax=1&a=confdomains',
   function (data) {
		if(data != ''){
			jQuery(".domain-configration-container").html(data);
			jQuery(".domain-configration-container").removeClass("hidden");
			// V1.0.4
			jQuery("#frmConfigureDomains").find(".panel").each(function(){jQuery(this).find("a").trigger("click");});
		}
		cartsummary();
   });
}
/***** function for configure domains part ****/
function wgsConfigDomainOption() {
   var btnTxt = jQuery('a.con-btn').text();
   jQuery('a.con-btn').addClass("wgsDisable");
   jQuery('a.con-btn').html(btnTxt + ' <i class="fa fa-spinner fa-spin"></i>');
   var post = WHMCS.http.jqClient.post("cart.php", 'ajax=1&a=confdomains&' + jQuery("#frmConfigureDomains").serialize());
   post.done(
           function (data) {
				if(jQuery.trim(data) != ''){
				   jQuery('#validation_error').addClass('alert alert-danger').html('<ul>' + data + '</ul>');
				   jQuery('html, body').animate({scrollTop: jQuery("#validation_error").offset().top - 30}, 500);
				   jQuery('a.con-btn').removeClass("wgsDisable");
				   jQuery('a.con-btn').html(btnTxt);				
				}else{
					cartsummary();
					jQuery('a.con-btn').removeClass("wgsDisable");
					jQuery('a.con-btn').html(btnTxt); 
				}
           }
   );  
}
/******** function for complete order **********/
function wgsCompleteOrderCheckout(obj,btntxt) {
	jQuery(obj).attr('disabled', true);
	jQuery(obj).html(btntxt + ' <i class="fa fa-spinner fa-spin"></i>');
    var lengthGet = jQuery("a.con-btn").length;
	var lengthCustomField = jQuery(".wgs-domain-custom-field").length;
    if(lengthGet > 0 || lengthCustomField > 0){
		wgsConfigDomainOptionCheckRequired();	
    }
	setTimeout(function() {	
		var resultValue = jQuery("#domainConfigureCheck").val();
		if(resultValue == 0){
		   jQuery(obj).attr('disabled', true);
		   jQuery(obj).html(btntxt + ' <i class="fa fa-spinner fa-spin"></i>');
		   jQuery('#validation_error').removeClass('alert alert-danger').html('');
		   jQuery('#fullpage-overlay').removeClass("hidden");
		   jQuery.post("cart.php?a=checkout", 'actionOnePage=callAjaxMethod&methodOnePage=checkhostingdomain',
		   function (data) {
				if(jQuery.trim(data) != ''){
				   jQuery('#fullpage-overlay').addClass("hidden");
				   jQuery('#validation_error').addClass('alert alert-danger').html('<ul>' + data + '</ul>');
				   jQuery('html, body').animate({scrollTop: jQuery("#validation_error").offset().top - 30}, 500);
				   jQuery(obj).attr('disabled', false);
				   jQuery(obj).html(btntxt);
				}else{
					jQuery(obj).attr('disabled', false);
					jQuery(obj).html(btntxt);
					jQuery("#btnOrderTriggerCheckout").click();
				}
			});
		}else{
			jQuery(obj).attr('disabled', false);
			jQuery(obj).html(btntxt);			
		}
	},2000);
}
/********* function for manage domain on remove ********/
function wgsManageDomainButton(obj,domainName){
	jQuery(".wgs-d-modal").attr("remove-data-d",domainName);
}
/******** function for manage product on remove *******/
function wgsManageProductButton(obj,pid){
	jQuery(".wgs-d-modal").attr("remove-data-p",pid);
} 
/******* function for edit product configuration ******/
function wgsEditProductConfigOption(ival,pid){
	jQuery("#loaderSpinsBilling").removeClass("hidden");
	jQuery("#wgs_set_pid").val(pid);
	jQuery("#productBoxWgs").find(".check_box").removeClass("active-bill");
	jQuery("#prod-check-"+pid).addClass("active-bill");
	jQuery("#addProdBtn"+pid).addClass("billCycleHit");
	jQuery("#addProdBtn"+pid).html(''+wgsOnePageBillingCycleChange+' <i class="fas fa-file-invoice"></i>');
	jQuery('html, body').animate({
        scrollTop: jQuery(".choose-pla-wrapper").offset().top
    }, 1000);
	var getSlideCount = jQuery("#prod"+pid).parent().parent().attr("data-slick-index");
	if(getSlideCount > 2){
	   var clickSlideNo = Math.ceil(parseFloat(parseInt(getSlideCount) + 1)/3);
		jQuery('ul.slick-dots li:nth-child('+clickSlideNo+')').trigger('click');
	}
    // version 1.0.3
		wgsOpenSummaryPanelAccordionWithPidPass(pid);
	// version 1.0.3 end
	wgsGetProductConfigOption(ival);
}
/**** function for manage domain div ******/
function wgsDomainSectionManage(obj,callFor){
		jQuery(".wgs-choose-dms").toggleClass("hidden");
		jQuery(".registerDm").toggleClass("hidden");
		jQuery(".transferDm").toggleClass("hidden");
		//jQuery(".ownDm").toggleClass("hidden");
}
/***** function to manage pid and domain *****/
function wgsManagePidDomain(pid,pc,subdomains){
		jQuery("#dpid").val(pid);
		jQuery("#pidcnt").val(pc);
		//jQuery("#btnDomainManages").prop("disabled",true);
		jQuery("#domainname").val('');
		jQuery(".wgs-choose-dms").removeClass("hidden");
		jQuery(".registerDm").removeClass("hidden");
		jQuery(".ownDm").removeClass("hidden");
		jQuery(".transferDm").removeClass("hidden");
		if(jQuery.trim(subdomains) != ''){
			jQuery(".subDm").removeClass("hidden");
			jQuery("#tldDropdownSubDomain").html(subdomains);
		}else{
			jQuery("#tldDropdownSubDomain").html('');
		}
		jQuery(".wgs-choose-dms").find("ul").find("li").first().find("input").trigger('click');
		jQuery(".input-search-container").removeClass("hidden");
		jQuery(".coose-domain-container").removeClass("hidden");
		jQuery("#productDomainMsg").html('');
		var productDomains = '';
		if(jQuery(".coose-domain-container").length > 0){
			productDomains = productHaveDomain;
		}else{
			productDomains = productHaveDomain+askAdminForEnable;
		}
		jQuery("#productDomainMsg").html('<p>'+productDomains+'</p>');
		jQuery("#productDomainMsg").removeClass("hidden");
		setTimeout(function() { 
			jQuery("#productDomainMsg").addClass("hidden");
			jQuery("#productDomainMsg").html('');
			if(jQuery(".coose-domain-container").length > 0) {
				jQuery('html, body').animate({
					scrollTop: jQuery(".coose-domain-container").offset().top
				}, 1000);
			}
		}, 3000);	
}
/******* function for change domain aftre assigned ********/
function wgsUpdateProductDomian(obj,icounter){
	var domainName = jQuery(obj).val();
	if(domainName != '0'){
		jQuery.ajax({
			type: "POST",
			url: '',
			data:{
				'actionOnePage':'callAjaxMethod',
				'methodOnePage':'switchAssignedProductDomain',
				'counterProdutc':icounter,
				'domainName':domainName,
			},
			success:function (data) {
				wgsDomainConfigureAddDomain();
				wgsCallAjaxForConfigureProduct();
				cartsummary();
			},                                 		         
		});
	}	
}
// version 1.0.2
/********* function for update domain period changed *******/
function wgsDomainPeriodChanged(){
	setTimeout(function() { 
		cartsummary();
	}, 500);
}
/******** function for addon add *****/
function wgsAddonPanelAddCart(obj,idaddon){
	jQuery(obj).addClass("hidden");
	jQuery(obj).next("a").removeClass("hidden");
	jQuery("#ado"+idaddon).trigger('click');
	// version 1.0.3
	jQuery("#wgs_set_pid").val(idaddon);
	// version 1.0.3 end
}
/******** function for addon remove *****/
function wgsAddonPanelRemoveCart(obj,idaddon){
	jQuery(obj).addClass("hidden");
	jQuery(obj).prev("a").removeClass("hidden");
	jQuery("#ado"+idaddon).trigger('click');
}
// version 1.0.3
function wgsOpenSummaryPanelAccordion(){
	var pidValues = jQuery("#wgs_set_pid").val();
	if(!(jQuery("#collapsep"+pidValues).hasClass("in"))){
		jQuery('#apid'+pidValues).trigger("click");
	}
}
function wgsOpenSummaryPanelAccordionWithPidPass(pidValues){
	if(!(jQuery("#collapsep"+pidValues).hasClass("in"))){
		jQuery('#apid'+pidValues).trigger("click");
	}
}
// version 1.0.4
/******** function for domain renew execute ************/
function wgsRenewDomainTriggerOnQuery(domainname){
		jQuery("#loaderSpinsRenewDomain").removeClass("hidden");
		jQuery.ajax({
			type: "GET",
			url: 'index.php/domain/'+domainname+'/renew',
			data:{
				'callactions':'true',
			},
			success:function (data) {
				jQuery("#loaderSpinsRenewDomain").addClass("hidden");
				var getHtmlData = jQuery(data).find("#domain-renew-domainGets").html();
				jQuery("#domain-renew-one-page").html(getHtmlData);
			},                                 		         
		});
}
/**** function for domain renew add functionality *******/
function wgsRenewDomainAddToCart(obj,domainId,btntxt,btntxt2){
	var GetValueDomainPeriod = jQuery("#renewalPricing"+domainId).val();
	jQuery(obj).attr('disabled', true);
	jQuery(obj).html(btntxt + ' <i class="fa fa-spinner fa-spin"></i>');
	jQuery.ajax({
		type: "POST",
		url: 'index.php?rp=/cart/domain/renew/add',
		data:{
			'domainId':domainId,
			'period':GetValueDomainPeriod,
			'token': csrfToken,
		},
		success:function (data) {
			cartsummary();
			jQuery(obj).html(btntxt2);
		},                                 		         
	});	
}
/*********  V1.0.5 *********/
/********* remove static product from session V1.0.5 *********/
function wgsRemoveStaticProductFromSession(obj,prodId){
	jQuery.ajax({
		type: "POST",
		url: 'cart.php?a=checkout',
		data:{
			'actionOnePage':'callAjaxMethod',
			'methodOnePage':'getStaticProductRemoved',
			'pid':prodId,
		},
		success:function (data) {
			jQuery("#staticProductId").val('');
		},                                 		         
	});		
}

/******** function trigger chekout button from summary **********/
function wgsTriggerOrderCheckout(obj,btntxt) {
   jQuery(obj).attr('disabled', true);
   jQuery(obj).html(btntxt + ' <i class="fa fa-spinner fa-spin"></i>');
   jQuery('#validation_error').removeClass('alert alert-danger').html('');
   jQuery('#wgsCompleteOrder').click();
   setTimeout(function() { 
		jQuery(obj).attr('disabled', false);
		jQuery(obj).html(btntxt);
   }, 5000);

}

/********** function to add product in cart *********/
function wgsTriggerProductAddition(obj,pid){
	jQuery("#prod"+pid).click();
	jQuery(obj).addClass("hidden");
	jQuery(obj).next("i").removeClass("hidden");
}

/****** function to add product ***********/
function wgsOnePageAddProduct(obj,pid,type){
	var billCycles = '';
	var cycles = jQuery("#linkContainOpc"+pid).attr("billcycle");
	if(typeof(cycles) != "undefined"){
		billCycles = cycles;
	}
	jQuery("#productBoxWgs").find(".check_box").removeClass("active-bill");
	if(jQuery(obj).hasClass("billCycleHit")){
		var icountGet = jQuery("#wgs-opc-for-i-"+pid).attr("icount");
		if(billCycles == ''){
			jQuery("#modalBillingCycleAlert").modal('show');	
		}else{
			jQuery(obj).find("i").toggleClass("fa-file-invoice fa-spinner fa-spin");
			setTimeout(function() {
				jQuery(obj).html(''+wgsOnePageBillingCycleChange+' <i class="fas fa-file-invoice"></i>');	
			}, 2000);
			jQuery("#frmConfigureProduct input[name='billingcycle']").attr("checked", false);
			jQuery("#frmConfigureProduct input[name='billingcycle']").each(function(){
				if(jQuery(this).val() == billCycles){
					jQuery(this).attr("checked", true);
					jQuery(this).prop("checked", true);
				}
			});
		}
		wgsCallAjaxForConfigureProductBillingCycle(icountGet,billCycles);
	}else{
		jQuery(obj).find("i").toggleClass("fa-plus-circle fa-spinner fa-spin");
		wgsAddProductGetProductRelatedInfo(obj,pid,billCycles);
		setTimeout(function() {
			if (type == 'liston') {
				jQuery(obj).addClass("billCycleHit");
				jQuery(obj).html(''+wgsOnePageBillingCycleChange+' <i class="fas fa-file-invoice"></i>');				
			}else{
                jQuery(obj).css("pointer-events", "none");
                jQuery(obj).html('' + wgsOnePageadded);				
			}	
		}, 3000);
	}
}
/*********** function to change billing cycle *******/
function wgsCallAjaxForConfigureProductBillingCycle(count,cycles){
  jQuery.post("cart.php", 'ajax=1&a=confproduct&configure=true&i='+count+'&billingcycle='+cycles+'',
   function (data) {
	   if (data != '') {
		   wgsGetProductConfigOption(count);
		   jQuery('#containerProductValidationErrorsList').removeClass('hidden').html(data);
			jQuery('html, body').animate({
				scrollTop: jQuery("#containerProductValidationErrorsList").offset().top
			}, 1000);		   
		   cartsummary();
	   } else {
		   wgsGetProductConfigOption(count);
		   jQuery('#containerProductValidationErrorsList').addClass('hidden').html('');
		   cartsummary();
	   }
   });
}
/************ function to change the renew cycle flow ********/
function wgsOpcChangeBillCyclesRenew(obj){
	var firstParam = jQuery('option:selected', obj).attr('domainopc');
	var secondParam = jQuery('option:selected', obj).attr("priceopc");
	var thirdParam = parseInt(jQuery('option:selected', obj).attr("yearopc"));
	var fourthParam = jQuery('option:selected', obj).attr("conditionopc");
	selectDomainPeriodInCart(firstParam,secondParam,thirdParam,fourthParam);
	wgsDomainPeriodChanged();
}
/**** function to check domain required field before submit V1.0.7 ******/
function wgsConfigDomainOptionCheckRequired() {
   var btnTxt = jQuery('a.con-btn').text();
   jQuery('a.con-btn').addClass("wgsDisable");
   jQuery('a.con-btn').html(btnTxt + ' <i class="fa fa-spinner fa-spin"></i>');
   var post = WHMCS.http.jqClient.post("cart.php", 'ajax=1&a=confdomains&' + jQuery("#frmConfigureDomains").serialize());
    post.done(
	    function (data) {
			if(jQuery.trim(data) != ''){
			   jQuery("#domainConfigureCheck").val(1);
			   jQuery('#validation_error').addClass('alert alert-danger').html('<ul>' + data + '</ul>');
			   jQuery('html, body').animate({scrollTop: jQuery("#validation_error").offset().top - 30}, 500);
			   jQuery('a.con-btn').removeClass("wgsDisable");
			   jQuery('a.con-btn').html(btnTxt);
			}else{
				jQuery("#domainConfigureCheck").val(0);
			}
	    }
    );
}
/** function  to fetch product billing cycle V1.0.8 *****/
function wgsFetchProductCycleForSummary(prodId,counts,cycleName){
	jQuery.ajax({
		type: "POST",
		url: 'cart.php?a=checkout',
		data:{
			'actionOnePage':'callAjaxMethod',
			'methodOnePage':'getProductBillingCycleForSummary',
			'pid':prodId,
			'icount':counts,
		},
		success:function (data) {
			var decodeJson = jQuery.parseJSON(data);
			if(jQuery('#wgsBillCycleDivProductQuery').length > 0){
				jQuery("#wgsBillCycleDivProductQuery").html(decodeJson.radioChecksummaryDropDownBillCycle);
				if(decodeJson.showDivCycle == 'yes'){
					jQuery("#wgsBillCycleDivProductQuery").removeClass("hidden");					
				}else{
				   jQuery("#wgsBillCycleDivProductQuery").addClass("hidden"); 
				}
				jQuery("span#billSummaryDropDown"+prodId).html(cycleName);
			}else{
				jQuery("span#billSummaryDropDown"+prodId).html(decodeJson.summaryDropDownBillCycle);	
			}
		},                                 		         
	});	
}

function wgsOnePageChangeCycleFromSummary(obj,pid){
	var billCycles = jQuery(obj).val();
	var icountGet = jQuery("#wgs-opc-for-i-"+pid).attr("icount");
	wgsCallAjaxForConfigureProductBillingCycle(icountGet,billCycles);
}