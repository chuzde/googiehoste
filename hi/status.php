<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>GoogieHost Server Status</title>
<meta name="description" content="Free Unlimited cPanel web hosting for everyone 20GB Disk Space, 200GB Bandwidth, PHP 5.4, MySQL, FTP, E-mail accounts ">
<meta name="keywords" content="free web hosting, how to create a website, how to make a website, free web hosting sites, free website hosting, free hosting sites, domain hosting, No Ads php hosting, free hosting server, free web hosting india, free email hosting, hosting website, how to make website, free cpanel hosting, what is web hosting, Unlimited WHM Reseller account, Cheap Reseller Hosting, Cloud Reseller Hosting, Best free web hosting, Cheap cPanel Hosting, free cloud hosting, Affordable Web Hosting, free site hosting"/>
<meta name="author" content="GoogieHost.com">
<link rel="image_src" href="https://www.googiehost.com/thumb.jpg" />
    <link rel="shortcut icon" href="images/icons/favicon.png" />
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/animate.min.css" />
    <link rel="stylesheet" href="css/morphext.css" />
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <link rel="stylesheet" href="css/slicknav.css">
    <link rel="stylesheet" href="style.css" />
    <script src="js/vendor/modernizr.js"></script>
		<!--Google analytics-->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36866638-1']);
  _gaq.push(['_setDomainName', '.googiehost.com']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!--End Google analytics-->
<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"nQZkh1aMQV00G7", domain:"googiehost.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="images/atrk.gif" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->
  </head>
  <body>


<!--  END OF MESSAGES ABOVE HEADER IMAGE -->

<div class="top">
  <div class="row">
  <div class="small-12 large-3 medium-3 columns">
   <div class="logo">
   <a href="index.html" title=""><img src="images/logo.png" alt="logo" title="Homepage"/></a>
   </div>
</div>

<div class="small-12 large-9 medium-9 columns">

    <nav class="desktop-menu">
     <ul class="sf-menu">
		 <li><a href="freehosting.html">Free Hosting</a>
		 <li><a href="freehosting.html">Website Design</a>
		 <li><a href="premiumhosting.html">Paid Hosting</a></li>
		 <li><a href="referral.html">Referral</a></li>
		 <li><a href="https://client.googiehost.com/clientarea.php">Login</a></li>
         <li><a href="signup.html">Signup</a></li>
		 <li><a href="contact.html">Support</a></li>
    </ul>
  </nav>
<!--  END OF NAVIGATION MENU AREA --> 

<!--  MOBILE MENU AREA -->
  <nav class="mobile-menu">
    <ul>
        <li><a href="freehosting.html">Free Hosting</a>
		<li><a href="freehosting.html">Website Design</a>
		 <li><a href="premiumhosting.html">Paid Hosting</a></li>
		 <li><a href="referral.html">Referral</a></li>
		 <li><a href="https://client.googiehost.com/clientarea.php">Login</a></li>
         <li><a href="signup.html">Signup</a></li>
		 <li><a href="contact.html">Support</a></li>
    </ul>
  </nav>
  <!--  END OF MOBILE MENU AREA -->


  </div>
  </div>
  </div>

</header>  

<!--  END OF HEADER -->  



</section>
<!--  Google Ads -->

<!-- COMPARISON  -->
<div class="pricingboxes-comparison">
<div class="row"> 
<div class="small-12 columns">
<br><br><br><br>
<h2>Thank you for choosing <a href="https://googiehost.com">free hosting</a></h2>

<!--  Google Ads  -->
<section class="google-ads">
<div class="row">
<center>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- pages -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3431396059777650"
     data-ad-slot="9163787323"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>

</div>
<align ="center">
<?php
echo "<table><tr><th>Monitor Name</th><th>Status</th><th>Total Uptime</th></tr>";
$apiKey = "u82276-fd9b862ad297e19ff5b05280";
$url = "https://api.uptimerobot.com/getMonitors?apiKey=" . $apiKey . "&format=xml";
$xml = file_get_contents($url);
$xml = new SimpleXMLElement ($xml);
foreach($xml->monitor as $monitor) {
  echo "<tr>";
  echo "<td>";
  echo $monitor['friendlyname'];
  echo "</td><td>";
  if ($monitor['status'] == 2) {
    echo "Online";
  }
  elseif ($monitor['status'] == 9) {
    echo "Offline";
  }
  else {
    echo "Not Available";
  }
  echo "</td><td>";
  if ($monitor['alltimeuptimeratio'] > 95) {
    echo "<b style=\"color:green;\">" . $monitor['alltimeuptimeratio'] . "%</b></td></tr>";
  }
  else {
    echo "<b style=\"color:red;\">" . $monitor['alltimeuptimeratio'] . "%</b></td></tr>";
  }
}
echo "</table>";
?>
</center>
</div>
</div>
</div>



</div>

</div>

<!--  END OF COMPARISON  -->



<!--  FOOTER  -->
<footer>


<div class="row">
<div class="small-12 columns">
<div class="footerlinks"> 
<div class="small-12 large-3 medium-3 columns border-right">
<h2>About GoogieHost</h2>
<ul>
<li><a href="about.html">About US</a></li>
<li><a href="team.html">Our Team Geeks</a></li>
<li><a href="terms.html">Terms of use</a></li>
<li><a href="privacy.html">Privacy Policy</a></li>
</ul>
</div>

<div class="small-12 large-3 medium-3 columns border-right">
<h2>GoogieHost International</h2>
<ul>
<li><a href="https://googiehost.com/fr">Français</a></li>
<li><a href="https://googiehost.com/es">Español</a></li>
<li><a href="https://googiehost.com/id">Indonesia</a></li>
<li><a href="https://googiehost.com/it">Italiano</a></li>
</ul>
</div>

<div class="small-12 large-3 medium-3 columns border-right">
<h2>Free Hosting Clients</h2>
<ul>
<li><a href="https://googiehost.com/de">Deutsch</a></li>
<li><a href="freedomains.html">Free Domain Name</a></li>
<li><a href="status.php">Check Server Uptime</a></li>
<li><a href="https://googiehost.com/blog/">Support Blog</a></li>


</ul>
</div>

<div class="small-12 large-3 medium-3 columns border-left">
<h2>Premium Client Section</h2>
<ul>
<li><a href="https://client.googiehost.com/clientarea.php">Login to Dashboard</a></li>
<li><a href="https://client.googiehost.com/register.php">Create Account</a></li>
<li><a href="https://client.googiehost.com/pwreset.php">Forget Password</a></li>
<li><a href="https://client.googiehost.com/">Domain Checker</a></li>
</ul>
<!--End mc_embed_signup-->
</div>

</div>
</div>
</div>
</br>
<center><a href="https://www.facebook.com/GoogieHost"><img src="images/fb-icon.png"/></a> <a href="https://plus.google.com/+GoogiehostFree"><img src="images/google-plus-icon.png"/></a> <a href="https://twitter.com/GoogieHost"><img src="images/twitter-icon.png"/></a> </center>
<p class="copyright">© Copyrights 2011-2021 GoogieHost, All Rights Reserved. </p>
</footer>
<!--  END OF FOOTER  -->

<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/vendor/hoverIntent.js"></script>
    <script src="js/vendor/superfish.min.js"></script>
    <script src="js/vendor/morphext.min.js"></script>
    <script src="js/vendor/wow.min.js"></script>
    <script src="js/vendor/jquery.slicknav.min.js"></script>
    <script src="js/vendor/waypoints.min.js"></script>
    <script src="js/vendor/jquery.animateNumber.min.js"></script>
    <script src="js/vendor/owl.carousel.min.js"></script>
    <script src="js/vendor/jquery.slicknav.min.js"></script>
    <script src="js/custom.js"></script>

  </body>
</html>