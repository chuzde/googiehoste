<?php
if(isset($_POST['email'])) {

	// CHANGE THE TWO LINES BELOW
	$email_to = "sales@youstable.com";

	$email_subject = "GoogieHost Online Infomation Desk";


	function died($error) {
		// your error code can go here
		echo "We're sorry, but there's errors found with the form you submitted.<br /><br />";
		echo $error."<br /><br />";
		echo "Please mail your query at .<br /><br />";
		die();
	}

	// validation expected data exists
	if(!isset($_POST['fname']) ||
		!isset($_POST['lname']) ||
			!isset($_POST['weburl']) ||
			!isset($_POST['phone']) ||
			!isset($_POST['email']) ||
			!isset($_POST['about'])) {
		died('We are sorry, but there appears to be a problem with the form you submitted.');
	}
        
        $fname = $_POST['fname'];
        $lname = $_POST['lname'];
        $weburl = $_POST['weburl'];
        $phone = $_POST['phone'];
        $goals = $_POST['goals'];
        $email = $_POST['email'];
        $about = $_POST['about'];

	$error_message = "";
	$string_exp = "/^[A-Za-z .'-]+$/";
  if(!preg_match($string_exp,$fname)) {
  	$error_message .= 'The First Name you entered does not appear to be valid.<br />';
  }
  if(strlen($about) < 2) {
  	$error_message .= 'The Comments you entered do not appear to be valid.<br />';
  }
  if(strlen($error_message) > 0) {
  	died($error_message);
  }
	$email_message = "Form details below.\n\n";

	function clean_string($string) {
	  $bad = array("content-type","bcc:","to:","cc:","href");
	  return str_replace($bad,"",$string);
	}


    $email_message .= "First Name: ".clean_string($fname)."\n";
    $email_message .= "First Name: ".clean_string($lname)."\n";
	$email_message .= "First Name: ".clean_string($name)."\n";
	$email_message .= "Website Url: ".clean_string($weburl)."\n";
	$email_message .= "Phone: ".clean_string($phone)."\n";
	$email_message .= "Goals: ".clean_string($goals)."\n";
	$email_message .= "Email: ".clean_string($email)."\n";
	$email_message .= "About: ".clean_string($about)."\n";
	

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);
?>

<!-- place your own success html below -->

Thank you for contacting GoogieHost. We will be in touch with you very soon.</br>
<b>PLEASE WAIT: I AM REDIRECTING ON THE PREVIOUS PAGE :)</b>
<meta http-equiv="refresh" content="1; url=https://googiehost.com/websitedesign.html" />

<?php
}
die();
?>
