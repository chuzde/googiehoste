jQuery('.is-responder-form').on('submit', function(e) {
    e.preventDefault();
    var the_form = jQuery(this),
        the_email = the_form.find('input[type="email"]');
    if (the_email.val().length <= 0 || !checkmail(the_email.val())) {
        jQuery(this).find('.error-notes').fadeIn().css('display', 'block');
        the_form.addClass('validation-error');
        the_email.attr('aria-invalid', 'true');
        return !1
    }
    jQuery(this).find('.error-notes').fadeOut().css('display', 'none');
    the_form.removeClass('validation-error');
    the_email.attr('aria-invalid', 'false');
    jQuery.ajax({
        type: 'POST',
        url: global.ajax_url,
        data: {
            action: 'get_info_from_form',
            user_email: the_form.find('input[type="email"]').val()
        },
        beforeSend: function() {
            jQuery.when(the_form.find('input[type="submit"]').fadeOut('fast')).then(function() {
                the_form.find('.loader').fadeIn().addClass('inline-bl-pos')
            })
        },
        success: function() {
            jQuery.when(the_form.find('.loader').fadeOut('fast').removeClass('inline-bl-pos')).then(function() {
                the_form.find('input[type="submit"]').attr('disabled', 'disabled').addClass('form-submitted').fadeIn()
            })
        },
        error: function() {
            jQuery('.error-notes').fadeIn()
        }
    })
});

function checkmail(string) {
    var email_patt = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return email_patt.test(string)
};
(function(w, d, $) {
    var tables = {
        init: function() {
            this.items = d.getElementsByClassName('pricing-webhosting');
            this.isRtl = document.body.classList.contains('rtl');
            this.state = '';
            var that = this;
            this.changeState();
            for (var el of this.items) {
                el.addEventListener('click', function(e) {
                    if (e.target.classList.contains('pricing-webhosting-title')) {
                        e.stopPropagation();
                        var parent = e.target.parentElement;
                        if (parent.classList.contains('open')) {
                            parent.classList.remove('open')
                        } else {
                            parent.classList.add('open')
                        }
                    }
                })
            }
        },
        getState: function() {
            return (w.innerWidth > 600) ? 't' : 'c'
        },
        setState: function(state) {
            this.state = state
        },
        changeState: function() {
            if (this.state === this.getState()) {
                return
            }
            if ('t' === this.getState()) {
                this.setState('t');
                this.initTable()
            } else {
                this.setState('c');
                this.initCarousel()
            }
        },
        initTable: function() {
            for (var el of this.items) {
                var body = el.querySelector('.pricing-webhosting-body');
                if (typeof body === 'undefined') {
                    continue
                }
                $(body).removeAttr('data-dots').trigger('destroy.owl.carousel')
            }
        },
        initCarousel: function() {
            var that = this;
            for (var el of this.items) {
                var body = el.querySelector('.pricing-webhosting-body');
                if (typeof body === 'undefined') {
                    continue
                }
                $(body).owlCarousel({
                    rtl: that.isRtl,
                    loop: !1,
                    dots: !0,
                    items: 1,
                    nav: !0,
                    onChanged: function(e) {
                        that.setButtonLink(e)
                    },
                    onInitialized: function(e) {
                        that.setButtonLink(e)
                    },
                })
            }
        },
        setButtonLink: function(e) {
            var hrefValue = $(e.target).find('.owl-item.active').children('a').eq(0).attr('href');
            $(e.target).parent().parent().find('.pricing-webhosting-link').attr({
                href: hrefValue
            })
        },
    };
    tables.init();
    w.addEventListener('resize', function(e) {
        tables.changeState()
    })
})(window, document, jQuery);
jQuery('.static-contact-form').on('submit', function(e) {
    e.preventDefault();
    var isMobile = !1;
    var container = jQuery(this).closest('.contact-us-block');
    if (container.length > 0 && container.hasClass('contact-us-block-mobile')) {
        isMobile = !0
    }
    var form_data = {},
        valid = !0;
    valid = !0;
    container.find('fieldset[data-req="req"]').each(function() {
        var input = jQuery(this).find('input');
        input.attr('aria-invalid', 'false');
        if (input.val().length < 1 || (input.attr('type') === 'email' && !checkmail(input.val()))) {
            valid = !1;
            input.attr('aria-invalid', 'true')
        }
    });
    if (!valid) {
        return !1
    }
    form_data = {
        action: 'send_static_contact_form',
        name: container.find('input[name="your-name"]').val().trim(),
        email: container.find('input[name="your-email"]').val().trim(),
        subject: container.find('input[name="your-subject"]').val().trim(),
        message: container.find('textarea[name="your-message"]').val().trim()
    };
    jQuery.ajax({
        type: 'post',
        url: global.ajax_url,
        data: form_data,
        beforeSend: function() {
            jQuery('.error-notes').fadeOut();
            jQuery.when(jQuery('.static-contact-form').find('input[type="submit"]').fadeOut('fast')).then(function() {
                jQuery('.static-contact-form').find('.loader').fadeIn()
            })
        },
        success: function() {
            jQuery.when(jQuery('.static-contact-form').find('.loader').fadeOut('fast')).then(function() {
                jQuery('.static-contact-form').find('input[type="submit"]').attr('disabled', 'disabled').addClass('form-submitted').fadeIn();
                jQuery('.static-contact-form').find('.loader').fadeOut()
            })
        },
        error: function() {
            jQuery('.error-notes').fadeIn();
            jQuery('.static-contact-form').find('.loader').fadeOut()
        }
    })
});
(function($) {
    $(document).ready(function($) {
        var $contents = $('.new-post-content a[href^="#"], .new-vendor-content a[href^="#"], .top10-content a[href^="#"]');
        if ($contents.length > 0) {
            $('img.img-lazy-load, .bg-lazy-load').each(function() {
                $(this).attr('src', $(this).attr('data-src'));
                $(this).load(function() {
                    $(this).css('opacity', '1')
                })
            });
            $('iframe.img-lazy-load').each(function() {
                $(this).attr('src', $(this).attr('data-src'));
                $(this).load(function() {
                    $(this).css('opacity', '1')
                })
            });
            $contents.click(function(e) {
                e.preventDefault();
                history.pushState({}, '', $(this).attr('href'));
                setTimeout(function() {
                    scrollToElementIfHashed()
                }, 600)
            })
        }
    });

    function scrollToElementIfHashed() {
        var $target = $(document),
            top = 0;
        if (typeof document.location.hash === 'string' && document.location.hash.length > 0) {
            var $el = $(document.location.hash);
            if ($el.length > 0) {
                $target = $el;
                top = $target.offset().top - 130
            }
        }
        $('html, body').animate({
            scrollTop: top
        }, 400)
    }
})(jQuery)

function checkmail(string) {
    var email_patt = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return email_patt.test(string)
}
jQuery('.content-toggle-title').click(function() {
    jQuery(this).parents('.content-toggle').stop().toggleClass('active').find('.content-toggle-inner').stop().slideToggle()
});
(function(d, w, $) {
    var width = $(w).width();
    var $shortcodes = $('.top10-vendor-shortcode-table-body');
    var Counter = function() {
        var i = 0;
        return function() {
            return i++
        }
    };
    var counter = Counter();
    $(d).ready(function() {
        changeTable();
        $shortcodes.find('.stars-rating').raty({
            'starOn': 'star full-star',
            'starHalf': 'star half-star',
            'starOff': 'star empty-star',
            'half': !0,
            'hints': ['', '', '', '', ''],
            score: function() {
                return $(this).attr('data-raty-score')
            },
            readOnly: function() {
                return !0
            }
        })
    });
    $(w).resize(function() {
        width = $(w).width();
        changeTable()
    });

    function changeTable() {
        if ($shortcodes.length > 0 && width < 601) {
            $shortcodes.each(function() {
                if (typeof $(this).attr('data-dots') === 'undefined') {
                    var id = 'top10-vendor-shortcode-table-dots-' + counter();
                    $(this).attr('data-dots', '#' + id);
                    $(this).parent('.top10-vendor-shortcode-table').after('<div class="top10-vendor-shortcode-table-dots" id="' + id + '"></div>')
                }
                $(this).owlCarousel({
                    loop: !1,
                    rtl: $('body').hasClass('rtl'),
                    margin: 0,
                    nav: !0,
                    dots: !0,
                    items: 1,
                    dotsContainer: $(this).attr('data-dots'),
                })
            })
        } else if ($shortcodes.length > 0) {
            $shortcodes.each(function() {
                $(this).trigger('destroy.owl.carousel');
                $(this).removeAttr('data-dots')
            })
        }
    }
})(document, window, jQuery);
(function($, d, w) {
    $(d).ready(function() {
        $('.shortcode-faq-question').click(function(e) {
            e.preventDefault()
        })
    })
})(jQuery, document, window);
jQuery(document).ready(function() {
    jQuery(".videoshortcode .overlayer").on("click", function(ev) {
        form_data = {
            action: 'update_video_count',
            id: jQuery(this).attr('data-id'),
            key: jQuery(this).attr('data-key')
        };
        jQuery.ajax({
            type: 'post',
            url: global.ajax_url,
            data: form_data,
            success: function(res) {
                console.log(res)
            },
        });
        jQuery(this).hide();
        jQuery(this).parent().find("iframe")[0].src += "?autoplay=1;start=0";
        ev.preventDefault()
    })
});
jQuery('.sections-navigation a').on('click', function(e) {
    var scrollto = jQuery(this).attr('href'),
        offset = jQuery('#masthead').outerHeight();
    if (jQuery('body').hasClass('admin-bar')) {
        offset += jQuery('#wpadminbar').outerHeight()
    }
    e.preventDefault();
    jQuery('html, body').animate({
        scrollTop: jQuery(scrollto).offset().top - offset - 20
    }, 2000)
});

function setCookieGclid(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d;
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/"
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1)
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length)
        }
    }
    return "";
    []
}

function close30days() {
    jQuery('.donateform').hide();
    var now = new Date();
    var time = now.getTime();
    var expireTime = time + 30 * 24 * 60 * 60 * 1000;
    now.setTime(expireTime);
    document.cookie = 'closedonate=true;expires=' + now.toGMTString() + ';path=/'
}
jQuery(document).ready(function() {
    jQuery('#donate-hide').click(function(event) {
        event.preventDefault();
        close30days()
    })
});

function aloomaReportPageView(trackType, trackData) {
    if (typeof trackType !== 'undefined') {
        alooma.oldalooma.track(trackType, trackData);
        alooma.newiknowlogy.track(trackType, trackData)
    } else {
        console.log("Alooma Track PageView: " + dataAloomaIds.pageviewRandomId + ", id: " + dataAloomaIds.serverPageData.id + ", lang: " + dataAloomaIds.serverPageData.lang + ", title: " + dataAloomaIds.serverPageData.title + ", type: " + dataAloomaIds.serverPageData.type);
        alooma.oldalooma.track("pageview", {
            "pageview_id": dataAloomaIds.pageviewRandomId,
            "id": dataAloomaIds.serverPageData.id,
            "lang": dataAloomaIds.serverPageData.lang,
            "title": dataAloomaIds.serverPageData.title,
            "type": dataAloomaIds.serverPageData.type,
            "userAgent": window.navigator.userAgent,
            "splitCurrentUrl": splitCurrentUrl
        });
        alooma.newiknowlogy.track("pageview", {
            "pageview_id": dataAloomaIds.pageviewRandomId,
            "id": dataAloomaIds.serverPageData.id,
            "lang": dataAloomaIds.serverPageData.lang,
            "title": dataAloomaIds.serverPageData.title,
            "type": dataAloomaIds.serverPageData.type,
            "userAgent": window.navigator.userAgent,
            "splitCurrentUrl": splitCurrentUrl
        })
    }
}

function aloomaReportClickout(clickedObject, vendorName, btnName) {
    var clickout_id = clickedObject.getAttribute("clickout_id");
    var pageview_id = clickedObject.getAttribute("pageview_id");
    var pos = ' ';
    var deeplink = clickedObject.getAttribute("data-deep-name");
    if (deeplink == null || deeplink == undefined) {
        deeplink = ''
    }
    if (document.body.classList.contains('single-landing-page')) {
        var elementposition = findAncestor(clickedObject, 'lp-sigle-tool');
        if (elementposition != undefined) {
            pos = elementposition.getAttribute("data-position")
        }
    }
    alooma.oldalooma.track("clickout", {
        "pageview_id": pageview_id,
        "clickout_id": clickout_id,
        "btnName": btnName,
        "pos": pos,
        "vendor": vendorName,
        "deeplink": deeplink,
        "id": dataAloomaIds.serverPageData.id,
        "lang": dataAloomaIds.serverPageData.lang,
        "title": dataAloomaIds.serverPageData.title,
        "type": dataAloomaIds.serverPageData.type,
        "splitCurrentUrl": splitCurrentUrl
    });
    alooma.newiknowlogy.track("clickout", {
        "pageview_id": pageview_id,
        "clickout_id": clickout_id,
        "btnName": btnName,
        "pos": pos,
        "vendor": vendorName,
        "deeplink": deeplink,
        "id": dataAloomaIds.serverPageData.id,
        "lang": dataAloomaIds.serverPageData.lang,
        "title": dataAloomaIds.serverPageData.title,
        "type": dataAloomaIds.serverPageData.type,
        "splitCurrentUrl": splitCurrentUrl
    });
    console.log("Alooma ClickOUT, btnName: " + btnName + ", pos: " + pos + "  pageview_id: " + pageview_id + ", clickout_id: " + clickout_id + ", vendor: " + vendorName + ", deeplink : " + deeplink + ", id: " + dataAloomaIds.serverPageData.id + ", lang: " + dataAloomaIds.serverPageData.lang + ", title: " + dataAloomaIds.serverPageData.title + ", type: " + dataAloomaIds.serverPageData.type)
}

function findAncestor(el, cls) {
    while ((el = el.parentElement) && !el.classList.contains(cls));
    return el
}

function trackClickout(hitType, category, action, label, clickedObject, trackAlooma) {
    event.stopPropagation();
    var labelL = label.toLowerCase().replace(' ', '-');
    ga('send', {
        hitType: hitType,
        eventCategory: category,
        eventAction: action,
        eventLabel: labelL,
    });
    var deeplink = clickedObject.getAttribute('data-deeplink');
    if (deeplink == 'undefined' || deeplink == null) {
        deeplink = ''
    }
    if (trackAlooma) {
        aloomaReportClickout(clickedObject, label, action, deeplink)
    }
}

function aloomaTrackPageview(trackType, trackData, redirectUrl) {
    if (typeof alooma !== 'undefined' && (alooma.oldalooma.track || alooma.newiknowlogy.track)) {
        trackData.verbose = !0;
        if (typeof window.chosenVariation !== 'undefined') {
            trackData.chosenVariation = window.chosenVariation
        }
        if (redirectUrl) {
            window.alooma.oldalooma.track(trackType, trackData, function() {
                window.location = redirectUrl
            });
            window.alooma.newiknowlogy.track(trackType, trackData, function() {
                window.location = redirectUrl
            })
        } else {
            window.alooma.oldalooma.track(trackType, trackData);
            window.alooma.newiknowlogy.track(trackType, trackData)
        }
    } else {
        setTimeout(function() {
            aloomaTrackPageview(trackType, trackData, redirectUrl)
        }, 50)
    }
}

function delPrm(Url, Prm) {
    var a = Url.split('?');
    var re = new RegExp('(\\?|&)' + Prm + '=[^&]+', 'g');
    Url = ('?' + a[1]).replace(re, '');
    Url = Url.replace(/^&|\?/, '');
    var dlm = (Url == '') ? '' : '?';
    return a[0] + dlm + Url
};

function addAloomaClickoutTrackingCodes() {
    jQuery('.clickoutLink').each(function() {
        var $this = jQuery(this);
        var clickoutRandomSuffix = dataAloomaIds.clickoutRandomIdPrefix++;
        $this.attr("pageview_id", dataAloomaIds.pageviewRandomId);
        $this.attr("clickout_id", dataAloomaIds.pageviewRandomId + "_" + clickoutRandomSuffix);
        if ($this.attr("href")) {
            var _href = $this.attr("href");
            if (_href.includes("pageview_id")) {
                delPrm(_href, 'pageview_id')
            }
            $this.attr("href", _href + '&pageview_id=' + dataAloomaIds.pageviewRandomId);
            var _href = $this.attr("href");
            if (_href.includes("clickout_id")) {
                delPrm(_href, 'clickout_id')
            }
            $this.attr("href", _href + '&clickout_id=' + dataAloomaIds.pageviewRandomId + "_" + clickoutRandomSuffix)
        }
    })
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName, i;
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? !0 : sParameterName[1]
        }
    }
};

function checkCookie() {
    var urlsparam = {};
    urlsparam.campaign = '';
    urlsparam.adgroup = '';
    urlsparam.keyword = '';
    urlsparam.gclid = '';
    var ampre = '\?';
    var gclid = getUrlParameter('gclid');
    if (gclid == '' || gclid == undefined) {
        gclid = getCookie('gclid')
    }
    jQuery.each(urlsparam, function(key, value) {
        value = getUrlParameter(key);
        if (value != '' && value != undefined) {
            jQuery('a[href*="reviewit.php"]').each(function() {
                var $this = jQuery(this);
                var _href = $this.attr("href");
                if (_href.includes(key + '=&')) {
                    var _href = _href.replace("" + key + "=&", "")
                }
                $this.attr("href", _href + "&" + key + '=' + value)
            });
            jQuery('a.textdeeplink.text-center.linkout-link').each(function() {
                var $this = jQuery(this);
                var _href = $this.attr("href");
                if (_href.includes(key + '=&')) {
                    var _href = _href.replace("" + key + "=&", "")
                }
                $this.attr("href", _href + "?" + key + '=' + value);
                ampre = '\&'
            });
            setCookieGclid(key, value, 1)
        }
    })
}
var QueryString = function() {
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1])
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]))
        }
    }
    return query_string
}();

function createPrerender(strUrl) {
    var pre = document.createElement("link");
    pre.setAttribute("rel", "prerender");
    pre.setAttribute("href", strUrl);
    document.getElementsByTagName("head")[0].appendChild(pre);
    var pre = document.createElement("link");
    pre.setAttribute("rel", "prefetch");
    pre.setAttribute("href", strUrl);
    document.getElementsByTagName("head")[0].appendChild(pre)
}

function checkCategory() {
    var catid = jQuery("#current-vendor-en-term-id").val()
}
jQuery(document).ready(function() {
    addAloomaClickoutTrackingCodes();
    checkCookie();
    checkCategory();
    if (jQuery('.donateform').length > 0 && !getCookie('closedonate')) {
        jQuery('.donateform').show()
    }
    setTimeout(function() {
        aloomaReportPageView()
    }, 50)
});
(function() {
    var isIe = /(trident|msie)/i.test(navigator.userAgent);
    if (isIe && document.getElementById && window.addEventListener) {
        window.addEventListener('hashchange', function() {
            var id = location.hash.substring(1),
                element;
            if (!(/^[A-z0-9_-]+$/.test(id))) {
                return
            }
            element = document.getElementById(id);
            if (element) {
                if (!(/^(?:a|select|input|button|textarea)$/i.test(element.tagName))) {
                    element.tabIndex = -1
                }
                element.focus()
            }
        }, !1)
    }
})();
(function($) {
    var methods = {
        init: function(settings) {
            return this.each(function() {
                methods.destroy.call(this);
                this.opt = $.extend(!0, {}, $.fn.raty.defaults, settings);
                var that = $(this),
                    inits = ['number', 'readOnly', 'score', 'scoreName'];
                methods._callback.call(this, inits);
                if (this.opt.precision) {
                    methods._adjustPrecision.call(this)
                }
                this.opt.number = methods._between(this.opt.number, 0, this.opt.numberMax);
                this.stars = methods._createStars.call(this);
                this.score = methods._createScore.call(this);
                methods._apply.call(this, this.opt.score);
                if (this.opt.cancel) {
                    this.cancel = methods._createCancel.call(this)
                }
                if (this.opt.width) {
                    that.css('width', this.opt.width)
                }
                if (this.opt.readOnly) {
                    methods._lock.call(this)
                } else {
                    that.css('cursor', 'pointer');
                    methods._binds.call(this)
                }
                methods._target.call(this, this.opt.score);
                that.data({
                    'settings': this.opt,
                    'raty': !0
                })
            })
        },
        _adjustPrecision: function() {
            this.opt.targetType = 'score';
            this.opt.half = !0
        },
        _apply: function(score) {
            if (typeof score !== 'undefined' && score >= 0) {
                score = methods._between(score, 0, this.opt.number);
                this.score.val(score)
            }
            methods._fill.call(this, score);
            if (score) {
                methods._roundStars.call(this, score)
            }
        },
        _between: function(value, min, max) {
            return Math.min(Math.max(parseFloat(value), min), max)
        },
        _binds: function() {
            if (this.cancel) {
                methods._bindCancel.call(this)
            }
            methods._bindClick.call(this);
            methods._bindOut.call(this);
            methods._bindOver.call(this)
        },
        _bindCancel: function() {
            methods._bindClickCancel.call(this);
            methods._bindOutCancel.call(this);
            methods._bindOverCancel.call(this)
        },
        _bindClick: function() {
            var self = this,
                that = $(self);
            self.stars.on('click.raty', function(evt) {
                self.score.val((self.opt.half || self.opt.precision) ? that.data('score') : $(this).data('score'));
                if (self.opt.click) {
                    self.opt.click.call(self, parseFloat(self.score.val()), evt)
                }
            })
        },
        _bindClickCancel: function() {
            var self = this;
            self.cancel.on('click.raty', function(evt) {
                self.score.removeAttr('value');
                if (self.opt.click) {
                    self.opt.click.call(self, null, evt)
                }
            })
        },
        _bindOut: function() {
            var self = this;
            $(this).on('mouseleave.raty', function(evt) {
                var score = parseFloat(self.score.val()) || undefined;
                methods._apply.call(self, score);
                methods._target.call(self, score, evt);
                if (self.opt.mouseout) {
                    self.opt.mouseout.call(self, score, evt)
                }
            })
        },
        _bindOutCancel: function() {
            var self = this;
            self.cancel.on('mouseleave.raty', function(evt) {
                $(this).attr('class', self.opt.cancelOff);
                if (self.opt.mouseout) {
                    self.opt.mouseout.call(self, self.score.val() || null, evt)
                }
            })
        },
        _bindOverCancel: function() {
            var self = this;
            self.cancel.on('mouseover.raty', function(evt) {
                $(this).attr('class', self.opt.cancelOn);
                self.stars.attr('class', self.opt.starOff);
                methods._target.call(self, null, evt);
                if (self.opt.mouseover) {
                    self.opt.mouseover.call(self, null)
                }
            })
        },
        _bindOver: function() {
            var self = this,
                that = $(self),
                action = self.opt.half ? 'mousemove.raty' : 'mouseover.raty';
            self.stars.on(action, function(evt) {
                var score = parseInt($(this).data('score'), 10);
                if (self.opt.half) {
                    var position = parseFloat((evt.pageX - $(this).offset().left) / (self.opt.size ? self.opt.size : parseInt(that.css('font-size')))),
                        plus = (position > .5) ? 1 : .5;
                    score = score - 1 + plus;
                    methods._fill.call(self, score);
                    if (self.opt.precision) {
                        score = score - plus + position
                    }
                    methods._roundStars.call(self, score);
                    that.data('score', score)
                } else {
                    methods._fill.call(self, score)
                }
                methods._target.call(self, score, evt);
                if (self.opt.mouseover) {
                    self.opt.mouseover.call(self, score, evt)
                }
            })
        },
        _callback: function(options) {
            for (var i in options) {
                if (typeof this.opt[options[i]] === 'function') {
                    this.opt[options[i]] = this.opt[options[i]].call(this)
                }
            }
        },
        _createCancel: function() {
            var that = $(this),
                icon = this.opt.cancelOff,
                cancel = $('<i />', {
                    'class': icon,
                    title: this.opt.cancelHint
                });
            if (this.opt.cancelPlace == 'left') {
                that.prepend('&#160;').prepend(cancel)
            } else {
                that.append('&#160;').append(cancel)
            }
            return cancel
        },
        _createScore: function() {
            return $('<input />', {
                type: 'hidden',
                name: this.opt.scoreName
            }).appendTo(this)
        },
        _createStars: function() {
            var that = $(this);
            for (var i = 1; i <= this.opt.number; i++) {
                var title = methods._getHint.call(this, i),
                    icon = (this.opt.score && this.opt.score >= i) ? 'starOn' : 'starOff';
                icon = this.opt[icon];
                $('<i />', {
                    'class': icon,
                    title: title,
                    'data-score': i
                }).appendTo(this);
                if (this.opt.space) {
                    that.append((i < this.opt.number) ? '&#160;' : '')
                }
            }
            return that.children('i')
        },
        _error: function(message) {
            $(this).html(message);
            $.error(message)
        },
        _fill: function(score) {
            var self = this,
                hash = 0;
            for (var i = 1; i <= self.stars.length; i++) {
                var star = self.stars.eq(i - 1),
                    select = self.opt.single ? (i == score) : (i <= score);
                if (self.opt.iconRange && self.opt.iconRange.length > hash) {
                    var irange = self.opt.iconRange[hash],
                        on = irange.on || self.opt.starOn,
                        off = irange.off || self.opt.starOff,
                        icon = select ? on : off;
                    if (i <= irange.range) {
                        star.attr('class', icon)
                    }
                    if (i == irange.range) {
                        hash++
                    }
                } else {
                    var icon = select ? 'starOn' : 'starOff';
                    star.attr('class', this.opt[icon])
                }
            }
        },
        _getHint: function(score) {
            var hint = this.opt.hints[score - 1];
            return (hint === '') ? '' : (hint || score)
        },
        _lock: function() {
            var score = parseInt(this.score.val(), 10),
                hint = score ? methods._getHint.call(this, score) : this.opt.noRatedMsg;
            $(this).data('readonly', !0).css('cursor', '').attr('title', hint);
            this.score.attr('readonly', 'readonly');
            this.stars.attr('title', hint);
            if (this.cancel) {
                this.cancel.hide()
            }
        },
        _roundStars: function(score) {
            var rest = (score - Math.floor(score)).toFixed(2);
            if (rest > this.opt.round.down) {
                var icon = 'starOn';
                if (this.opt.halfShow && rest < this.opt.round.up) {
                    icon = 'starHalf'
                } else if (rest < this.opt.round.full) {
                    icon = 'starOff'
                }
                this.stars.eq(Math.ceil(score) - 1).attr('class', this.opt[icon])
            }
        },
        _target: function(score, evt) {
            if (this.opt.target) {
                var target = $(this.opt.target);
                if (target.length === 0) {
                    methods._error.call(this, 'Target selector invalid or missing!')
                }
                if (this.opt.targetFormat.indexOf('{score}') < 0) {
                    methods._error.call(this, 'Template "{score}" missing!')
                }
                var mouseover = evt && evt.type == 'mouseover';
                if (score === undefined) {
                    score = this.opt.targetText
                } else if (score === null) {
                    score = mouseover ? this.opt.cancelHint : this.opt.targetText
                } else {
                    if (this.opt.targetType == 'hint') {
                        score = methods._getHint.call(this, Math.ceil(score))
                    } else if (this.opt.precision) {
                        score = parseFloat(score).toFixed(1)
                    }
                    if (!mouseover && !this.opt.targetKeep) {
                        score = this.opt.targetText
                    }
                }
                if (score) {
                    score = this.opt.targetFormat.toString().replace('{score}', score)
                }
                if (target.is(':input')) {
                    target.val(score)
                } else {
                    target.html(score)
                }
            }
        },
        _unlock: function() {
            $(this).data('readonly', !1).css('cursor', 'pointer').removeAttr('title');
            this.score.removeAttr('readonly', 'readonly');
            for (var i = 0; i < this.opt.number; i++) {
                this.stars.eq(i).attr('title', methods._getHint.call(this, i + 1))
            }
            if (this.cancel) {
                this.cancel.css('display', '')
            }
        },
        cancel: function(click) {
            return this.each(function() {
                if ($(this).data('readonly') !== !0) {
                    methods[click ? 'click' : 'score'].call(this, null);
                    this.score.removeAttr('value')
                }
            })
        },
        click: function(score) {
            return $(this).each(function() {
                if ($(this).data('readonly') !== !0) {
                    methods._apply.call(this, score);
                    if (!this.opt.click) {
                        methods._error.call(this, 'You must add the "click: function(score, evt) { }" callback.')
                    }
                    this.opt.click.call(this, score, $.Event('click'));
                    methods._target.call(this, score)
                }
            })
        },
        destroy: function() {
            return $(this).each(function() {
                var that = $(this),
                    raw = that.data('raw');
                if (raw) {
                    that.off('.raty').empty().css({
                        cursor: raw.style.cursor,
                        width: raw.style.width
                    }).removeData('readonly')
                } else {
                    that.data('raw', that.clone()[0])
                }
            })
        },
        getScore: function() {
            var score = [],
                value;
            $(this).each(function() {
                value = this.score.val();
                score.push(value ? parseFloat(value) : undefined)
            });
            return (score.length > 1) ? score : score[0]
        },
        readOnly: function(readonly) {
            return this.each(function() {
                var that = $(this);
                if (that.data('readonly') !== readonly) {
                    if (readonly) {
                        that.off('.raty').children('i').off('.raty');
                        methods._lock.call(this)
                    } else {
                        methods._binds.call(this);
                        methods._unlock.call(this)
                    }
                    that.data('readonly', readonly)
                }
            })
        },
        reload: function() {
            return methods.set.call(this, {})
        },
        score: function() {
            return arguments.length ? methods.setScore.apply(this, arguments) : methods.getScore.call(this)
        },
        set: function(settings) {
            return this.each(function() {
                var that = $(this),
                    actual = that.data('settings'),
                    news = $.extend({}, actual, settings);
                that.raty(news)
            })
        },
        setScore: function(score) {
            return $(this).each(function() {
                if ($(this).data('readonly') !== !0) {
                    methods._apply.call(this, score);
                    methods._target.call(this, score)
                }
            })
        }
    };
    $.fn.raty = function(method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1))
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments)
        } else {
            $.error('Method ' + method + ' does not exist!')
        }
    };
    $.fn.raty.defaults = {
        cancel: !1,
        cancelHint: 'Cancel this rating!',
        cancelOff: 'fa fa-fw fa-minus-square',
        cancelOn: 'fa fa-fw fa-check-square',
        cancelPlace: 'left',
        click: undefined,
        half: !1,
        halfShow: !0,
        hints: ['bad', 'poor', 'regular', 'good', 'gorgeous'],
        iconRange: undefined,
        mouseout: undefined,
        mouseover: undefined,
        noRatedMsg: 'Not rated yet!',
        number: 5,
        numberMax: 20,
        precision: !1,
        readOnly: !1,
        round: {
            down: .25,
            full: .6,
            up: .76
        },
        score: undefined,
        scoreName: 'score',
        single: !1,
        size: null,
        space: !0,
        starHalf: 'fa fa-fw fa-star-half-o',
        starOff: 'fa fa-fw fa-star-o',
        starOn: 'fa fa-fw fa-star',
        target: undefined,
        targetFormat: '{score}',
        targetKeep: !1,
        targetText: '',
        targetType: 'hint',
        width: !1
    }
})(jQuery);
(function($) {
    var mainMenuItems = {
        init: function() {
            var mainMenu = this;
            this.windowWidth = $(window).width();
            this.status = this.getStatus();
            this.menuItems = $('#header-main-nav > ul > li.menu-item-has-children, #header-additional-nav > ul > li.menu-item-has-children');
            this.menuMobileItems = $('#header-main-nav, #header-nav-menu-item-wpml, #header-nav-menu-item-search, #header-nav-menu-item-cur');
            this.menuMobileItems.click(function(e) {
                e.stopPropagation()
            });
            this.menuItems.find('span.header-nav-menu-item-text').click(function(e) {
                e.preventDefault();
                e.stopPropagation();
                if ('mobile' === mainMenu.status && $(this).hasClass('header-nav-menu-item-text-wpml')) {
                    return
                }
                var $target = $(this).closest('li.menu-item-has-children');
                if (!$target.hasClass('menu-item-open')) {
                    mainMenu.closeAll()
                }
                $target.toggleClass('menu-item-open');
                if (mainMenu.windowWidth < 993) {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 300)
                }
            });
            $('body').click(function() {
                mainMenu.closeAll();
                mainMenu.closeAllMobile()
            });
            $('#masthead .header-menu-mob-button, #header-nav-menu-item-additional-text-close').click(function(e) {
                e.preventDefault();
                e.stopPropagation();
                var $target = mainMenu.getMobileTargetByButtonId($(this).attr('id'));
                if (!$target.hasClass('menu-mobile-open')) {
                    mainMenu.closeAllMobile()
                }
                $target.toggleClass('menu-mobile-open');
                $(this).toggleClass('header-menu-mob-button-open')
            });
            $('#header-additional-nav .menu-currency-item, #header-nav-menu-item-search-close').click(function(e) {
                e.preventDefault();
                mainMenu.closeAllMobile();
                if ('mobile' === mainMenu.status) {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 350)
                }
            });
            $('.header-search #header-searchform').on('submit', function(e) {
                if ($('#header-searchform input[type=search]').val() == "") {
                    e.preventDefault();
                    console.log('empty search')
                }
            });
            $('#header-search-input').on('click', function(e) {
                mainMenu.closeAll()
            });
            if ($('.archive #wp-admin-bar-edit').length) {
                if ($('#wp-admin-bar-edit').text() == 'Edit Verticals Category') {
                    $('#wp-admin-bar-edit').css('display', 'none')
                }
            }
            $(window).resize(function() {
                mainMenu.windowWidth = $(window).width();
                mainMenu.status = mainMenu.getStatus()
            })
        },
        closeAll: function() {
            this.menuItems.removeClass('menu-item-open');
            $('#header-nav-menu-item-search').removeClass('autocomplete').find('#header-search-autocomplete').removeClass('show').html('');
            return this
        },
        closeAllMobile: function() {
            this.menuMobileItems.removeClass('menu-mobile-open');
            this.closeAll();
            $('#masthead .header-menu-mob-button, #header-nav-menu-item-additional-text-close').removeClass('header-menu-mob-button-open')
        },
        getMobileTargetByButtonId: function(id) {
            switch (id) {
                case 'header-lang-menu-button':
                    return this.menuMobileItems.filter('#header-nav-menu-item-wpml');
                    break;
                case 'header-menu-button':
                    return this.menuMobileItems.filter('#header-main-nav');
                    break;
                case 'header-search-menu-button':
                    return this.menuMobileItems.filter('#header-nav-menu-item-search');
                    break;
                case 'header-cur-menu-button':
                    return this.menuMobileItems.filter('#header-nav-menu-item-cur');
                    break;
                case 'header-nav-menu-item-additional-text-close':
                    return this.menuMobileItems.filter('#header-nav-menu-item-wpml');
                    break
            }
        },
        getStatus: function() {
            if (parseInt(this.windowWidth) > 992) {
                return 'desktop'
            } else {
                return 'mobile'
            }
        },
    };
    $(document).ready(function() {
        mainMenuItems.init()
    })
})(jQuery);
(function($) {
    var currency = {
        init: function() {
            var cur = this;
            this.currentCode = this._getCurrentCode();
            this.menuLinks = this._getMenuLinks();
            cur._onChange(this.currentCode, !1);
            this.menuLinks.click(function(e) {
                e.preventDefault();
                cur._onChange($(this).attr('data-cur').trim(), !0)
            })
        },
        _onChange: function(curCode, changeCookie) {
            if (typeof curCode === 'undefined') {
                var curCode = this._getCurrentCode()
            }
            if (typeof curCode === 'undefined' || typeof currencyObject === 'undefined' || typeof currencyObject.values[curCode] === 'undefined' || typeof currencyObject.values[curCode].value === 'undefined' || typeof currencyObject.values[curCode].symbol === 'undefined') {
                return
            }
            this.currentCode = curCode;
            if (changeCookie) {
                this._setCookie();
                console.log(curCode);
                ga('send', 'event', 'Currency', curCode, window.location.pathname)
            }
            $('#header-nav-menu-item-text-cur, #header-cur-menu-button').text(curCode);
            this._changeSymbol(currencyObject.values[curCode].symbol);
            this._changePrice(parseFloat(currencyObject.values[curCode].value));
            this.changeActiveMenuCurrency(this.currentCode)
        },
        _changeSymbol: function(symbol) {
            $('body').find('.currency-symbol').each(function() {
                $(this).html(symbol)
            })
        },
        _changePrice: function(k) {
            $('body').find('.currency-value').each(function() {
                var value = $(this).data('curValue');
                var cur = $(this).data('curCode');
                if (typeof value === 'undefined') {
                    value = parseFloat($(this).attr('data-price'));
                    $(this).data('curValue', value);
                    cur = $(this).attr('data-currency');
                    $(this).data('curCode', cur)
                }
                if (typeof currencyObject.values[cur].value === 'undefined') {
                    return
                }
                var newValue = k * value / currencyObject.values[cur].value;
                if (isNaN(newValue)) {
                    $(this).html('')
                } else {
                    if (newValue > 100) {
                        $(this).html(newValue.toFixed(0))
                    } else if (newValue > 0.000001) {
                        $(this).html(newValue.toFixed(2))
                    } else {
                        $(this).html('0')
                    }
                }
            })
        },
        _getCookie: function() {
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf('currencyCode=') == 0) {
                    return c.substring('currencyCode='.length, c.length)
                }
            }
            return null
        },
        _setCookie: function() {
            var date = new Date();
            date.setTime(date.getTime() + (365 * 24 * 60 * 60 * 1000));
            expires = '; expires=' + date.toUTCString();
            var curCode = (typeof this.currentCode === 'undefined') ? this._getCurrentCode() : this.currentCode;
            document.cookie = 'currencyCode=' + curCode + expires + '; path=/'
        },
        _getCurrentCode: function() {
            if (typeof this.currentCode === 'undefined') {
                var cookieValue = this._getCookie();
                if (cookieValue === null) {
                    return $('#header-nav-menu-item-text-cur').text().trim()
                } else {
                    return cookieValue
                }
            }
            return this.currentCode
        },
        _getMenuLinks: function() {
            return $('#header-additional-nav .menu-currency-item')
        },
        _setCurrentCode: function(currentCode) {
            this.currentCode = currentCode;
            this._setCookie()
        },
        isInt: function(n) {
            return Number(n) === n && n % 1 === 0
        },
        changeActiveMenuCurrency: function(code) {
            this.menuLinks.filter('.active').removeClass('active');
            this.menuLinks.filter('[data-cur="' + code + '"]').first().addClass('active')
        },
    };
    currency.init()
})(jQuery);

function activateStars(container) {
    (function($) {
        container = (typeof container === 'undefined') ? 'body' : container;
        $(container + ' span.stars-rating').each(function() {
            var score = parseFloat($(this).attr('data-raty-score'));
            var stars = '';
            if ($(this).hasClass('stars-rating-int'))
                score = parseInt(score);
            score = isNaN(score) ? 0 : score;
            for (var i = 1; i < 6; i++) {
                if (score < i && (i - score) < 1) {
                    if ((i - score) < 0.41) {
                        stars += '<span class="star full-star"></span>'
                    } else if ((i - score) > 0.79) {
                        stars += '<span class="star empty-star"></span>'
                    } else {
                        stars += '<span class="star half-star"></span>'
                    }
                } else if (score < i) {
                    stars += '<span class="star empty-star"></span>'
                } else {
                    stars += '<span class="star full-star"></span>'
                }
            }
            $(this).html(stars)
        })
    })(jQuery)
}
document.addEventListener("DOMContentLoaded", function(event) {
    activateStars()
});
(function($) {
    var time = 24 * 60 * 60 * 1000;
    $(document).ready(function() {
        var promotionPopup = $('#promotion-popup');
        var promotionPopupB = $('#promotion-popupB');
        var isShowPromotionPopup = getCookie('promotionPopup');
        var isShowPromotionPopupB = getCookie('promotionPopupB');
        if (promotionPopup.length > 0 || promotionPopupB.length > 0) {
            $(document).mouseleave(function() {
                if (!isShowPromotionPopup) {
                    showPromotePopup();
                    setCookie('promotionPopup', 1, time);
                    isShowPromotionPopup = !0
                }
                if (!isShowPromotionPopupB) {
                    showPromotePopupB();
                    setCookie('promotionPopupB', 1, time);
                    isShowPromotionPopupB = !0
                }
            });
            $('.clickoutLink.tailor, .clickoutLink.wix').click(function() {
                hidePromotePopup()
            });
            $('.Multiwix').click(function() {
                hidePromotePopupB()
            });
            promotionPopup.click(function() {
                hidePromotePopup()
            });
            promotionPopupB.click(function() {
                hidePromotePopupB()
            });
            promotionPopup.find('.wix-popup-container').click(function(e) {
                e.preventDefault();
                e.stopPropagation()
            });
            promotionPopupB.find('.wix-popup-container').click(function(e) {
                e.preventDefault();
                e.stopPropagation()
            });
            promotionPopup.find('#promotion-popup-close').add('#promotion-popup-button').click(function() {
                hidePromotePopup()
            });
            promotionPopupB.find('.popup-text-close').click(function() {
                hidePromotePopupB()
            });
            promotionPopupB.find('#promotion-popup-close').click(function() {
                hidePromotePopupB()
            });
            document.addEventListener('keydown', function(e) {
                if ((typeof e.which !== 'undefined' && e.which === 27) || (typeof e.keyCode !== 'undefined' && e.keyCode === 27)) {
                    hidePromotePopup();
                    hidePromotePopupB()
                }
            })
        }

        function showPromotePopup() {
            promotionPopup.css('display', 'block');
            var elem = document.getElementById('promotion-popup');
            if (elem != null) {
                var test = elem.getAttribute('data-name');
                var NameVendor = elem.getAttribute('data-name');
                trackClickout('event', 'Load Exit Intent Popup', 'Load ' + NameVendor + ' popup', test, elem, !1)
            }
        }

        function showPromotePopupB() {
            promotionPopupB.css('display', 'block');
            var elemb = document.getElementById('promotion-popupB');
            if (elemb != null) {
                var testb = elemb.getAttribute('data-name');
                var NameVendor = elemb.getAttribute('data-name');
                trackClickout('event', 'Load Exit Intent Popup', 'Load ' + NameVendor + ' Multichoice popup', testb, elemb, !1)
            }
        }

        function hidePromotePopup() {
            promotionPopup.css('display', 'none')
        }

        function hidePromotePopupB() {
            promotionPopupB.css('display', 'none')
        }

        function setCookie(key, value, time) {
            var expires = new Date();
            expires.setTime(expires.getTime() + time);
            document.cookie = key + '=' + value + ';path=/;expires=' + expires.toUTCString()
        }

        function getCookie(key) {
            var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
            return keyValue ? keyValue[2] : null
        }
    })
})(jQuery);
(function($) {
    var searchAutocomplete = {
        initialize: function() {
            var self = this;
            this._min_size = 2, this.$input = $('#header-search-input');
            this.$container = $('#header-search-autocomplete');
            this.$form = $('#header-nav-menu-item-search');
            this.moreString = this.$container.attr('data-more-string');
            this.postString = this.$container.attr('data-post-string');
            this.vendorString = this.$container.attr('data-vendor-string');
            this.query = '';
            this.prevQuery = '';
            this.data = [];
            this.lang = this.$input.attr('data-lang');
            this.showMore = !1;
            this.windowWidth = window.innerWidth;
            this.$input.on('input', function() {
                self.query = self.$input.val().trim();
                if (self.isValidQuery()) {
                    self.getData()
                }
            });
            $('#header-nav-menu-item-search').click(function(e) {
                e.stopPropagation()
            });
            $(window).resize(function() {
                self.windowWidth = window.innerWidth
            })
        },
        isValidQuery: function() {
            return (this.query.length > this._min_size && this.query !== this.prevQuery)
        },
        getData: function() {
            var that = this;
            $.ajax({
                url: '/search_vendor/search.php',
                type: 'POST',
                dataType: 'json',
                contentType: 'application/x-www-form-urlencoded;charset=utf-8',
                data: {
                    action: 'search_autocomplete_get',
                    lang: that.lang,
                    query: that.query.toLowerCase(),
                },
                success: function(d) {
                    if (typeof d.success !== 'undefined' && d.success === 1 && Array.isArray(d.data.vendor) && d.search_query === that.query.toLowerCase() && Array.isArray(d.data.post) && (d.data.post.length > 0 || d.data.vendor.length > 0)) {
                        that.data = d.data;
                        that.showMore = typeof d.need_more !== 'undefined' && 1 === d.need_more;
                        that.hide();
                        that.show()
                    } else {
                        that.hide()
                    }
                },
                complete: function() {
                    that.prevQuery = that.query
                },
            })
        },
        getStrings: function() {
            var strings = {
                vendor: '',
                post: '',
                more: '',
            };
            var that = this;
            this.data.vendor.forEach(function(d) {
                var link = that._itemVendorLink(d);
                var title = that._titleFilter(d.title);
                var score = Math.round(parseFloat(d.score) * 10) * 1.0 / 10;
                strings.vendor += `<a href="${link}" onclick="trackClickout('event', 'clickin', 'Vendor Page - ${d.title} - Search', '${d.post_slug}', this, false);" class="header-search-autocomplete header-search-autocomplete-vendor">${title}<span class="header-search-autocomplete-score"><span class="stars-rating" data-raty-score="${score}"></span><span class="stars-score">${score}</span></span></a>`
            });
            this.data.post.forEach(function(d) {
                var link = that._itemPostLink(d);
                var title = that._titleFilter(d.title);
                strings.post += `<a href="${link}" class="header-search-autocomplete header-search-autocomplete-post" onclick="trackClickout('event', 'clickin', 'Blog Page - ${d.title}', '${d.post_slug}', this, false);">${title}</a>`
            });
            if (this.showMore) {
                var searchLink = this._searchLink();
                strings.more = `<a href="${searchLink}" class="header-search-autocomplete-search" onclick="trackClickout('event', 'clickin', 'Search - more results', '', this, false);">${this.moreString}</a>`
            }
            return strings
        },
        add: function() {
            var strings = this.getStrings();
            var string = '';
            if (strings.vendor.length > 0) {
                string += `<span class="header-search-autocomplete-header">${this.vendorString}</span><span>${strings.vendor}</span>`
            }
            if (strings.post.length > 0) {
                string += `<span class="header-search-autocomplete-header">${this.postString}</span><span>${strings.post}</span>`
            }
            if (strings.more.length > 0) {
                string += strings.more
            }
            this.$container.html(string);
            this.$container.find('.header-search-autocomplete-score')
        },
        _searchLink: function() {
            return this._domainPrefix() + this._langPrefix() + '?s=' + this.query
        },
        _itemVendorLink: function(item) {
            return this._domainPrefix() + this._langPrefix() + item.cat_slug + '/' + item.post_slug + '/'
        },
        _itemPostLink: function(item) {
            return this._domainPrefix() + this._langPrefix() + 'blog/' + item.post_slug + '/'
        },
        _domainPrefix: function() {
            return window.location.protocol + '//' + window.location.hostname + '/'
        },
        _langPrefix: function() {
            return (this.lang === 'en') ? '' : this.lang + '/'
        },
        show: function() {
            this.add();
            this.$container.addClass('show');
            this.$form.addClass('autocomplete');
            this._activateStars()
        },
        hide: function() {
            this.$form.removeClass('autocomplete');
            this.$container.removeClass('show').html('')
        },
        _titleFilter: function(title) {
            var reg = new RegExp(this.query, 'gi');
            var titlelower = title.toLowerCase();
            var searchlower = this.query.toLowerCase();
            var startfrom = titlelower.indexOf(searchlower);
            var querylenght = searchlower.length;
            return title.substr(0, startfrom) + '<b>' + title.substr(startfrom, querylenght) + '</b>' + title.substr(startfrom + querylenght)
        },
        _activateStars: function() {
            this.$container.find('.stars-rating').each(function() {
                var score = parseFloat($(this).attr('data-raty-score'));
                var stars = '';
                if ($(this).hasClass('stars-rating-int'))
                    score = parseInt(score);
                score = isNaN(score) ? 0 : score;
                for (var i = 1; i < 6; i++) {
                    if (score < i && (i - score) < 1) {
                        if ((i - score) < 0.41) {
                            stars += '<span class="star full-star"></span>'
                        } else if ((i - score) > 0.79) {
                            stars += '<span class="star empty-star"></span>'
                        } else {
                            stars += '<span class="star half-star"></span>'
                        }
                    } else if (score < i) {
                        stars += '<span class="star empty-star"></span>'
                    } else {
                        stars += '<span class="star full-star"></span>'
                    }
                }
                $(this).html(stars)
            })
        }
    };
    searchAutocomplete.initialize()
})(jQuery);
document.addEventListener("DOMContentLoaded", function() {
    var lImgs = [].slice.call(document.querySelectorAll("img.ll-img"));
    var lBgs = [].slice.call(document.querySelectorAll('.ll-bg'));
    var lFrs = [].slice.call(document.querySelectorAll('iframe.ll-fr'));
    if ("IntersectionObserver" in window) {
        var lImgObserver = new IntersectionObserver(function(entries, observer) {
            entries.forEach(function(entry) {
                if (entry.isIntersecting) {
                    var lImg = entry.target;
                    lImg.src = lImg.dataset.src;
                    if (typeof lImg.dataset.srcset === 'string') {
                        lImg.srcset = lImg.dataset.srcset
                    }
                    lImg.style.opacity = '1';
                    lImgObserver.unobserve(lImg)
                }
            })
        });
        var lBgObserver = new IntersectionObserver(function(entries, observer) {
            entries.forEach(function(entry) {
                if (entry.isIntersecting) {
                    var lBg = entry.target;
                    var bg = lBg.dataset.bg;
                    if (typeof bg === 'string') {
                        lBg.style.backgroundImage = 'url("' + bg + '")';
                        lBg.style.opacity = '1'
                    }
                    lBgObserver.unobserve(lBg)
                }
            })
        });
        var lFrObserver = new IntersectionObserver(function(entries, observer) {
            entries.forEach(function(entry) {
                if (entry.isIntersecting) {
                    var lFr = entry.target;
                    var src = lFr.dataset.src;
                    if (typeof src === 'string') {
                        lFr.src = src;
                        lFr.style.opacity = '1'
                    }
                    lFrObserver.unobserve(lFr)
                }
            })
        });
        lImgs.forEach(function(lImg) {
            lImgObserver.observe(lImg)
        });
        lBgs.forEach(function(lBg) {
            lBgObserver.observe(lBg)
        });
        lFrs.forEach(function(lFr) {
            lFrObserver.observe(lFr)
        })
    }
});
(function($) {
    $(document).ready(function() {
        var popup = $('#popup-non-translated');
        if (popup.length < 1) {
            return
        }
        $('#masthead .translate-empty').click(function(e) {
            e.preventDefault();
            e.stopPropagation();
            $('#popup-non-translated-lang').text($(this).text());
            popup.addClass('visible')
        });
        $('#popup-non-translated, #popup-non-translated-close').click(function(e) {
            e.stopPropagation();
            popup.removeClass('visible')
        })
    })
})(jQuery);
'use strict';
var buttonOnPopupLoad;
var loadPopupEvent = new CustomEvent('loadPopupEvent');
window.addEventListener('loadPopupEvent', function(e) {
    var body = document.getElementById('body');
    var el = document.createElement('script');
    el.defer = !0;
    el.src = '/wp-content/themes/websiteplanet/js/vendor-review.js?v=1.1.12';
    body.appendChild(el);
    el = document.createElement('script');
    el.id = 'facebook-jssdk';
    el.defer = !0;
    el.src = "https://connect.facebook.net/en_US/sdk.js";
    body.appendChild(el);
    el = document.createElement('link');
    el.type = 'text/css';
    el.rel = 'stylesheet';
    el.href = '/wp-content/themes/websiteplanet/css/vendor-review.css?v=1.1';
    body.appendChild(el);
    window.fbAsyncInit = function() {
        FB.init({
            appId: '541243596288498',
            cookie: !0,
            xfbml: !0,
            version: 'v3.2'
        });
        FB.AppEvents.logPageView()
    };
    if (typeof popupData === 'object') {
        for (var p in popupData) {
            var n = document.getElementById(p);
            if (n === null) {
                continue
            }
            n.innerHTML = popupData[p]
        }
    }
}, !1);
(function($) {
    var sections = [];
    $('#content p:empty').remove();
    $(window).load(function() {
        createSections();
        recalculateSections();
        var reply = $('#review-translate').text();
        var shortReply = $('#review-translate-small').text();
        var visit = $('#review-visit').text();
        var visitLink = $('#review-visit-link').text();
        var currentLang = $('#current-lang').text();
        var defaultCommentsCount = 6;
        var isSingleLang = ($('#section-reviews-empty').length > 0) ? !0 : !1;
        var isReply = !1;
        var beforeComment = '';
        var navbar = $('#vendor-navbar');
        var navbarWrapper = $('#vendor-navbar-wrapper');
        var navbarMobileItem = $('#navbar-item-current-mobile');
        var nonce = '';
        var isSelectOpen = !1;
        var onClickReviewLink = $('#new-vendor-reviews-items-onclick-event').text();
        var isAdmin = ($('#wpadminbar').length > 0);
        var $scrollTopButton = $('#scrolltop-button');
        var filters = {
            'lang': {
                'parent': $('#comment-filter-langs'),
                'value': '',
                'worked': !1,
            },
            'order': {
                'parent': $('#comment-filter-sortby'),
                'value': '',
                'worked': !1,
            },
        };
        var isShowScrolltopButton = ($('.vendor-section').length > 3);
        var $popupBg = $('#popup-bg');
        var $preloader = $('#preloader-sk');
        recalculateSections();
        var $contents = $('.content a[href^="#"], .header-author-text a[href^="#"]');
        if ($contents.length > 0) {
            $contents.click(function(e) {
                e.preventDefault();
                var $target = $($(this).attr('href'));
                recalculateSections();
                if ($target.length > 0) {
                    $('html, body').animate({
                        scrollTop: ($target.offset().top - 120)
                    }, 400)
                }
            })
        }
        sections.sort(function(a, b) {
            return a.position - b.position
        });
        activateStars();
        $scrollTopButton.click(function() {
            $('html, body').animate({
                scrollTop: (0)
            }, 400)
        });

        function uploadPopupTemplates(e) {
            enablePreloader();
            $.ajax({
                url: '/wp-content/themes/websiteplanet/vendor-hosting-popups.html',
                type: 'GET',
                dataType: 'html',
                data: {},
                success: function(d) {
                    if (typeof d != 'undefined' && d.length > 0) {
                        $popupBg.html(d);
                        window.dispatchEvent(loadPopupEvent);
                        buttonOnPopupLoad = e.target
                    }
                },
                complete: function() {
                    setTimeout(function() {
                        disablePreloader()
                    }, 300)
                },
            })
        }
        $('#header-button-review, #comment-empty-block-button, #comment-filtres-button').click(function(e) {
            e.preventDefault();
            if ($popupBg.html().length < 10) {
                uploadPopupTemplates(e)
            }
        });
        $('#review-items').on('click', '.review-reply', function(e) {
            e.preventDefault();
            if ($popupBg.html().length < 10) {
                uploadPopupTemplates(e)
            }
        });
        $(window).scroll(function() {
            var top = $(document).scrollTop();
            if (isShowScrolltopButton && top > 2 * window.innerHeight) {
                $scrollTopButton.css('display', 'block')
            } else if (isShowScrolltopButton && top < 2 * window.innerHeight) {
                $scrollTopButton.css('display', 'none')
            }
            if (navbar.length > 0) {
                var l = sections.length;
                var navbarPos = (navbarWrapper.offset()).top;
                if (top < navbarPos && navbar.hasClass('vendor-navbar-fixed')) {
                    navbar.removeClass('vendor-navbar-fixed');
                    $('#masthead').css({
                        display: 'block'
                    })
                } else if (top > navbarPos && !navbar.hasClass('vendor-navbar-fixed')) {
                    navbar.addClass('vendor-navbar-fixed');
                    $('#masthead').css({
                        display: 'none'
                    });
                    if (isAdmin) {
                        $('#vendor-navbar').css({
                            top: $('#wpadminbar').height()
                        })
                    }
                }
                for (var i = 0; i < l; i++) {
                    if (sections[i].position < top) {
                        if (typeof sections[i + 1] !== 'undefined') {
                            if (sections[i + 1].position > top) {
                                sections[i].link.addClass('navbar-item-last-visited');
                                navbarMobileItem.attr('data-link', sections[i].link.attr('href')).html(sections[i].link.html())
                            } else {
                                sections[i].link.removeClass('navbar-item-last-visited')
                            }
                        } else if (typeof sections[i + 1] == 'undefined') {
                            var position = sections[i].link.position();
                            sections[i].link.addClass('navbar-item-visited navbar-item-last-visited');
                            navbarMobileItem.attr('data-link', sections[i].link.attr('href')).html(sections[i].link.html())
                        }
                    } else {
                        sections[i].link.removeClass('navbar-item-last-visited')
                    }
                }
            }
        });
        $('#comment-filtres-icon').click(function(e) {
            e.stopPropagation();
            $(this).toggleClass('opened');
            $('#comments-filtres').toggleClass('open')
        });
        $('#comments-filtres .comment-filter-label').click(function(e) {
            e.stopPropagation();
            $(this).parent().toggleClass('open')
        });
        $(' #header-card-based, #header-carousel .header-card-price-item').click(function(e) {
            e.preventDefault();
            var anchor = $(this).attr('href');
            var $node = $(anchor);
            if ($node.length < 1) {
                return
            }
            if ($(e.currentTarget).hasClass('header-card-price-item')) {
                console.log(anchor);
                $(anchor).addClass('open')
            }
            setTimeout(function() {
                $('html, body').animate({
                    scrollTop: $node.offset().top - 170
                }, 400);
                var urlArray = window.location.href.split('#');
                window.location.href = urlArray[0] + anchor
            }, 300)
        });
        $('#vendor-navbar a.navbar-item').click(function(e) {
            e.preventDefault();
            var that = $(this);
            $('html, body').animate({
                scrollTop: ($(that.attr('href')).offset().top - 160)
            }, 400);
            var urlArray = window.location.href.split('#');
            window.location.href = urlArray[0] + that.attr('href')
        });
        checkLang();
        $('#comment-filter-header').click(function() {
            $(this).parents('#comments-filtres').toggleClass('comments-filtres-open')
        });
        $('#comment-filter-select-sortby > li, .comment-filter-langs-item').click(function() {
            var parent = $(this).parents('.comment-filter');
            parent.removeClass('open');
            if (parent.attr('id') === 'comment-filter-sortby') {
                parent.find('li').removeClass('checked');
                $(this).addClass('checked');
                filters.order.value = $(this).attr('data-value');
                $('#comment-filter-label-orderby').attr('data-value', $(this).attr('data-value')).text($(this).text())
            } else if (parent.attr('id') === 'comment-filter-langs') {
                if (!isSingleLang) {
                    var lang = $(this).attr('data-value');
                    if ($(this).hasClass('checked')) {
                        removeLang(lang)
                    } else {
                        addLang(lang)
                    }
                    changeComments()
                }
            }
            changeComments()
        });
        $('#comment-filter-label-langs').on('click', '.comment-filter-selecteditem span', function(e) {
            if (!isSingleLang) {
                e.stopPropagation();
                var lang = $(this).parent().attr('data-value');
                var parent = $(this).parents('.new-vendor-comment-filter-select');
                removeLang(lang);
                changeComments()
            }
        });
        $('#reviews-add').on('click', function(e) {
            e.preventDefault();
            var link = $(this);
            allFilterValuesToArray();
            if (!link.hasClass('disabled')) {
                var lang = $('#comment-filter-label-langs').attr('data-value');
                if (typeof lang === 'undefined' || lang === '') {
                    lang = currentLang
                }
                link.addClass('disabled');
                if (typeof filters.order.value === 'undefined') {
                    filters.order.value = 'newest'
                }
                var sorting = filters.order.value;
                enablePreloader();
                $.ajax({
                    url: '/wp-admin/admin-ajax.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        action: 'get_comments_new_data_fc',
                        post_id: $('#new-vendor-reviews-items-postid').text(),
                        langs: lang,
                        count: defaultCommentsCount,
                        sorting: $('#comment-filter-label-orderby').attr('data-value'),
                        parent_id: 0,
                        offset: $('#review-items > .review').length,
                        service: '',
                    },
                    success: function(d) {
                        if (typeof d.comment_data.comments != 'undefined') {
                            addComments(d);
                            updateStatusAddNewReviews(d.langsavailable, d.filtercount)
                        }
                    },
                    complete: function() {
                        link.removeClass('disabled');
                        disablePreloader();
                        recalculateSections();
                        addAloomaClickoutTrackingCodes();
                        checkCookie()
                    }
                })
            }
        });
        $('#section-bottom-author').click(function() {
            $(this).toggleClass('open')
        });
        $('#navbar-item-current-mobile').click(function(e) {
            e.stopPropagation();
            if ($(this).hasClass('open')) {
                closeMobileMenu()
            } else {
                openMobileMenu()
            }
        });
        $('body').on('click', function(e) {
            closeMobileMenu();
            $('#comment-filter-langs, #comment-filter-sortby').each(function() {
                $(this).removeClass('open')
            })
        });
        if (getQueryVariable("vendorcommentid") || getQueryVariable("vendordirectcomment")) {
            var commentId = getQueryVariable("vendordirectcomment");
            var vendordirectcommentTrue = !0;
            if (commentId === !1 || commentId.length < 1) {
                commentId = getQueryVariable("vendorcommentid");
                vendordirectcommentTrue = !1
            }
            var $target = $('review-items > .review[data-id="' + commentId + '"]');
            if ($target.length > 0) {
                $('#review-items').prepend($target);
                return
            }
            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'get_commentfromid',
                    post_id: $('#new-vendor-reviews-items-postid').text(),
                    comment_id: commentId
                },
                success: function(d) {
                    if (vendordirectcommentTrue) {
                        if ($('#review-shilditems-' + d.comment_ID).length) {
                            $('#review-shilditems-' + d.comment_ID).css("display", "none");
                            $('#review-shilditems-' + d.comment_ID).prev().css("display", "none");
                            var tt = '<div class="review" data-id="' + d.comment_ID + '" data-lang="">' + $('#review-shilditems-' + d.comment_ID).prev().html() + '</div>';
                            $('#review-items').prepend('<div class="review-childitems" id="review-shilditems-' + d.comment_ID + '">' + $('#review-shilditems-' + d.comment_ID).html() + '</div>');
                            $('#review-items').prepend(tt)
                        } else {
                            if ($('.review[data-id="' + d.comment_ID + '"]').length < 1) {
                                $('#review-items').prepend(createComment(d));
                                activateStars()
                            }
                        }
                    } else {
                        $('#review-items').prepend(createComment(d));
                        activateStars()
                    }
                    var that = $(this);
                    recalculateSections();
                    setTimeout(function() {
                        $('html, body').animate({
                            scrollTop: ($("#reviews").offset().top - 160)
                        }, parseInt(that.attr('data-scroll')))
                    }, 200)
                },
                complete: function() {
                    recalculateSections()
                }
            })
        }
        if (typeof document.location.hash === 'string' && document.location.hash.length > 0) {
            setTimeout(function() {
                $('html, body').animate({
                    scrollTop: $(document.location.hash).offset().top - 200
                }, 300)
            }, 300)
        }

        function addComments(response) {
            if (response.comment_data.comments.length > 0) {
                var template = '';
                var length = response.comment_data.comments.length;
                var items = $('#review-items');
                for (var i = 0; i < length; i++) {
                    template += createComment(response.comment_data.comments[i])
                }
                items.append(template);
                items.find('img.ll-img').each(function() {
                    this.style.opacity = '1'
                });
                activateStars('#review-items')
            }
        }

        function changeComments() {
            allFilterValuesToArray();
            enablePreloader();
            var langs = filters.lang.value;
            if (typeof langs === 'undefined' || langs == '') {
                langs = $('#comment-filter-label-langs').attr('data-value')
            }
            $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'get_comments_new_data_fc',
                    post_id: $('#new-vendor-reviews-items-postid').text(),
                    langs: langs,
                    count: defaultCommentsCount,
                    sorting: $('#comment-filter-label-orderby').attr('data-value'),
                    parent_id: 0,
                    offset: 0,
                    service: '',
                },
                success: function(d) {
                    if (typeof d.comment_data.comments != 'undefined') {
                        var template = '';
                        var length = d.comment_data.comments.length;
                        var items = $('#review-items');
                        for (var i = 0; i < length; i++) {
                            template += createComment(d.comment_data.comments[i])
                        }
                        items.html(template);
                        items.find('img.ll-img').each(function() {
                            this.style.opacity = '1'
                        });
                        updateStatusAddNewReviews(d.langsavailable, d.filtercount);
                        activateStars('#review-items');
                        var langs = $('#comment-filter-label-langs .comment-filter-selecteditem');
                        if (langs.length < 1) {
                            checkLang()
                        }
                    }
                },
                complete: function() {
                    disablePreloader();
                    recalculateSections()
                }
            })
        }

        function createComment(comment) {
            var commentTemplate = '<div class="review" data-id="' + comment.comment_ID + '" data-lang="' + comment.language + '"><div class="review-column">';
            if (typeof comment.authorimg !== 'undefined' && comment.authorimg !== null) {
                comment.authorimg = comment.authorimg.replace('data-src', 'src');
                comment.authorimg = comment.authorimg.replace('img-lazy-load', '');
                commentTemplate += '<div class="review-icon">' + comment.authorimg + '</div>'
            } else {
                commentTemplate += '<div class="review-icon"><img src="https://dt2sdf0db8zob.cloudfront.net/wp-content/themes/websiteplanet/img/new-vendor/new-vendor-reviews-strange.png" alt=""></div>'
            }
            if (typeof comment.rating !== 'undefined' && comment.rating !== '0' && comment.rating !== null) {
                var showstars = Math.round(comment.rating) * 20;
                commentTemplate += '<div class="review-rating rating"><div class="wsp-star-block vendor-medium star-rating-' + showstars + ' show-score"></div><span class="stars-score">' + Math.round(comment.rating) + '</span></div>'
            }
            commentTemplate += '<div class="review-name">' + comment.comment_author + '<span class="wsp-flag wsp-flag-' + comment.author_country + '"></span></div>';
            commentTemplate += '<div class="review-country"><span class="wsp-flag wsp-flag-' + comment.author_country + '"></span>' + comment.author_country_name + '</div>';
            if (typeof comment.user_site !== 'undefined') {
                commentTemplate += '<div class="review-item-site">' + comment.user_site + '</div>'
            }
            commentTemplate += '<div class="review-date">' + comment.comment_date + '</div>';
            commentTemplate += '</div><div class="review-column">';
            if (typeof comment.vendor_review_title !== 'undefined' && comment.vendor_review_title !== null) {
                commentTemplate += '<h5 class="review-title">' + comment.vendor_review_title + '</h5>'
            }
            if (typeof comment.comment_content !== 'undefined' && comment.comment_content !== null) {
                commentTemplate += '<p class="review-text">' + comment.comment_content + '</p>'
            }
            commentTemplate += '<div class="review-links"><a href="#" class="review-reply">' + reply + '<span>' + shortReply + '</span></a>';
            commentTemplate += '<a href="' + visitLink + '" ' + onClickReviewLink + ' rel="nofollow" class="clickoutLink review-link">' + visit + '</a>';
            commentTemplate += '</div></div>';
            if (typeof comment.childs !== 'undefined' && typeof comment.childs.filtercount !== 'undefined' && comment.childs.filtercount > 0) {
                commentTemplate += '<div class="review-childitems" id="review-shilditems-' + comment.comment_ID + '">';
                comment.childs.comment_data.comments.forEach(function(child) {
                    commentTemplate += '<div class="review" data-id="' + child.comment_ID + '" data-lang="' + child.language + '"><div class="review-column">';
                    if (typeof child.authorimg !== 'undefined' && child.authorimg !== null) {
                        child.authorimg = child.authorimg.replace('data-src', 'src');
                        child.authorimg = child.authorimg.replace('img-lazy-load', '');
                        commentTemplate += '<div class="review-icon">' + child.authorimg + '</div>'
                    } else {
                        commentTemplate += '<div class="review-icon"><img src="https://dt2sdf0db8zob.cloudfront.net/wp-content/themes/websiteplanet/img/new-vendor/new-vendor-reviews-strange.png" alt=""></div>'
                    }
                    commentTemplate += '<div class="review-name">' + child.comment_author + '</div>';
                    commentTemplate += '<div class="review-date">' + child.comment_date + '</div>';
                    commentTemplate += '</div><div class="review-column">';
                    if (typeof child.comment_content !== 'undefined' && child.comment_content !== null) {
                        commentTemplate += '<p class="review-text">' + child.comment_content + '</p>'
                    }
                    commentTemplate += '<div class="review-links"><a href="#" class="review-reply">' + reply + '<span>' + shortReply + '</span></a>';
                    commentTemplate += '<a href="' + visitLink + '" ' + onClickReviewLink + ' rel="nofollow" class="clickoutLink review-link">' + visit + '</a>';
                    commentTemplate += '</div></div></div>'
                })
            }
            commentTemplate += '</div></div>';
            return commentTemplate
        }

        function getChildCommentString(count) {
            if (typeof count !== 'number' || count < 1) {
                return ''
            }
            if (count === 1) {
                return $('#child-comment-single').text()
            } else {
                return $('#child-comment-single').text().replace('%d', count)
            }
        }

        function isCommentsFiltersSelected() {
            for (var key in filters) {
                if (typeof filters[key].value !== 'undefined' && filters[key].value.length !== '') {
                    return !0
                }
            }
            return !1
        }

        function isCommentFiltersWorked() {
            for (var key in filters) {
                if (filters[key].value.worked) {
                    return !0
                }
            }
            return !1
        }

        function resetAllCommentFiltres() {
            for (var key in filters) {
                filters[key].worked = !1;
                if (!(key === 'lang' && isSingleLang)) {
                    filters[key].value = ''
                }
                if (key == 'lang' && !isSingleLang) {
                    filters[key].parent.removeClass('selected');
                    filters[key].parent.find('.new-vendor-comment-filter-option-lang').removeClass('checked')
                } else if (key !== 'lang') {
                    filters[key].parent.parent().removeClass('new-vendor-comment-filter-select-selected');
                    filters[key].parent.find('.new-vendor-comment-filter-option-default').attr('data-value', '').html(filters[key].parent.attr('data-default'))
                }
            }
        }

        function copyToClipboard(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).text()).select();
            document.execCommand("copy");
            $temp.remove()
        }

        function addLangSelectedItem(parent, lang, text) {
            $('#comment-filter-label-langs').append('<span data-value="' + lang + '" class="comment-filter-selecteditem">' + text + '<span>x</span></span>')
        }

        function insertLangSelectedItem(parent, lang, text) {
            $('#comment-filter-label-langs').html('<span data-value="' + lang + '" class="comment-filter-selecteditem">' + text + '<span>x</span></span>')
        }

        function removeLangSelectedItem(parent, lang) {
            parent.find('#comment-filter-label-langs > .comment-filter-selecteditem[data-value="' + lang + '"]').remove()
        }

        function addLang(lang) {
            var parent = $('#comment-filter-langs');
            var string = parent.find('#comment-filter-label-langs').attr('data-value');
            var item = parent.find('.comment-filter-langs-item[data-value="' + lang + '"]');
            var arrayLang = string.split(',');
            arrayLang.push(lang);
            arrayLang = arrayLang.filter((a, b) => arrayLang.indexOf(a) === b)
            string = arrayLang.join(',');
            addLangSelectedItem(parent, lang, item.find('.comment-filter-option-lang-name').text())
            parent.find('#comment-filter-label-langs').attr('data-value', string);
            item.addClass('checked');
            filters.lang.value = string
        }

        function removeLang(lang) {
            var parent = $('#comment-filter-langs');
            $('#comment-filter-label-langs').find('span[data-value="' + lang + '"]').remove();
            $('#comment-filter-select-langs').find('li[data-value="' + lang + '"]').addClass('checked');
            var string = parent.find('#comment-filter-label-langs').attr('data-value');
            var item = parent.find('.comment-filter-langs-item[data-value="' + lang + '"]');
            string = string.replace(lang + ',', '');
            removeLangSelectedItem(parent, lang);
            parent.find('#comment-filter-label-langs').attr('data-value', string);
            if (string.length < 1) {
                parent.removeClass('selected');
                parent.find('#comment-filter-label-langs').text(parent.attr('data-default'))
            }
            item.removeClass('checked');
            filters.lang.value = string
        }

        function checkLang() {
            var $langFilter = $('#comment-filter-label-langs');
            if ($langFilter.length < 1) {
                return
            }
            addLang($('#comment-filter-label-langs').attr('data-value'))
        }

        function allFilterValuesToArray() {
            for (var key in filters) {
                if (key === 'service') {
                    filters[key].value = {
                        'parent_service': filters[key].parent.find('.new-vendor-comment-filter-option-default').attr('data-parent'),
                        'child_service': filters[key].parent.find('.new-vendor-comment-filter-option-default').attr('data-value'),
                    }
                } else {
                    filters[key].value = filters[key].parent.find('.new-vendor-comment-filter-option-default').attr('data-value')
                }
            }
        }

        function enablePreloader() {
            $preloader.append('<div class="preloader-sk-circle" id="preloader-sk-circle"></div>');
            var inner = $preloader.find('#preloader-sk-circle');
            for (var i = 1; i < 13; i++) {
                inner.append('<div class="preloader-sk-circle' + i + '"></div>')
            }
            $('#preloader-sk').css({
                opacity: '1',
                display: 'block'
            })
        }

        function disablePreloader() {
            $preloader.css({
                display: 'none',
                opacity: '0'
            });
            $preloader.html('')
        }

        function updateStatusAddNewReviews(langArray, totalCount) {
            var totalCountsArray = [],
                count = $('#review-items .review').length;
            if (typeof filters.lang.value === 'undefined' || filters.lang.value == '') {
                totalCountsArray.push($('#comment-filter-label-langs').attr('data-default-value'))
            } else {
                totalCountsArray = filters.lang.value.split(',')
            }
            if (totalCount > count) {
                $('#reviews-add').css('display', 'block')
            } else {
                $('#reviews-add').css('display', 'none')
            }
        }
    });
    $(window).resize(function() {
        recalculateSections();
        closeMobileMenu()
    });

    function carouselInit() {}

    function recalculateSections() {
        var length = sections.length;
        for (var i = 0; i < length; i++) {
            var elem = $(sections[i].slug);
            if (elem.length > 0) {
                sections[i].position = ((elem.offset()).top - 250)
            }
        }
    }

    function createSections() {
        $('#navbar-items a').each(function() {
            sections.push({
                'link': $(this),
                'slug': $(this).attr('href'),
                'position': 0,
                'scrolled': 0,
            })
        })
    }

    function openMobileMenu() {
        $('#navbar-item-current-mobile').addClass('open').siblings('#navbar-items').addClass('open')
    }

    function closeMobileMenu() {
        $('#navbar-item-current-mobile').removeClass('open').siblings('#navbar-items').removeClass('open')
    }

    
    function activateStars(container) {
        container = (typeof container === 'undefined') ? 'body' : container;
        $(container + ' span.stars-rating').each(function() {
            var score = parseFloat($(this).attr('data-raty-score'));
            var stars = '';
            if ($(this).hasClass('stars-rating-int'))
                score = parseInt(score);
            score = isNaN(score) ? 0 : score;
            for (var i = 1; i < 6; i++) {
                if (score < i && (i - score) < 1) {
                    if ((i - score) < 0.41) {
                        stars += '<span class="star full-star"></span>'
                    } else if ((i - score) > 0.79) {
                        stars += '<span class="star empty-star"></span>'
                    } else {
                        stars += '<span class="star half-star"></span>'
                    }
                } else if (score < i) {
                    stars += '<span class="star empty-star"></span>'
                } else {
                    stars += '<span class="star full-star"></span>'
                }
            }
            $(this).html(stars)
        })
    }

    function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return pair[1]
            }
        }
        return (!1)
    }
})(jQuery);
(function($) {
    $('#header-carousel .header-card-show').click(function() {
        $(this).parent().addClass('show')
    });
    $('#header-carousel .header-card-hide').click(function() {
        $(this).parent().removeClass('show')
    });
    if ($('.new-vendor-popup-bg-inner').hasClass('form-page-for-review')) {
        window.dispatchEvent(loadFbJsEvent);
        $('#new-vendor-popup-bg').css('display', 'block').find('.new-comment-window').css('display', 'none');
        $('#new-comment-first').css('display', 'block')
    }
})(jQuery);
! function(d, l) {
    "use strict";
    var e = !1,
        o = !1;
    if (l.querySelector)
        if (d.addEventListener) e = !0;
    if (d.wp = d.wp || {}, !d.wp.receiveEmbedMessage)
        if (d.wp.receiveEmbedMessage = function(e) {
                var t = e.data;
                if (t)
                    if (t.secret || t.message || t.value)
                        if (!/[^a-zA-Z0-9]/.test(t.secret)) {
                            var r, a, i, s, n, o = l.querySelectorAll('iframe[data-secret="' + t.secret + '"]'),
                                c = l.querySelectorAll('blockquote[data-secret="' + t.secret + '"]');
                            for (r = 0; r < c.length; r++) c[r].style.display = "none";
                            for (r = 0; r < o.length; r++)
                                if (a = o[r], e.source === a.contentWindow) {
                                    if (a.removeAttribute("style"), "height" === t.message) {
                                        if (1e3 < (i = parseInt(t.value, 10))) i = 1e3;
                                        else if (~~i < 200) i = 200;
                                        a.height = i
                                    }
                                    if ("link" === t.message)
                                        if (s = l.createElement("a"), n = l.createElement("a"), s.href = a.getAttribute("src"), n.href = t.value, n.host === s.host)
                                            if (l.activeElement === a) d.top.location.href = t.value
                                }
                        }
            }, e) d.addEventListener("message", d.wp.receiveEmbedMessage, !1), l.addEventListener("DOMContentLoaded", t, !1), d.addEventListener("load", t, !1);

    function t() {
        if (!o) {
            o = !0;
            var e, t, r, a, i = -1 !== navigator.appVersion.indexOf("MSIE 10"),
                s = !!navigator.userAgent.match(/Trident.*rv:11\./),
                n = l.querySelectorAll("iframe.wp-embedded-content");
            for (t = 0; t < n.length; t++) {
                if (!(r = n[t]).getAttribute("data-secret")) a = Math.random().toString(36).substr(2, 10), r.src += "#?secret=" + a, r.setAttribute("data-secret", a);
                if (i || s)(e = r.cloneNode(!0)).removeAttribute("security"), r.parentNode.replaceChild(e, r)
            }
        }
    }
}(window, document);