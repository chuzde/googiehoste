<!doctype html>
<html class="no-js" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<meta http-equiv="Content-Language" content="es" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <title>Genere un aviso de eliminación de DMCA gratuito y envíelo a la empresa de alojamiento</title>
<meta name="description" content="Si alguien copió su artículo o cualquier propiedad digital, esta herramienta lo ayudará a generar un Aviso DMCA y a retirar su propiedad de la web.">
<meta name="Keywords" CONTENT="DMCA, libre de DMCA, material con derechos de autor de eliminación, generador de avisos de DMCA">
<script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"Organization","name":"GoogieHost","legalName":"GoogieHost","foundingDate":"2012","logo":"https:\/\/googiehost.com\/images\/logo.png","image":"https:\/\/googiehost.com\/images\/logo.png","url":"https:\/\/googiehost.com","address":{"@type":"PostalAddress","streetAddress":"4/453, Vibhav Khand, Gomti Nagar, Lucknow ","addressLocality":"Uttar Pradesh","postalCode":"226010","addressCountry":"India"},"contactPoint":{"@type":"ContactPoint","telephone":"+91-9616782253","areaServed":"IN","availableLanguage":"English","email":"mailto:support@googiehost.com","contactType":"customer service"},"sameAs":["https:\/\/www.facebook.com\/GoogieHost","https:\/\/plus.google.com\/+GoogiehostFreeHosting","https:\/\/twitter.com\/GoogieHost","https:\/\/www.youtube.com\/user\/GoogieHostFree"]}</script>
<script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"WebSite","name":"GoogieHost","alternateName":"YouStable Technologies Pvt Ltd","url":"https:\/\/googiehost.com","image":{"@type":"ImageObject","url":"https:\/\/googiehost.com\/images\/logo.png"},"sameAs":["https:\/\/www.facebook.com\/GoogieHost","https:\/\/plus.google.com\/+GoogiehostFreeHosting","https:\/\/twitter.com\/GoogieHost","https:\/\/www.youtube.com\/user\/GoogieHostFree"]}</script>
<script type="application/ld+json">
        {"@context":"http:\/\/schema.org\/","@type":"Product","name":"Generate Free DMCA Takedown Notice","description":"Si alguien copió su artículo o cualquier propiedad digital, esta herramienta lo ayudará a generar un Aviso DMCA y a retirar su propiedad de la web.","brand":{"@type":"Brand","name":"GoogieHost"},"aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "4.7",
    "reviewCount": "832"
    },"offers": {
        "@type": "AggregateOffer",
        "priceCurrency": "USD",
        "lowprice": "0",                   
        "seller": {
      "@type": "Organization",
      "name": "GoogieHost"
    }
      } }
    </script>

<link rel="image_src" href="http://www.googiehost.com/thumb.jpg" />
    <link rel="shortcut icon" href="images/icons/favicon.png" />
      <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title" content="CHANGE-ME">
  <link rel="apple-touch-icon" href="images/icons/touch-icon-iphone.png">
  <link rel="apple-touch-icon" sizes="76x76" href="images/icons/touch-icon-ipad.png">
  <link rel="apple-touch-icon" sizes="120x120" href="images/icons/touch-icon-iphone-retina.png">
  <link rel="apple-touch-icon" sizes="152x152" href="images/icons/touch-icon-ipad-retina.png">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/animate.min.css" />
    <link rel="stylesheet" href="css/morphext.css" />
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <link rel="stylesheet" href="css/slicknav.css">
    <link rel="stylesheet" href="style.css" />
    <link rel=stylesheet href=premium.css />
    <script src="js/vendor/modernizr.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-54079021-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-54079021-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->

<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"nQZkh1aMQV00G7", domain:"googiehost.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="images/atrk.gif" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->
  </head>
    <body>
<style>
.info {margin-left:18px;font-size:16px;}
.pure-form-stacked input:not([type]), .pure-form-stacked input[type=color], .pure-form-stacked input[type=date], .pure-form-stacked input[type=datetime-local], .pure-form-stacked input[type=datetime], .pure-form-stacked input[type=email], .pure-form-stacked input[type=file], .pure-form-stacked input[type=month], .pure-form-stacked input[type=number], .pure-form-stacked input[type=password], .pure-form-stacked input[type=search], .pure-form-stacked input[type=tel], .pure-form-stacked input[type=text], .pure-form-stacked input[type=time], .pure-form-stacked input[type=url], .pure-form-stacked input[type=week], .pure-form-stacked label, .pure-form-stacked select, .pure-form-stacked textarea {
    display: block;
    margin: .25em 0;
}
.pure-form input[type=color], .pure-form input[type=date], .pure-form input[type=datetime-local], .pure-form input[type=datetime], .pure-form input[type=email], .pure-form input[type=month], .pure-form input[type=number], .pure-form input[type=password], .pure-form input[type=search], .pure-form input[type=tel], .pure-form input[type=text], .pure-form input[type=time], .pure-form input[type=url], .pure-form input[type=week], .pure-form select, .pure-form textarea {
    height: 40px;
    padding: .5em .6em;
    display: inline-block;
    border: 1px solid #ccc;
    box-shadow: inset 0 1px 3px #ddd;
    border-radius: 4px;
    vertical-align: middle;
    box-sizing: border-box;font-size:16px;
}
.pure-form .pure-input-1-2 {
    width: 50%;
}
.pure-form .pure-input-1 {
    width: 50%;
}
.pure-form label {
    margin: .5em 0 .2em;font-size: 16px; 
}

.pure-form fieldset {
    margin: 0; 
    padding: .35em 0 .75em; 
    border: 0; 
}
.pure-form legend {
    display: block;
    width: 100%;
    padding: .3em 0;
    margin-bottom: .3em;
    color: #333;
    border-bottom: 1px solid #e5e5e5;
       font-weight: 400; 
}
    .pure-form   .pure-button{color:white}
h3 {
    color: #14475e;
    margin: 50px 0 20px;
    padding: 0;font-weight: 600;
}
@media (min-width: 1281px) {
 .tablink {
    background-color: white;
    color: black;
    float: center;
    border:1px solid black;
    outline: none;
    cursor: pointer;
    padding: 10px 10px;
    font-size: 17px;
    width:10%;
}
.tablink:hover {
   color:white;
}
.tablink:hover .tablink{
    color:black;
}
.tabcontent {
    color: black;
    display: none;
    padding: 10px 20px;
    height: 100%;border:1px solid black;}
}
@media (min-width: 1025px) and (max-width: 1280px) {
.tablink {
    background-color: white;
    color: black;
    float: center;
    border:1px solid black;
    outline: none;
    cursor: pointer;
    padding: 10px 10px;
    font-size: 17px;
    width:10%;
}
.tablink:hover {
    color:white;
}
.tablink:hover .tablink{
    color:black;
}
.tabcontent {
    color: black;
    display: none;
    padding: 10px 20px;
    height: 100%;border:1px solid black;}
}
@media (min-width: 768px) and (max-width: 1024px) {
.tablink {
    background-color: white;
    color: black;
    float: center;
    border:1px solid black;
    outline: none;
    cursor: pointer;
    padding: 10px 10px;
    font-size: 17px;
    width:15%;
}
.tablink:hover {
   color:white;
}
.tablink:hover .tablink{
   color:black;
}
.tabcontent {
    color: black;
    display: none;
    padding: 10px 20px;
    height: 100%;border:1px solid black;}
}
@media (min-width: 481px) and (max-width: 767px) {

.tablink {
    background-color: white;
    color: black;
    float: center;
    border:1px solid black;
    outline: none;
    cursor: pointer;
    padding: 10px 10px;
    font-size: 17px;
}
.tablink:hover {
    color:white;
}
.tablink:hover .tablink{
color:black;
}
.tabcontent {
    color: black;
    display: none;
    padding: 10px 20px;
    height: 100%;border:1px solid black;}
}

@media (min-width: 320px) and (max-width: 480px) {
.tablink {
    background-color: white;
    color: black;
    float: center;
    border:1px solid black;
    outline: none;
    cursor: pointer;
    padding: 10px 10px;
    font-size: 17px;
}

.tablink:hover {
   
    color:white;
}
.tablink:hover .tablink{
   
    color:black;
}

/* Style the tab content (and add height:100% for full page content) */
.tabcontent {
    color: black;
    display: none;
    padding: 10px 20px;
    height: 100%;border:1px solid black;}
}
  

/* Style tab links */
    #text{font-size:16px}
#text li{margin-bottom:10px;}
    #text li dl dt{font-size:16px;font-weight:600}
     #text li dl dd{font-size:16px;}
#styled_text {background-color: white;}
#html {background-color: white;}
.features3 .dmca .content{margin:0 5px 10px;} .features3 .dmca h4{font-size:18px;font-weight:600;text-align:center;}.features3 .medium-block-grid-3 .img-hover-zoom img{display:block;-moz-transition:all .3s;-webkit-transition:all .3s;transition:all .3s}.features3 .dmca .img-hover-zoom a{text-decoration:none;}.features3 .dmca .img-hover-zoom img{display:block;-moz-transition:all .3s;-webkit-transition:all .3s;transition:all .3s}.features3 .medium-block-grid-3 .img-hover-zoom:hover img{-moz-transform:scale(1.1);-webkit-transform:scale(1.1);transform:scale(1.1)}.features3 .dmca .img-hover-zoom:hover img{-moz-transform:scale(1.1);-webkit-transform:scale(1.1);transform:scale(1.1)}
</style>
<!--  HEADER -->
<header class="alt-2 header5">
<div class="top">
  <div class="row">
  <div class="small-12 large-3 medium-3 columns">
   <div class="logo">
   <a href="index.html" title=""><img src="images/logo.png" alt="logo" title="Free Domain name registration"/></a>
   </div>
</div>
<div class="small-12 large-9 medium-9 columns">
        <nav class="desktop-menu">
    <ul class=sf-menu>
<li><a href=freehosting.html>Alojamiento Gratuito</a></li>
<li>
<span class="nav-top-txt nav-top-red"><span>99% de descuento</span></span>
<a href="https://youstable.com/manage/cart.php?a=add&pid=21&promocode=POWERUP">Alojamiento Pagado</a></li>
<li class="dropdown">
<span class="nav-top-txt nav-top-red earn"><span>Ahorre $ 100</span></span><a href="#">Descuento <i class="fa fa-chevron-down"></i></a>
<div class="dropdown-content">
      <a href="https://googiehost.com/web-hosting-sale-coupons.html">Hospedaje Web</a>
      <a href="https://googiehost.com/cheap-dedicated-server.html">Servidor dedicado</a>
      <a href="https://googiehost.com/cheap-vps-hosting.html">Alojamiento VPS</a>
    </div>
    </li>
<li><a href="https://client.googiehost.com/clientarea.php?language=spanish">Inicia Sesión</a></li>
<li><a href=signup.html>Registrate</a></li>
<li><a href=support.html>Soporte</a></li>
</ul>
  </nav>
<!--  END OF NAVIGATION MENU AREA -->
<!--  MOBILE MENU AREA -->
  <nav class="mobile-menu">
   <ul class="mobile-menu1">
<li><a href=freehosting.html>Alojamiento Gratuito</a></li>
<li><a href="https://youstable.com/manage/cart.php?a=add&pid=21&promocode=POWERUP">Alojamiento Pagado<span class="paid">99% de descuento</span></a> </li>
 <li class="dropdown menu-item1"><a class="dropbtn">Descuento <i class="fa fa-chevron-down"> </i><span>Ahorre $100</span></a>
      <div class="dropdown-content">
      <a href="https://googiehost.com/web-hosting-sale-coupons.html">Hospedaje Web</a>
      <a href="https://googiehost.com/cheap-dedicated-server.html">Servidor dedicado</a>
      <a href="https://googiehost.com/cheap-vps-hosting.html">Alojamiento VPS</a>
    </div></li>
<li><a href="https://client.googiehost.com/clientarea.php?language=spanish">Inicia Sesión</a></li>
<li><a href=signup.html>Registrate</a></li>
<li><a href=support.html>Soporte</a></li>
</ul>
  </nav>
  <!--  END OF MOBILE MENU AREA -->
  </div>
  </div>
  </div>
    <div class="content-block1 quote-box text-center message" style="padding:0">
<div class="container box">
<div class="row"><!--<div class="columns large-1 hide-for-small-only hide-for-medium-only"><span style="opacity:0">testing</span></div> -->
<div class="columns large-8 medium-9 med small-12"> <h1 class="text-xs-center text-md-left"> Web Hosting </h1><p class="hide1">en el que puedes confiar. Ofrecemos cPanel Web Hosting Powered by</p><br>
<div class="row">
<div class="columns small-12 large-6">
  <ul  style="list-style-type:none;"><li><i class="fa fa-check" aria-hidden="true"></i> Unidades SSD y LiteSpeed</li><li><i class="fa fa-check" aria-hidden="true"></i>Certificado SSL gratuito</li><li><i class="fa fa-check" aria-hidden="true"></i>Gratis Cloudflare CDN</li></ul>
</div>
<div class="columns small-12 large-6">
  <ul  style="list-style-type:none;"><li><i class="fa fa-check" aria-hidden="true"></i> 99.95% Servidor Uptime</li><li><i class="fa fa-check" aria-hidden="true"></i> Backbone de hardware de gran alcance</li><li><i class="fa fa-check" aria-hidden="true"></i> Chat 24x7 y soporte de entradas</li></ul>
</div>
  </div>
 <br/> 
</div>
<div class="columns large-4 medium-3  med1 small-12">
  <div class="offer">
  <center> <div ><span class="savings">POWERUP</span></div> 
    <div class="arrow"></div>
    <div class="price">
<p><span class="price4">
  <span class="sign">$</span><span class="int" style="font-size:120px;vertical-align:top;">0</span><span class="sub"><span class="float">.18</span><span class="terms">/month</span></span>
</span></p>
<center><a class="custom button button--moema button--inverted button--text-thick button--size-s"  href="https://youstable.com/manage/cart.php?a=add&pid=21&promocode=POWERUP" target="_blank">Ordenar Ahora </a></center>
  </div>
  </center>
  </div>
</div>
  </div>
  </div>
</div>
</header>
<!--  END OF HEADER -->
<section>
<div class="content" style="padding: 30px 0 10px 0;position: relative;">
  <script src="js/ads.js" type="text/javascript"></script>
<div class="row">
  <div class="columns large-1 medium-1 hide-for-small-only"><span style="opacity:0">test</span></div>
<div class="columns large-10 medium-10 small-12"><div id="banner-12345">
   <a href="disable-adblock.html" style="color:#333;text-decoration:none"><b>Disable Ad Block!!</b></a> We understand that you hate Ads but this the only option to recover our server cost, We request you to Disable Ads Blocking to keep this service up and running.
</div></div>
<div class="columns large-1 medium-1 hide-for-small-only"></div></div>
<script type="text/javascript">
if ( !document.getElementById('test-block') ) {
document.getElementById('banner-12345').style.display='block';
document.getElementById('banner-12346').style.display='block'; 
}
</script>
<div class="row">
<div class="small-12 columns">
<h2 style="font-size:24px;margin-top:40px;margin-bottom:30px;font-weight:800;color:black">¿Qué es el aviso de DMCA? ¿Y puede ayudarme?</h2>
<p>Desde que comenzó Internet, las personas han subido más de mil millones de gigabytes de contenido digital.</p>
<p>Eso incluye música, películas, juegos y mucho más. ¿Qué protege a los creadores de contenido cuando alguien viola las leyes de derechos de autor y publica su trabajo en línea sin su consentimiento? En los Estados Unidos, es la DMCA.</p>
<p>Las disposiciones de la DMCA protegen a los creadores y propietarios de contenido de los infractores de derechos de autor al proporcionar un proceso que cualquiera puede usar para que el trabajo protegido legalmente se elimine de un sitio web.</p>
<h2 style="font-size:24px;margin-top:40px;margin-bottom:30px;font-weight:800;color:black">Generador de notificaciones DMCA</h2>
<p>Con nuestro generador de avisos de eliminación de DMCA, crear un aviso es fácil. Todo lo que tiene que hacer si ingresa algunos campos y el aviso se creará para usted. Consulte a continuación para obtener <a href="#what-is-dmca">más información</a> sobre la DMCA en general y sobre cómo usar nuestro generador en particular.</p>
<script type="text/javascript">
 function showHide() {
   var div = document.getElementById(notice);
   if (div.style.display == 'none') {
     div.style.display = '';
   }
   else {
     div.style.display = 'none';
   }
 }
</script>

<form class="pure-form pure-form-stacked" method="post" action="notice.php">
    <fieldset>
        <legend>Sus Datos</legend>
        <label for="dmca_full_name">Nombre Completo</label>
        <input class="pure-input-1-2" type="text" id="dmca_full_name" name="dmca_full_name" value="" required="">

        <label for="dmca_address">Dirección</label>
        <input class="pure-input-1" type="text" id="dmca_address" name="dmca_address" value="" required="">

        <label for="dmca_tel">Número de Teléfono</label>
        <input class="pure-input-1-2" type="tel" name="dmca_tel" id="dmca_tel" value="">

        <label for="dmca_email">Correo Electrónico</label>
        <input class="pure-input-1-2" type="email" id="dmca_email" name="dmca_email" value="" required="">
    </fieldset>
    <fieldset>
        <legend>Detalles de infracción</legend>

        <label for="dmca_infringement_name">Nombre del trabajo infractor</label>
        <input class="pure-input-1-2" type="text" id="dmca_infringement_name" name="dmca_infringement_name" value="" required="">
        <label for="dmca_original_urls">URL originales del contenido Infringir</label>
        <textarea class="pure-input-1" name="dmca_original_urls" id="dmca_original_urls" rows="6" required=""></textarea>

        <label for="dmca_infringing_urls">URLs de contenido</label>
        <textarea class="pure-input-1" name="dmca_infringing_urls" id="dmca_infringing_urls" rows="6" required=""></textarea>
    </fieldset> 
    <button class="pure-button pure-button-primary" type="submit" name="SubmitButton" >Generar aviso DMCA</button>
</form>
<div class="row">
<div class="small-12 medium-12 large-12 columns">
<div style="text-align:center"><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- pages -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3431396059777650"
     data-ad-slot="9163787323"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div><br/>
</div>
</div>
<div class="row">
  <div class="columns large-1 medium-1 hide-for-small-only"><span style="opacity:0">test</span></div>
<div class="columns large-10 medium-10 small-12"><div id="banner-12345">
   <a href="disable-adblock.html" style="color:#333;text-decoration:none"><strong>Disable Ad Block!!</strong></a>&nbsp;&nbsp;हम समझते हैं कि आप विज्ञापनों से घृणा करते हैं लेकिन हमारी सर्वर लागत को पुनर्प्राप्त करने के लिए यह एकमात्र विकल्प है, हम आपसे अनुरोध करते हैं कि आप इस सेवा को चालू रखने के लिए विज्ञापन block करना अक्षम करें।</div></div>
<div class="columns large-1 medium-1 hide-for-small-only"></div></div>
<script src="js/ads.js" type="text/javascript"></script>
<script type="text/javascript">
    if ( !document.getElementById('test-block') ) {
        document.getElementById('banner-12345').style.display='block';
    }
</script>
<div>
  <div class="row features3">
  <div class="large-4 medium-4 columns hide-for-small-only"><span style="opacity:0">test</span></div>
<div class="large-4 medium-4 small-12 columns">
 <div class="dmca" style="border: 1px solid #edf4fc;">
<div class="img-hover-zoom">
  <a target="_blank" href="https://googiehost.com/blog/someone-copied-blog-post/"><p>
  <img src="../images/Someone-Copied-Blog-Post.jpg">
 </p>
 <div class="content">
 <h4>Someone Copied Blog Post What To Do? – DMCA Letter</h4>

</div>
</a></div>
</div>
</div>  <div class="large-4 medium-4 columns hide-for-small-only"><span style="opacity:0">test</span></div></div>
<h2 style="font-size:24px;margin-top:40px;margin-bottom:30px;font-weight:800;color:black" >¿Qué es la DMCA?</h2>
<p id="what-is-dmca">Desde mediados hasta finales de la década de 1990, el intercambio de archivos de igual a igual y otras nuevas tecnologías digitales habían facilitado el acceso ilegal generalizado a material protegido por derechos de autor. </p>
<p>En respuesta, organizaciones de la industria como la Asociación de la Industria de la Grabación de América (RIAA, por sus siglas en inglés) presionaron para la creación de un proceso formal mediante el cual los titulares de derechos de autor pudieran hacer valer sus derechos sobre los medios publicados en sitios web de terceros y eliminar el material protegido por derechos de autor.</p>
<p>La DMCA es el resultado: una colaboración entre legisladores, compañías de medios y defensores de los consumidores. DMCA es la abreviatura de la Ley de Derechos de Autor del Milenio Digital. Es una ley de los Estados Unidos que fue promulgada por el Congreso de los Estados Unidos en 1998 y firmada por el presidente Bill Clinton el 28 de octubre de ese año.</p>
<h3 >¿Cómo se pueden presentar las notificaciones?</h3>
<p>La herramienta principal de la DMCA es el aviso de eliminación de la DMCA. Cuando un titular de derechos de autor se entera de una infracción, se emite un aviso de eliminación de DMCA al servicio que aloja el sitio web ofensivo o al proveedor de servicios de Internet (ISP) del infractor.</p>
<p>El material infractor también puede eliminarse de los resultados de búsqueda mediante la publicación de un aviso a un motor de búsqueda.</p>
<p>Se debe enviar un aviso por escrito al agente de la DMCA de la organización por escrito, identificando el trabajo con copyright original y el material que infringe los derechos de autor. Debe estar firmado por el titular de los derechos de autor o su agente.</p>
<p>No hay un formulario oficial de aviso de eliminación de DMCA que los titulares de derechos de autor deban usar.</p>
<p>Sin embargo, cada queja debe cumplir con ciertas especificaciones para ser válida. Además de proporcionar información de contacto e identificar la presunta infracción de derechos de autor, el autor de la notificación debe indicar:</p>
<ul  class="info" style="list-style:disc;font-size:18px">
<li>Que el aviso se presente de buena fe.</li>
<li>Que toda la información en el aviso sea precisa.</li>
<li>Que bajo pena de perjurio, el originador tiene derecho a actuar en nombre de alguien que posee un derecho exclusivo, es decir, un derecho de autor, que actualmente se está violando.</li>
</ul><br/>
</div>
</div></div>
<div class="row">
  <div class="columns large-12 medium-12 small-12">
<div style="text-align:center"><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- pages -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3431396059777650"
     data-ad-slot="9163787323"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div></div></div> </div>
</section>
<!--  FOOTER  -->

<footer>
<div class=row>
<div class="small-12 columns">
<div class=footerlinks>
<div class="small-12 large-3 medium-3 columns border-right">
<h2>Acerca de GoogieHost</h2>
<ul>
<li><a href=about.html>Acerca de nosotros</a></li>
<li><a href=team.html>Nuestro Equipo Geeks</a></li>
<li><a href=contact.html title="Contact Us">Contáctanos</a></li>
<li><a href="https://status.googiehost.com" title="Server Uptime" target="_blank">Server Uptime</a></li>
<li><a href=terms.html>Términos de uso</a></li>
<li><a href=privacy.html>Política de Privacidad</a></li>
</ul>
</div>
<div class="small-12 large-3 medium-3 columns border-right">
<h2>GoogieHost Internacional</h2>
<ul>
<li><a href=dmca-notice.php>Generar aviso DMCA</a></li>
<li><a href="https://googiehost.com/hi">Hindi</a></li>
<li><a href="https://googiehost.com/es">Español</a></li>
<li><a href="https://googiehost.com/id">Indonesia</a></li>
<li><a href="https://googiehost.com/it">Italiano</a></li>
</ul>
</div>
<div class="small-12 large-3 medium-3 columns border-right">
<h2>Clientes de Alojamiento Gratis</h2>
<ul><li><a href="referral.html">Referencia</a></li>
<li><a href="freehostingreview.html">Reseñas de GoogieHost</a></li>
<li><a href="https://googiehost.com/best-hosting-for-wordpress.html">Mejor alojamiento para WordPress</a></li>
<li><a href=seo.html>Optimización para motores de búsqueda</a></li>
<li><a href="https://googiehost.com/blog/">Blog de Soporte</a></li>
</ul>
</div>
<div class="small-12 large-3 medium-3 columns border-left">
<h2>Por qué Elegir GoogieHost?</h2>
<ul>
<li><a href=freedomains.html>Nombre de dominio gratis</a></li>  
<li><a href=freephphosting.html>Alojamiento Gratuito de Php</a></li>
<li><a href=freewebsitebuilder.html>Constructor Gratuito de Sitios web</a></li>
<li><a href=freewordpresshosting.html>Alojamiento Gratuito para wordpress</a></li>
<li><a href=freeautoinstaller.html>Instalador Automático gratuito para sitio web</a></li>
</ul>
</div>
</div>
</div>
</div>
<br/>
<center><a href="https://www.facebook.com/GoogieHost" target="_blank"><img src="../images/fb-icon.png" alt="facebook"></a> <a href="https://www.instagram.com/googiehost/" target="_blank"><img src="../images/instagram.png" alt="instagram"></a>  <a href="https://twitter.com/GoogieHost" target="_blank"><img src="../images/twitter-icon.png" alt="twitter"></a> <a href="https://www.youtube.com/user/GoogieHostFree" target="_blank"><img src="../images/youtube.png" alt="youtube"></a></center>
<p class=copyright>© COPYRIGHTS 2011-2021 GOOGIEHOST, TODOS LOS DERECHOS RESERVADOS</p>
<div align=center>
<div style="color:#a4a4a4;">
Calificación <span>4.7</span>/5 basado en <span>832</span> comentarios en <a href="https://www.google.co.in/search?q=googiehost&oq=googiehost&aqs=chrome..69i57j69i60l3j69i61.2096j0j1" target=_blank style="color:#a4a4a4;">Google</a>
</div>
</div>
</footer>
<a href="#top" id=back-to-top><i class="fa fa-angle-up"></i></a>
<script src="js/vendor/jquery.js"></script>
<script src="js/foundation.min.js"></script>
<script src="js/vendor/hoverIntent.js"></script>
<script src="js/vendor/superfish.min.js"></script>
<script src="js/vendor/morphext.min.js"></script>
<script src="js/vendor/wow.min.js"></script>
<script src="js/vendor/jquery.slicknav.min.js"></script>
<script src="js/vendor/waypoints.min.js"></script>
<script src="js/vendor/jquery.animateNumber.min.js"></script>
<script src="js/vendor/owl.carousel.min.js"></script>
<script src="js/vendor/jquery.slicknav.min.js"></script>
<script src="js/custom.js"></script>

</body>
</html>