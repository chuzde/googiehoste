<!DOCTYPE html>
<html lang="en-gb">
	<head>
		<title>Fastest SSD WordPress Hosting</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="styles/main-style9f89.css?v=0.0.7.9">
		<link rel="stylesheet" href="styles/responsive9baa.css?v=0.0.4.2">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700" rel="stylesheet">
		<link rel="stylesheet" href="use.fontawesome.com/releases/v5.4.1/css/all.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
		<style>
					.bla1-h1dr{font-size:12px;text-transform:uppercase;}
					@media(min-width:1100px){
						._bla1-h1-21{font-size:36px;line-height:1.3;}
					}
					
					@media (min-width:1200px){
						.ultrn-container{margin-top:60px;}
					}
					.prrty-top-reduce{margin-top:-70px;}
					@media (max-width:1250px){
						.prrty-top-reduce{margin-top:-20px;}
					}
					@media(min-width:800px){
						.plans-top-reduce{margin-top:-40px;}
					}
					
					.plntbl-mncntnr{display:none;padding-bottom:40px;}
					.plntbl-mncntnr-activ{display:block;}
					.plntbl{margin-bottom:50px;}
					.plntbl-togl{display:none;}
					
					.prjk-top-increase{margin-top:40px;}
					
					.trymw-mncntnr{margin-top:0;padding-top:20px;}
					.bgnm-txt-trymw{top:10px;}
				/* new code */
		/*Our Wide Range of Web Hosting Solutions*/
					.riod-mncntnr{padding:80px 10px 30px;position:relative;}
					.bgnm-txt-riod{width:80%;top:40px;left:16%;}
					.riod-mnwrapr{max-width:1150px;margin:70px auto 0;display:flex;flex-flow:row wrap;}
					.riod-col-content{width:50%;display:flex;flex-flow:row wrap;}
					.riod-box{width:calc( ( 100% - 35px ) / 2 );margin-bottom:20px;margin-right:35px;padding-bottom:10px;position:relative;cursor:pointer;}
					.riod-col-content .riod-box:nth-child(even){margin-right:0;}
					.riod-box:after{content:'';position:absolute;top:100%;left:0;width:100%;height:1px;background:#D4DADF;}
					.riod-title{font-size:20px;font-weight:600;color:#949698;transition:all 0.3s ease-in-out;}
					.riod-box-active .riod-title{color:#000;}
					.riod-box:hover .riod-title{color:#000;}
					.riod-info{display:none;}
					.riod-button{display:none;}
					.riod-col-screen{width:50%;padding:10px 20px;}
					.riod-screen-wrapr{max-width:450px;margin:0 auto;color:#fff;background:#408de4;border-radius:10px;padding:30px 40px;}
					.riod-screen-title{font-size:22px;font-weight:600;margin-bottom:10px;}
					.riod-screen-info{margin:15px 0;}
					.riod-screen-info p{font-size:16px;line-height:1.4;margin-bottom:10px;}
					.riod-screen-info p:last-child{margin-bottom:0;}
					.riod-screen-button{margin-top:20px;}
					.riod-screen-button a{display:inline-block;color:#fff;background:#FDBF1E;font-size:18px;font-weight:700;padding:10px 35px;transition:all 0.3s ease-in-out;}
					.riod-screen-button a:hover{background:#e7af1c;}
						
					.riod-footer{margin-top:30px;padding:10px;text-align:center;}
					.riod-footer p{font-size:20px;line-height:1.3;color:#828282;}
					.riod-footer p a{color:#010101;border-bottom:1px solid #FEB73B;transition:all 0.3s ease-in-out;}
					.riod-footer p a:hover{color:#191919;border-bottom-color:#e9a328;}
					@media screen and (max-width:1150px){
						.riod-mncntnr{padding:50px 15px 30px;}
						.bgnm-txt-riod{top:20px;}
						.riod-mnwrapr{margin:50px auto 0;}
						.riod-box{width:calc( ( 100% - 20px ) / 2 );margin-bottom:15px;margin-right:20px;padding-bottom:5px;}
						.riod-title{font-size:16px;}
						.riod-screen-wrapr{border-radius:5px;padding:20px;}
						.riod-screen-title{font-size:20px;}
						.riod-screen-info p{font-size:15px;}
						.riod-screen-button a{font-size:15px;padding:7px 25px;}
						.riod-footer p{font-size:15px;}
					}
					@media screen and (max-width:700px){
						.riod-col-content{width:55%;}
						.riod-col-screen{width:45%;padding:0 0 0 10px;}
						.riod-title{font-size:15px;}
						.riod-screen-wrapr{padding:20px 15px}
					}
					@media screen and (max-width:650px){
						.riod-mncntnr{padding:30px 15px 30px;}
						.bgnm-txt-riod{top:10px;width:95%;left:2%;}
					}
					@media screen and (max-width:600px){
						.riod-col-content{width:100%;}
						.riod-col-screen{display:none;}
						.riod-box{width:100%;margin:10px 0 5px;padding-bottom:15px;}
						.riod-info{margin-top:5px;padding:0 5px;}
						.riod-button{margin:10px 0 5px;padding:0 5px;}
						.riod-button a{display:inline-block;color:#fff;background:#FDBF1E;font-size:14px;font-weight:600;padding:6px 15px;transition:all 0.3s ease-in-out;}
						.riod-box-active .riod-info{display:block;}
						.riod-box-active .riod-button{display:block;}
					}
		/*Tired of Your Existing Web Host?*/
					.thro4-mncntnr{background:#f1f6f9;padding:50px 10px 170px;position:relative;}
					.thro4-mnwrapr{max-width:1200px;margin:0 auto;display:flex;flex-flow:row wrap;align-items:center;position:relative;z-index:5;}
					.thro4-col-graphic{width:50%;padding:10px 7%;}
					.thro4-image{width:100%;margin:0 auto;position:relative;}
					.thro4-image:before{content:'';position:absolute;top:0;left:0;width:100%;height:100%;background:transparent;}
					.thro4-image img{display:block;width:100%;}
					.thro4-col-content{width:50%;padding-right:8%;}
					.thro4-title{color:#434343;font-size:48px;font-weight:700;line-height:1.2;margin-bottom:15px;}
					.thro4-info{margin-bottom:15px;}
					.thro4-info p{font-size:18px;line-height:1.4;color:#6B6B6B;}
					.thro4-button a{display:inline-block;font-size:18px;font-weight:600;padding:8px 25px;border-radius:3px;}
					.thro4-footbg{position:absolute;bottom:0;left:0;width:100%;}
					.thro4-footbg div{width:100%;height:0;padding-top:15%;background:url('img-assets/web-host-tired-wave.svg')no-repeat;background-size:100%;}
					@media screen and (max-width:1200px){
						.thro4-mncntnr{padding:30px 10px 100px;}
						.thro4-title{font-size:40px;}
						.thro4-info p{font-size:16px;}
						.thro4-button a{font-size:15px;}
					}
					@media screen and (max-width:1024px){
						.thro4-mncntnr{padding:40px 10px;}
						.thro4-col-graphic{width:40%;padding:10px;}
						.thro4-col-content{width:60%;padding:10px 15px;}
						.thro4-title{font-size:35px;}
					}
					@media screen and (max-width:750px){
						.thro4-col-graphic{width:100%;padding:0 10px;}
						.thro4-col-content{width:100%;padding:10px;text-align:center;}
						.thro4-image{width:300px;}
						.thro4-footbg{top:45%;}
						.thro4-title{font-size:23px;font-weight:600;}
						.thro4-info p{font-size:15px;}
					}
		/*Select Your Brand Name*/
					.domnlk-mncntnr{background:#408de5;padding:50px 10px;}
					.domnlk-hdrttl{color:#fff;}
					.domnlk-form{max-width:950px;margin:40px auto 0;}
					.domnlk-inputs{background:#fff;width:100%;display:flex;flex-flow:row wrap;align-items:center;margin-bottom:10px;padding:5px;border-radius:3px;}
					.domnlk-inputs label{width:100px;text-align:center;}
					.domnlk-inputs input, .domnlk-inputs select{font-size:16px;font-weight:600;padding:10px;}
					.domnlk-inputs input[type=text]{width:calc( 100% - 360px);border-left:1px solid #ddd;border-right:1px solid #ddd;}
					.domnlk-inputs select{width:100px;padding-right:18px;-webkit-appearance:none;cursor:pointer;background:url('img-assets/form/dropdown-arrow.svg')no-repeat,#fff;background-size:15px;background-position:97% 53%;transition:all 0.3s ease-in-out;}
					.domnlk-inputs input[type=submit]{margin-left:10px;width:150px;}
					.domnlk-domains-wrapr{width:100%;display:flex;flex-flow:row wrap;align-items:center;justify-content:space-between;}
					.domnlk-domain-box{display:flex;flex-flow:row wrap;align-items:center;justify-content:center;color:#fff;margin:5px;}
					.domnlk-domain-title{font-size:33px;font-weight:700;margin-right:10px;}
					.domnlk-prices{display:flex;flex-flow:row wrap;align-items:center;margin-top:5px;}
					.domnlk-price-strike{font-size:18px;opacity:0.4;display:flex;flex-flow:row wrap;align-items:center;margin:0 10px;position:relative;padding:0 5px;}
					.domnlk-price-strike:before{content:'';position:absolute;top:50%;left:0;width:100%;height:1px;background:#fff;}
					.domnlk-price-strike:after{content:'/yr';font-size:80%;margin-left:3px;line-height:1;}
					.domnlk-price-real{display:flex;flex-flow:row wrap;align-items:center;font-size:30px;}
					.domnlk-price-real:after{content:'/yr';font-size:80%;margin-left:3px;line-height:1;}
					.domnlk-price-real i{margin-right:5px;}
					@media screen and (max-width:1100px){
						.domnlk-mncntnr{padding:40px 10px;}
						.domnlk-inputs input{font-size:15px;}
						.domnlk-domain-title{font-size:25px;font-weight:600;}
						.domnlk-price-strike{font-size:15px;}
						.domnlk-price-real{font-size:20px;}
					}
					@media screen and (max-width:800px){
						.domnlk-domains-wrapr{width:100%;}
					}
					@media screen and (max-width:750px){
						.domnlk-inputs{background:transparent;padding:0;}
						.domnlk-inputs label{width:65px;background:#fff;padding:10px 5px;}
						.domnlk-inputs select{width:80px;padding:9.3px 5px;}
						.domnlk-inputs input[type=text]{width:calc( 100% - 145px);}
						.domnlk-inputs input[type=submit]{margin:15px 0 0;width:100%;border-radius:3px;}
					}
					@media screen and (max-width:600px){
						.domnlk-domains-wrapr{justify-content:center;}
						.domnlk-domain-box{margin:5px 10px;}
					}
					/* minimal section*/
					.mnml90-mncntnr{background:#F1F6F9;padding:50px 15px;}
					.mnml90-hdrttl{text-align:center;color:#434343;font-size:24px;font-weight:600;margin-bottom:30px;}
					.mnml90-buttons{display:flex;flex-flow:row wrap;justify-content:center;}
					.mnml90-buttons a{display:inline-block;font-size:20px;font-weight:600;margin:13px;padding:13px 35px;transition:all 0.3s ease-in-out;}
					@media screen and (max-width:1024px){
						.mnml90-mncntnr{padding:40px 15px;}
						.mnml90-buttons a{font-size:16px;margin:10px;padding:8px 25px;}
					}
					@media screen and (max-width:700px){
						.mnml90-hdrttl{font-size:22px;margin-bottom:25px;}
					}
					.custrt-mncntnr{padding-top:20px;}
					.bgnm-txt-custrt{top:0;}
					.indpop-btn{cursor:pointer;border-bottom:1px dotted #ec6f30;transition:all 0.3s ease-in-out;}
					.riod-screen-info .indpop-btn{border-bottom:1px dotted #fff;}
					
					/*
					.bla1-buttons a:first-child{margin-right:25px;}
					.bla1-btn-filled{font-size:16px;margin:5px 0;border:1px solid #408de4;}
					.bla1-btn-filled2{color:#408de4;background:#fff;box-shadow:none;}
					*/
					
					.bla1-vid-thumb{position:relative;cursor:pointer;}
					.bla1-vid-thumb:before{
						content: '';position: absolute;top: 0;left: 0;width: 100%;height: 100%;opacity: 0.8;
						background: url('img-assets/youtube/ytsl-play-icon3.svg')no-repeat;
						background-size: 20%;background-position:55% 60%;transition: all 0.3s ease-in-out;z-index: 5;
						}
					.bla1-vid-thumb:hover:before{opacity:1;}
					
					@media (max-width:600px){
						.bla1-vid-thumb:before{background-size: 23%;background-position:55% 52%;}
					}
					
					.jnst-2-top-increase{margin-top:40px;}
					
					
					.fmetp-bla1-h1{margin-bottom:26px;}
					.fmetp-tag-free-domain{position:relative;}
					.fmetp-tag-free-domain:before{content:'';position:absolute;bottom:3px;left:calc(100% - 11px);width:115px;height:34px;background:url('img-assets/tags/tag-free-domain-feb21.svg');background-size:100%;transform:rotate(10deg);transform-origin:left;}
					.fmetp-mncntr{margin:20px 0 35px;}
					.fmetp-hdrttl{font-size:15px;color:#565656;}
					.fmetp-tmr-wrapr{margin-top:10px;display:flex;flex-flow:row wrap;align-items:flex-start;}
					.fmetp-tmbx{text-align:center;margin-right:25px;}
					.fmetp-nmbrs{margin-bottom:4px;position:relative;}
					.fmetp-nmbrs:before, .fmetp-nmbrs:after{content:'';position:absolute;width:8px;height:8px;background:#99d0fe;border-radius:50%;}
					.fmetp-nmbrs:before{top:18px;right:-16px;}
					.fmetp-nmbrs:after{bottom:18px;right:-16px;}
					.fmetp-tmr-wrapr .fmetp-tmbx:last-child .fmetp-nmbrs:before, .fmetp-tmr-wrapr .fmetp-tmbx:last-child .fmetp-nmbrs:after{display:none;}
					.fmetp-nmbrs b{width:40px;display:inline-block;color:#fff;background:#54a8f0;font-size:40px;line-height:0.8;font-weight:600;padding:15px 2px;}
					.fmetp-nmbrs b:first-child{margin-right:2px;}
					.fmetp-mntxt{color:#73acdd;font-size:14px;font-weight:600;text-transform:uppercase;}
					.btn-fmetp-1{border:1px solid #408de4;}
					.btn-fmetp-1:hover{color:#408de4;background:#fff;box-shadow:5px 5px 15px rgba(2,154,248,0.2);border:1px solid #408de4;}
					@media(max-width:1024px){
						.fmetp-bla1-h1{margin-bottom:15px;}
						.fmetp-tag-free-domain:before{bottom:-2px;left:calc(100% - 10px);width:100px;height:30px;}
						.fmetp-mncntr{margin:10px 0 25px;}
						.fmetp-tmbx{margin-right:20px;}
						.fmetp-nmbrs:before, .fmetp-nmbrs:after{width:8px;height:8px;}
						.fmetp-nmbrs:before{top:12px;right:-14px;}
						.fmetp-nmbrs:after{bottom:12px;right:-14px;}
						.fmetp-nmbrs b{width:34px;font-size:30px;padding:12px 2px;}
					}
					@media(max-width:800px){
						.fmetp-nmbrs:before, .fmetp-nmbrs:after{width:5px;height:5px;}
						.fmetp-nmbrs b{width:28px;font-size:25px;padding:10px 1px;}
						.fmetp-nmbrs b:first-child{margin-right:1px;}
						.fmetp-mntxt{font-size:10px;}
					}
					@media(max-width:600px){
						.fmetp-tmr-wrapr{justify-content:center;}
						.fmetp-bla1-h1{margin-bottom:40px;}
					}
					
					
					.xms20-bics-list{margin:30px 0;display:flex;flex-flow:row wrap;}
					.xms20-bics-list li{width:50%;font-size:18px;color:#434343;margin-bottom:8px;position:relative;padding-left:35px;}
					.xms20-bics-list li:nth-child(even){margin-left:10px;width:calc(50% - 10px);}
					.xms20-bics-list li:before{content:'';position:absolute;top:3px;left:0;width:23px;height:19px;background:url('img-assets/xmas-2020/check-mark-434343-green.svg')no-repeat;background-size:100%;}
					.xms20-bics-list li:last-child{margin-bottom:0;}
					.xms20-prcrow-cntnr{display:flex;flex-flow:row wrap;}
					.xms20-prc-hdr{font-size:20px;line-height:1;margin-right:20px;color:#848484;}
					.xms20-prcrow-wrapr{color:#434343;font-size:60px;line-height:1;display:flex;flex-flow:row wrap;align-items:center;}
					.xms20-prcrow-wrapr i{font-size:50%;align-self:flex-start;}
					.xms20-prcrow-wrapr b{font-weight:700;line-height:0.7;margin:0 5px 0 2px;}
					.xms20-prcrow-wrapr u{font-size:33%;font-weight:600;text-decoration:none;}
					@media(max-width:1100px){
						.xms20-bics-list li{width:100%;}
						.xms20-bics-list li:nth-child(even){margin-left:0;width:100%;}
					}
					@media(max-width:950px){
						.xms20-bics-list li{font-size:16px;padding-left:25px;margin-bottom:5px;}
						.xms20-bics-list li:before{top:2px;width:18px;height:15px;}
						.xms20-prc-hdr{font-size:17px;margin-right:15px;}
						.xms20-prcrow-wrapr{font-size:50px;}
						.xms20-prcrow-wrapr u{font-size:40%;}
					}
					@media(max-width:700px){
						.xms20-bics-list{margin:20px 0;}
						.xms20-bla1-buttons{margin-top:30px;}
					}
					@media(max-width:600px){
						.xms20-prcrow-cntnr{justify-content:center;}
						.xms20-bics-list li{display:inline-block;margin:5px 10px;}
					}
					@media(max-width:400px){
						.fmetp-tag-free-domain:before{bottom:-3px;left:2px;width:90px;height:27px;transform:rotate(25deg);}
					}
		@media(min-width:1100px){
			.btn-fmetp-1{font-size:18px;}
		}
		._2hlt{position:relative;z-index:10;color:#13b763;}
		._2hlt:after{display:none;content:'';position:absolute;top:calc(100% - 10px);left:0;width:100%;height:8px;background:#13b763;border-radius:30px;z-index:-1;opacity:0.6;}
		.trplt-mncntnr {background:#fff;}
		</style>
		<link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,400;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
		<link rel="stylesheet" href="styles/style-v0221a398.css?v=20210309164954">
		<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/57c2c3742b03647ba16c8dbe/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
	</head>
	<body>
		

		<div class="top-notice-mncntnr" style="background:#fff;" id="mobile">
			<div class="top-notice-mnwrapr">
				<div class="top-notice-mncol top-notice-mncol-one" style="margin-left: auto;margin-right: auto;">
					<div style="display: flex;
    flex-flow: row wrap;
    justify-content: center; font-weight: bold;">
<div class="jnst-mncol jnst-mncol-a" > 
<span class="text-h" style="font-weight: bold;font-size: 25px;" >Call &nbsp;</span> <a class="text-red blink-soft" class="text-red blink-soft text-h " style="font-weight: bold; font-size: 30px;" href="tel:9616782253" target="_blank"><i class="fas fa-phone-square"></i> +919616782253</a>

</div>

</div>
				</div>
				<!-- <div class="top-notice-mncol top-notice-mncol-two">
					<a href="affiliates.html" target="_blank">Affiliates</a>
					<a href="contact.html">Contact Us</a>
				</div> -->
			</div>
		</div>
		<header class="igfm-mncntnr igfm-sticky-950" >
			<div class="igfm-wrapper">
				<div class="">
					<div class="igfm-logo"><a href="index.html"><img src="images/youstable-logo-2020.png" title="youstable Hosting" style="width: 100%;"></a></div>
					
				</div>
				<div class="jnst-mncol jnst-mncol-a" id="desktop">
<span class="call" style="font-weight: bold; color: #fff;" >Call &nbsp;</span> <a class="text-red blink-soft call" class="text-red blink-soft" style="font-weight: bold; color:#fff;" href="tel:9616782253" target="_blank"><i class="fas fa-phone-square"></i> +919616782253</a>

</div>
			</header>
			<style type="text/css">
				.call{
					font-size: 32px!important;
				}
				.text-red {
  color: red;
}
.blink-hard {
  animation: blinker 1s step-end infinite;
}
.blink-soft {
  animation: blinker 1.5s linear infinite;
}
@keyframes blinker {
  50% {
    opacity: 0;
  }
}

			</style>
			<div class="_f3b-font-pr1m abh1r-mncntnr">
<div class="abh1r-mnwrapr">
<div class="abh1r-mncol abh1r-mncol-content">
<div class="_hedtl-abh1r" id="text">Best SSD Web Hosting in India</h1></div>
<div class="_subhedtl-abh1r">Safe & Reliable SSD Hosting</div>
<ul class="abh1r-quals">
<li class="text-red blink-soft">1
<strong data-t8p0p-popup="YES" data-t8p0p-title="Free Domain" data-t8p0p-content="<p>Register a unique domain name for your business free of charge. With our best web hosting plans you get to register your choice of domain for free.</p>
<p>We provide 1 free .XYZ domain with our Tyro plan and 1 .COM domain with our Swift and Turbo web hosting plan. The client must add the free domain to the shopping cart before making the payment for the web hosting plan.</p>
<p>Domain name will be free only for the first year and renewal charges will be applicable from the second year. To avail the free domain, the cheap web hosting plan should be ordered for 1/3 years.</p>
<p>This offer is valid on new sign-ups only. Choose our cheap web hosting plans and book your free domain now!</p>">
FREE</strong> Domain</li>
<li clas="text-red blink-hard">Trusted
<strong data-t8p0p-popup="YES" data-t8p0p-title="Trusted SSL Certificate" data-t8p0p-content="<p>An SSL Certificate adds a greater level of security to your website. Any data going to and from your website will be encrypted using strong 4096-bit encryption that reduces any chances of data being exposed to attackers. Most importantly our built-in SSL Certificate helps to keep transactions on your website secure.</p>
<p>Once the SSL is activated the URL will contain 'https' that helps in boosting the confidence of your website visitors &amp; helps in increasing your Google rankings.</p>
<p>All the websites hosted on our servers are secured with Let’s Encrypt SSL at no additional cost.</p>">
SSL</strong> Certificate</li>
<li class="text-red blink-soft"><strong data-t8p0p-popup="YES" data-t8p0p-title="WordPress Optimized" data-t8p0p-content="<p>Take your WordPress website online with our WordPress Optimized servers. We furnish the all-in-one site speedup features, including server-level cache and a collection of optimization features.</p>
<p>With WordPress Optimized server, we make your WordPress site highly reliable and secure. Also, you get 20X faster hosting for your WordPress site, enhanced security and best performance with us. Our high-speed servers improve the page load time of your website.</p>">
WordPress</strong> Optimized</li>
<li><strong data-t8p0p-popup="YES" data-t8p0p-title="Lightning Fast Website" data-t8p0p-content="<p>The processing speed of your website is our responsibility. You get the best server resources for your website. The CDN network will keep your data floating around the internet helping your website to load faster by serving your static resources around the world.</p>
<p>The data compression and image compression increases the processing speed of the website. We leverage better browser caching of contents resulting in faster webpage loads. Additionally, the GZIP compression compresses HTML, CSS and JavaScript saving the bandwidth and helps sites achieve more prominent load times.</p>">
Lightning</strong> Fast Website</li>
</ul>
<div class="abh1r-btn-wrapr">
<a class="abh1r-btn-pr1m _f3b-btn-pr1m-clr" href="#plansToJmp" style="background: #f9ca0e; color: #263238;">Buy Now</a>
<a class="abh1r-btn-pr1m _f3b-btn-pr1m-clr" href="#plansToJmp" style="background: #41c4ac; color: #fff;">View Plans</a>
</div>
</div>
<div class="abh1r-mncol abh1r-mncol-graphic _mw-overlay"id="topform">
<img class="abh1r-image _h0l121-image" src="hero.png" alt="Best Web Hosting">
</div>
</div>
</div>
	</div>
	<div class="_f3b-font-pr1m ch9py-mncntnr">
		<div class="ch9py-mnwrapr">
			<div class="ch9py-mncol ch9py-mncol-graphic _mw-overlay">
				<img src="images/chat.png">
			</div>
			<div class="ch9py-mncol ch9py-mncol-content">
				<span>24/7/365 Support Available</span>
			</div>
		</div>
	</div>
	<div style="margin-bottom:80px;"></div>
	<div class="trplt-mncntnr ">
		<div class="trplt-mnwrapr">
			<strong>TRUSTPILOT</strong> rated us <strong>Excellent</strong>
			<span class="trplt-stars"><img src="img-assets/star-ratings/stars-trustpilot.svg"></span>
			with the <span>TrustScore <strong>4.8</strong></span>
			out of 5 based on 2,527 customer reviews.
		</div>
	</div>
	<div class="prjk-mncntnr prjk-mncntnr-no-margin prjk-mncntnr-bg-fff">
		<div class="prjk-mnwrapr">
			<div class="prjk-col">
				<div class="prjk-ico"><i class="far fa-smile"></i></div>
				<div class="prjk-title">28,000+ Happy Customers</div>
			</div>
			<div class="prjk-col">
				<div class="prjk-ico"><i class="fas fa-building"></i></div>
				<div class="prjk-title">Founded 2015</div>
			</div>
			<div class="prjk-col">
				<div class="prjk-ico"><i class="fas fa-star"></i></div>
				<div class="prjk-title">Highest Rated Web Host</div>
			</div>
			<div class="prjk-col">
				<div class="prjk-ico"><i class="fas fa-fist-raised"></i></div>
				<div class="prjk-title">100% Independent</div>
			</div>
			<div class="prjk-col">
				<div class="prjk-ico"><i class="fas fa-server"></i></div>
				<div class="prjk-title">Tier-3 and Tier-4 Datacenter</div>
			</div>
		</div>
	</div>
	<div class="plans-top-reduce"></div>
	<div class="sywhp-mn-cntnr" id="plansToJmp">
		<div class="bgnm-wrapr bgnm-wrapr-sywhp">
			<div class="bgnm-txt bgnm-txt-sywhp-cpanel bgnm-txt-cpanel"><div></div></div>
		</div>
		<h2 class="htwo-prime1 sywhp-bgnm-hdrttl">SEO Optimized Hosting</h2>
		<div class="ultrn-container ultrn-container-3col">
			<div class="ultrn-tag-bsln81 ultrn-tag-bsln81-orange">
			</div>
			
<div class="ultrn-mncol ultrn-mncol2">
<div class="ultrn-tag-bsln81 ultrn-tag-bsln81-orange">
<span class=""><i class="fas fa-star"></i> SAVE 70% <i class="fas fa-star"></i></span>
</div>
<div class="ultrn-patch-top ultrn-patch-top-red"><span>Free Domain</span></div>
<div class="ultrn-mnttl">UNLIMITED HOSTING</div>
<div class="ultwn-selbil"><mark class="mark-yello1">EXTREMELY FAST, SECURE, UNLIMITED HOSTING FOR YOUR SUCCESSFUL ONLINE PROJECTS.</mark></div>
<div class="gatfk-mncntnr">
<div class="gatfk-lnk-cntnr">

<div class="gatfk-s1-wrapr">
<div class="s1-save-wrapr">Save 70%</div>
<div class="s1-dur kl_price_duration">3 Years at</div>
<div class="s1-prc-strik">
<i class="fas fa-rupee-sign"></i><div class="kl_price_sriked">299</div><span>/mo</span>
</div>
<div class="s1-prc-fnl">
<i class="fas fa-rupee-sign"></i><div class="kl_price_discounted">89</div><span>/mo</span>
</div>
</div>
</div>
<div class="satfl-mncntnr">
<div class="satfl-mnwrapr"><div class="satfl-row" data-ultn-bilcycle="monthly"><div class="p1-save" style="display:none;"></div></div><div class="satfl-row" data-ultn-bilcycle="annually"><div class="p1-save" style="">Save 55%</div><div class="p1-dur">1 Year at</div><div class="p1-prc-strik" style="">
<i class="fas fa-rupee-sign"></i><div>299</div><span>/mo</span>
</div><div class="p1-prc-fnl">
<i class="fas fa-rupee-sign"></i><div>134</div><span>/mo</span>
</div></div><div class="satfl-row" data-ultn-bilcycle="triennially"><div class="p1-save" style="">Save 70%</div><div class="p1-dur">3 Years at</div><div class="p1-prc-strik" style="">
<i class="fas fa-rupee-sign"></i><div>299</div><span>/mo</span>
</div><div class="p1-prc-fnl">
<i class="fas fa-rupee-sign"></i><div>89</div><span>/mo</span>
</div></div></div>
</div>
</div>
<div class="ultwn-btn-wrapr">
<a class="btn-blu1-clr no-select ultwn-btn" href="http://bit.ly/398oEce">Buy Now</a>
</div>
<ul class="ultrn-featrs-cntnr">
<li><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div><div class="ftrtxt">Single Website <span class="marktxt"></span> </div></li>
<li class="ultrn-red-list"><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div><div class="ftrtxt"><span style="border-bottom:1px dotted #5c5f67;"  rel="tooltip" >
.COM Free Domain</span></div>
</li>
<li><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div><div class="ftrtxt"><span style="border-bottom:1px dotted #5c5f67;" class="marktxt" rel="tooltip" ></span><span class="marktxt">5GB</span> SSD Disk Space</div>
</li>
<li><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div> <div class="ftrtxt" style="border-bottom:1px dotted #5c5f67;" rel="tooltip" >
Lightspeed Server</div>
</li>
<li><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div><div class="ftrtxt"><span style="border-bottom:1px dotted #5c5f67;" class="marktxt" rel="tooltip" >
Free SSL</span><i class="fas fa-lock lock-clr-green"></i></div>
</li>
<li><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div><div class="ftrtxt"><span style="border-bottom:1px dotted #5c5f67;" class="marktxt" rel="tooltip" >
</span>Unlimited E-MAIL/FTP</div>
</li>
<li><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div><div class="ftrtxt"><span style="border-bottom:1px dotted #5c5f67;" class="marktxt" rel="tooltip" >
</span>Priority Support 24x7x365</div>
</li>

</li>
</div>
<div class="ultrn-mncol ultrn-mncol1">
				<div class="ultrn-mnttl">SEO HOSTING</div>
				<div class="ultwn-selbil"><mark class="mark-yello1">
				RECOMMENDED FOR WEBSITES & BLOGS THAT WANT TO RANK</mark><br><br></div>
				<div class="gatfk-mncntnr">
					<div class="gatfk-lnk-cntnr">
						
						<div class="gatfk-s1-wrapr">
							<div class="s1-save-wrapr">Save 75%</div>
							<div class="s1-dur kl_price_duration">3 Years at</div>
							<div class="s1-prc-strik">
								<i class="fas fa-rupee-sign"></i><div class="kl_price_sriked">376.97</div><span>/mo</span>
							</div>
							<div class="s1-prc-fnl">
								<i class="fas fa-rupee-sign"></i><div class="kl_price_discounted">94.25</div><span>/mo</span>
							</div>
						</div>
					</div>
					<div class="satfl-mncntnr">
						<div class="satfl-mnwrapr"><div class="satfl-row" data-ultn-bilcycle="monthly"><div class="p1-save" style="display:none;"></div><div class="p1-dur">Monthly</div><div class="p1-prc-strik" style="display:none;">
						<i class="fas fa-rupee-sign"></i><div>376.97</div><span>/mo</span>
					</div><div class="p1-prc-fnl">
					<i class="fas fa-rupee-sign"></i><div>94.25</div><span>/mo</span>
					</div></div><div class="satfl-row" data-ultn-bilcycle="annually"><div class="p1-save" style="">Save 55%</div><div class="p1-dur">1 Year at</div><div class="p1-prc-strik" style="">
					<i class="fas fa-rupee-sign"></i><div>376.97</div><span>/mo</span>
				</div><div class="p1-prc-fnl">
				<i class="fas fa-rupee-sign"></i><div>169.65</div><span>/mo</span>
				</div></div><div class="satfl-row" data-ultn-bilcycle="triennially"><div class="p1-save" style="">Save 75%</div><div class="p1-dur">3 Years at</div><div class="p1-prc-strik" style="">
				<i class="fas fa-rupee-sign"></i><div>376.97</div><span>/mo</span>
			</div><div class="p1-prc-fnl">
			<i class="fas fa-rupee-sign"></i><div>94.25</div><span>/mo</span>
		</div></div></div>
	</div>
</div>
<div class="ultwn-btn-wrapr">
	<a class="btn-blu1-clr no-select ultwn-btn" href="https://bit.ly/3si0Lq0">Buy Now</a>
</div>
<ul class="ultrn-featrs-cntnr">
	<div class="ultrn-tag-bsln81 ultrn-tag-bsln81-orange">
		<span class=""><i class="fas fa-star"></i> SAVE 75% <i class="fas fa-star"></i></span>
	</div>
	<li><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div><div class="ftrtxt"><span class="marktxt">1-</span> Website</div></li>
	<li class="ultrn-red-list"><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div><div class="ftrtxt"><span style="border-bottom:1px dotted #5c5f67;"  rel="tooltip" >
	
.IN Free Domain</span></div>
</li>
</li>
<li><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div><div class="ftrtxt"><span class="marktxt">10GB</span> SSD Space</div></li>
<li><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div><div class="ftrtxt"><span style="border-bottom:1px dotted #5c5f67;"  rel="tooltip" >
100GB</span> Bandwidth</div>
</li>
<li><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div><div class="ftrtxt"><span style="border-bottom:1px dotted #5c5f67;"  rel="tooltip" >
2 Email</span> Accounts</div>

</li>
<li><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div> <div class="ftrtxt" style="border-bottom:1px dotted #5c5f67;" rel="tooltip" >
Lightspeed</div>
</li>
<li><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div> <div class="ftrtxt" style="border-bottom:1px dotted #5c5f67;" rel="tooltip">
Daily Backup</div>
</li>

</ul>
</div>
<div class="ultrn-mncol ultrn-mncol1">
<div class="ultrn-tag-bsln81 ultrn-tag-bsln81-orange">
<span class=""><i class="fas fa-star"></i> SAVE 40% <i class="fas fa-star"></i></span>
</div>
<div class="ultrn-mnttl">VPS HOSTING</div>
<div class="ultwn-selbil"><mark class="mark-yello1">RECOMMENDED FOR HIGH-TRAFFIC WEBSITES.</mark><br><br></div>
<div class="gatfk-mncntnr">
<div class="gatfk-lnk-cntnr">

<div class="gatfk-s1-wrapr">
<div class="s1-save-wrapr">Save 40%</div>
<div class="s1-dur kl_price_duration">3 Years at</div>
<div class="s1-prc-strik">
<i class="fas fa-rupee-sign"></i><div class="kl_price_sriked">1141</div><span>/mo</span>
</div>
<div class="s1-prc-fnl">
<i class="fas fa-rupee-sign"></i><div class="kl_price_discounted">799</div><span>/mo</span>
</div>
</div>
</div>

<div class="ultwn-btn-wrapr">
<a class="btn-blu1-clr no-select ultwn-btn" href="http://bit.ly/393XlzG">Buy Now</a>
</div>
<ul class="ultrn-featrs-cntnr">
<li><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div><div class="ftrtxt">Host<span style="border-bottom:1px dotted #5c5f67;" class="marktxt" rel="tooltip" title="Host as many websites as you want under one roof, unless you exceed the given resources or violate our Terms of Service."></span> 2 GB Ram</div>
</li>
<li class="ultrn-red-list"><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div><div class="ftrtxt"><span style="border-bottom:1px dotted #5c5f67;" class="marktxt" rel="tooltip" ></span> 50 GB SSD Disk Space</div>
</li>
<li><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div><div class="ftrtxt"><span style="border-bottom:1px dotted #5c5f67;" class="marktxt" rel="tooltip" >
</span>500 GB Bandwidth</div>
</li>
<li><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div><div class="ftrtxt"><span style="border-bottom:1px dotted #5c5f67;" class="marktxt" rel="tooltip" ></span>Single Cores</div>
</li>
<li><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div><div class="ftrtxt"><span style="border-bottom:1px dotted #5c5f67;" class="marktxt" rel="tooltip" >
Unlimited</span>WebuzoPro-Panel</div>
</li>
<li><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div><div class="ftrtxt"><span style="border-bottom:1px dotted #5c5f67;" class="marktxt" rel="tooltip" ></span> Virtualisation-KVM <i class="fas fa-lock lock-clr-green"></i></div>
</li>
<li><div class="ftrico"><i class="fas fa-arrow-alt-circle-right"></i></div> <div class="ftrtxt" style="border-bottom:1px dotted #5c5f67;" rel="tooltip" >Priority Support 24x7x365</div>
</li>

</ul>
</div>
</div>
</div>

<div class="prrty-top-reduce"></div>
<div class="plntbl-mncntnr plntbl-tooltip-active plntbl-mncntnr-4sticky">
<div class="plntbl-mncntnr-top-padding"></div>
<div class="plntbl-close-bx"><span class="plntbl-close-btn"></span></div>
<h2 class="htwo-prime1 plntbl-hdrttl">Tyro Vs Swift Vs Turbo</h2>
<div class="sbttl-prime1 plntbl-sbttl">
<p>Full features explained in detail</p>
</div>
<div class="plntbl-mnwrapr">
<table class="plntbl">
<thead>
<tr>
<th class="border-add"></th>
<th>Tyro</th>
<th>Swift</th>
<th>Turbo</th>
</tr>
<tr>
<td class="border-add">
<div class="krsn-hdr-ttl">Select Billing Cycle</div>
<div class="krsn-drp-mncntnr">
<div class="krsn-plan-selected" data-krsn-dropdown="yes">
3 Years
</div>
<div class="krsn-drp-wrapr">
<div class="krsn-drp-item" data-krsn-item-dur-num="3yr" data-krsn-item-billcycle="triennially">3 Years</div>
<div class="krsn-drp-item" data-krsn-item-dur-num="1yr" data-krsn-item-billcycle="annually">1 Year</div>
<div class="krsn-drp-item" data-krsn-item-dur-num="1mo" data-krsn-item-billcycle="monthly">Monthly</div>
</div>
</div>
</td>
<td>
<div class="plntbl-dscnt-wrapr" data-krsn-plan-discount="yes" data-krsn-dscnt-3yr="Save 80%" data-krsn-dscnt-1yr="Save 50%" data-krsn-dscnt-1mo="">Save 80%</div>
<div class="plntbl-price-strik-wrapr">
<span class="plntbl-pric-strik-crnc"><i class="fas fa-rupee-sign"></i></span>
<span class="plntbl-pric-strik-num" data-krsn-plan-price-striked="yes" data-krsn-pric-strik-3yr="200" data-krsn-pric-strik-1yr="200" data-krsn-pric-strik-1mo="">200</span>
<span class="plntbl-pric-strik-mo">/mo</span>
</div>
<div class="plntbl-price-wrapr">
<span class="plntbl-pric-crnc"><i class="fas fa-rupee-sign"></i></span>
<span class="plntbl-pric-num plntbl-pric-num-wght500" data-krsn-plan-price-original="yes" data-krsn-pric-3yr="40" data-krsn-pric-1yr="100" data-krsn-pric-1mo="200">40</span>
<span class="plntbl-pric-mo">/mo</span>
</div>
<div class="plntbl-btn"><a class="btngtm-buynow btn-blu1-clr" href="#" data-krsn-btn="#">Add to Cart</a></div> </td>
<td>
<div class="plntbl-dscnt-wrapr" data-krsn-plan-discount="yes" data-krsn-dscnt-3yr="Save 80%" data-krsn-dscnt-1yr="Save 60%" data-krsn-dscnt-1mo="">Save 80%</div>
<div class="plntbl-price-strik-wrapr">
<span class="plntbl-pric-strik-crnc"><i class="fas fa-rupee-sign"></i></span>
<span class="plntbl-pric-strik-num" data-krsn-plan-price-striked="yes" data-krsn-pric-strik-3yr="650" data-krsn-pric-strik-1yr="650" data-krsn-pric-strik-1mo="">650</span>
<span class="plntbl-pric-strik-mo">/mo</span>
</div>
<div class="plntbl-price-wrapr">
<span class="plntbl-pric-crnc"><i class="fas fa-rupee-sign"></i></span>
<span class="plntbl-pric-num plntbl-pric-num-wght500" data-krsn-plan-price-original="yes" data-krsn-pric-3yr="130" data-krsn-pric-1yr="260" data-krsn-pric-1mo="650">130</span>
<span class="plntbl-pric-mo">/mo</span>
</div>
<div class="plntbl-btn"><a class="btngtm-buynow btn-blu1-clr" href="#" data-krsn-btn="#">Add to Cart</a></div> </td>
<td>
<div class="plntbl-dscnt-wrapr" data-krsn-plan-discount="yes" data-krsn-dscnt-3yr="Save 80%" data-krsn-dscnt-1yr="Save 60%" data-krsn-dscnt-1mo="">Save 80%</div>
<div class="plntbl-price-strik-wrapr">
<span class="plntbl-pric-strik-crnc"><i class="fas fa-rupee-sign"></i></span>
<span class="plntbl-pric-strik-num" data-krsn-plan-price-striked="yes" data-krsn-pric-strik-3yr="850" data-krsn-pric-strik-1yr="850" data-krsn-pric-strik-1mo="">850</span>
<span class="plntbl-pric-strik-mo">/mo</span>
</div>
<div class="plntbl-price-wrapr">
<span class="plntbl-pric-crnc"><i class="fas fa-rupee-sign"></i></span>
<span class="plntbl-pric-num plntbl-pric-num-wght500" data-krsn-plan-price-original="yes" data-krsn-pric-3yr="170" data-krsn-pric-1yr="340" data-krsn-pric-1mo="850">170</span>
<span class="plntbl-pric-mo">/mo</span>
</div>
<div class="plntbl-btn"><a class="btngtm-buynow btn-blu1-clr" href="#" data-krsn-btn="#">Add to Cart</a></div> </td>
</tr>
</thead>
<tbody>
<tr>
<td>Host Website <i class="fas fa-info-circle" rel="tooltip" title="The number of websites that you can host in your web hosting account."></i></td>
<td>1</td>
<td>1</td>
<td>Unlimited</td>
</tr>
<tr>
<td>Free Domain <i class="fas fa-info-circle" rel="tooltip" title="Buy Swift/ Turbo plan and get 1 free .COM domain for FREE. To avail this offer you will need to buy these plans for 1/3 years. The domain name will be free for the first year and renewal charges would be applicable from the second year."></i></td>
<td><i class="fas fa-check"></i></td>
<td><i class="fas fa-check"></i></td>
<td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>SSD Disk Space <i class="fas fa-info-circle" rel="tooltip" title="Our storage is based on the modern Solid State Drives. Your website will perform 200% better as compared to the regular drives."></i></td>
<td>1 GB</td>
<td>Unlimited</td>
<td>Unlimited</td>
</tr>
<tr>
<td>Bandwidth <i class="fas fa-info-circle" rel="tooltip" title="It is the amount of data transferred between the Internet users and your website. We provide unlimited data transfer for you to host all types of websites and grow independently for gaining more visitors."></i></td>
<td>Unlimited</td>
<td>Unlimited</td>
<td>Unlimited</td>
</tr>
<tr>
<td>Control Panel <i class="fas fa-info-circle" rel="tooltip" title="The top rated control panel that helps you to manage your domain, website, email, database, etc. from a single dashboard."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>1-Click Installer <i class="fas fa-info-circle" rel="tooltip" title="<strong>1-click Installer:</strong> Easily install popular apps like WordPress, Laravel, PrestaShop and over 400+ such applications with just a single click."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Free Drag and Drop Sitebuilder <i class="fas fa-info-circle" rel="tooltip" title="Create a responsive and beautiful website with our drag-and-drop site builder tool. You don’t need to have any technical skills for building the website."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Sub-Domains <i class="fas fa-info-circle" rel="tooltip" title="A sub-domain helps you to create a separate website under your main domain and host a complete new website on it. For example, blog.youstable.com is the sub-domain of youstable.com domain."></i></td>
<td>Unlimited</td>
<td>Unlimited</td>
<td>Unlimited</td>
</tr>
<tr>
<td>Parked Domains <i class="fas fa-info-circle" rel="tooltip" title="Parked domains are domains linked to your account but point to the primary domain name. This helps in increasing the website’s visibility on the Internet. For example, registering youstable.net and youstable.us and pointing them to youstable.com."></i></td>
<td>Unlimited</td>
<td>Unlimited</td>
<td>Unlimited</td>
</tr>
<tr>
<td>Addon Domains <i class="fas fa-info-circle" rel="tooltip" title="It allows you to add a new domain and launch a new website on it. You can allocate all the features to the add-on domain such as database, ftp, email, forwarders, etc."></i></td>
<td><i class="fas fa-times"></i></td>
<td><i class="fas fa-times"></i></td>
<td>Unlimited</td>
</tr>
<tr>
<td>SSL Certificate <i class="fas fa-info-circle" rel="tooltip" title="We ensure all the websites are secure by offering free and automated installation of SSL certificates on our shared plans. They are easily configured without any validation email. Get automatically renewed and supported by all popular browsers."></i></td>
<td>Unlimited</td>
<td>Unlimited</td>
<td>Unlimited</td>
</tr>
<tr>
<td>FTP Users <i class="fas fa-info-circle" rel="tooltip" title="You can create unlimited FTP user accounts with youstable. It allows you to add and remove FTP accounts. It is also possible to change their passwords."></i></td>
<td>Unlimited</td>
<td>Unlimited</td>
<td>Unlimited</td>
</tr>
<tr>
<td>Automated Backup <i class="fas fa-info-circle" rel="tooltip" title="If you accidentally delete your website data you can restore it with the help of a backup stored at our server. Your database backup will be taken daily in which we will have the backup of your data for the last 7 days. While your website content backup will be taken once in every 7 days."></i></td>
<td>
<div class="plntbl-pric-rw">
<div class="plntbl-prc-activ">
<i class="fas fa-rupee-sign"></i><span>9</span>/mo
</div>
</div>
</td>
<td>
<div class="plntbl-pric-rw">
<div class="plntbl-prc-activ">
<i class="fas fa-rupee-sign"></i><span>21</span>/mo
</div>
</div>
</td>
<td>
<div class="plntbl-pric-rw">
<div class="plntbl-prc-activ">
<i class="fas fa-rupee-sign"></i><span>42</span>/mo
</div>
</div>
</td>
</tr>
</tbody>
<tbody>
<tr class="plntbl-rwmrk">
<td colspan="4">Control Panel Features</td>
</tr>
<tr>
<td>FTP Account Management <i class="fas fa-info-circle" rel="tooltip" title="Keep a track of visitors that are logged into your site via FTP. It also allows you to terminate FTP connections for restricting the file access to unauthorized users."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Virus Scanner <i class="fas fa-info-circle" rel="tooltip" title="The ClamAV Virus Scanner helps in detecting viruses, Trojans, malware and other malicious threats by scanning your entire home directory, mail, public web space and public FTP space."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Image Manager <i class="fas fa-info-circle" rel="tooltip" title="Resize the complete image directory to the desired size and store the images in a folder of thumbnails, resize every single image and change the type of one image into another."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Apache Handlers Manager <i class="fas fa-info-circle" rel="tooltip" title="Makes management of your apache handlers easy. Apache is pre-configured to control CGI-scripts and server-parsed files. It can be manually configured by adding the extension and handler in order to manage a new file type with an existing handler."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Hotlink Protection <i class="fas fa-info-circle" rel="tooltip" title="It doesn’t allow other websites to link to the files on your website directly (e.g. restricts your website’s images from getting displayed on other websites). But if you don’t specify the file type, other websites will be able to link to that file type."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>IP Deny Manager <i class="fas fa-info-circle" rel="tooltip" title="You can decline access to your site from a particular IP address or a range of IP addresses."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Index Manager <i class="fas fa-info-circle" rel="tooltip" title="Customize the listing of directories on the website by selecting between no indexes, a default system setting, or two types of indexing."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Leech Protect <i class="fas fa-info-circle" rel="tooltip" title="It allows you to set the number of logins that can take place within a 2-hour period to detect the unusual level of activities in the password-authorized directories."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Mailman List Manager <i class="fas fa-info-circle" rel="tooltip" title="It allows you to create mailing lists and send mail to multiple email addresses with a single address."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>MIME Types Manager <i class="fas fa-info-circle" rel="tooltip" title="This is used to specify the way web browsers manage certain file extensions. Browsers can manage new technologies as they are available with MIME types."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Network Tools <i class="fas fa-info-circle" rel="tooltip" title="Find out the IP address of any domain along with its DNS information. You can also trace route from the computer via which you access control panel to the server on which your site is located."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>PGP/GPG <i class="fas fa-info-circle" rel="tooltip" title="The GnuPG key encrypts your outgoing emails. Messages are encoded with a public key. The intended recipient that has the private key can only decrypt the message."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Random HTML Generator <i class="fas fa-info-circle" rel="tooltip" title="It selects a HTML code’s string from a list and includes it on the web pages that have SSI enabled on them."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Redirect Manager <i class="fas fa-info-circle" rel="tooltip" title="It enables you to send a domain’s or a page’s visitors to a different URL."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Simple CGI Wrapper <i class="fas fa-info-circle" rel="tooltip" title="It helps to create a folder called /scgi-bin within the /public_html directory so that the scripts placed in it are handled as CGI scripts."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Server Status Viewer <i class="fas fa-info-circle" rel="tooltip" title="With this set of features, you can access several lists to see your server’s information. It will help you while troubleshooting hardware as well as software issues."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Change Language <i class="fas fa-info-circle" rel="tooltip" title="It allows you to change the language of control panel interface easily."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Web Disk <i class="fas fa-info-circle" rel="tooltip" title="It allows you to upload, download, navigate and manage the files of your website pretending they were on your local drive."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Password Protection <i class="fas fa-info-circle" rel="tooltip" title="It enables to secure particular directories in the files of your control panel account. When users try to view a directory via the website, they are requested to log in."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Multiple PHP Support <i class="fas fa-info-circle" rel="tooltip" title="Apart from the default PHP version, our web hosting plans also support other PHP versions."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Customizable php.ini <i class="fas fa-info-circle" rel="tooltip" title="All PHP settings can be managed with your local php.ini file. For example, check if global variables are turned on or the default directory for uploading files to while writing upload scripts."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Cron Jobs <i class="fas fa-info-circle" rel="tooltip" title="These are scheduled tasks that occur at on the server specific times or intervals. Usually, it is a series of simple tasks run from a script file."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Simple DNS Zone Editor <i class="fas fa-info-circle" rel="tooltip" title="Create and delete CNAME and A records for your subdomain names."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Advanced DNS Zone Editor <i class="fas fa-info-circle" rel="tooltip" title="Create and edit all types of DNS records including CNAME, A, TXT and SRV for your domains as well as subdomains."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Install PHP PEAR Packages <i class="fas fa-info-circle" rel="tooltip" title="Collection of functions that help you to execute tasks in PHP is known as PEAR packages."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Install Perl Modules <i class="fas fa-info-circle" rel="tooltip" title="Collection of functions that help you to execute tasks in Perl is called as Perl modules."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Install Ruby Gems <i class="fas fa-info-circle" rel="tooltip" title="Collection of functions that help you to execute tasks in Ruby is called as Ruby Gems."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Backup Manager <i class="fas fa-info-circle" rel="tooltip" title="It helps in downloading a zipped copy of your complete site or parts of it onto your computer or restoring parts of your hosting account by uploading your partial backup zip file(s)."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Git Version Control <i class="fas fa-info-circle" rel="tooltip" title="Host Git repositories on your control panel account easily. You can create, view, clone, and deploy repositories without accessing your account via SSH."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Resource Usage Monitoring <i class="fas fa-info-circle" rel="tooltip" title="Check the statistics for the amount of resources that are used such as memory, CPU and entry processes."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>User Manager <i class="fas fa-info-circle" rel="tooltip" title="It helps to manage the users in your control panel with a single interface. You can add, edit, delete or change passwords of the user accounts."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Style and Preferences Management <i class="fas fa-info-circle" rel="tooltip" title="Allows customizing your control panel interface’s appearance by selecting a style."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Custom Error Pages <i class="fas fa-info-circle" rel="tooltip" title="You can customize the pages displayed when there is an error on your website. These enhance your website's appearance and save you from losing your website visitors."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
</tbody>
<tbody>
<tr class="plntbl-rwmrk">
<td colspan="4">Server Features</td>
</tr>
<tr>
<td>Apache Mod_Lsapi <i class="fas fa-info-circle" rel="tooltip" title="An Apache module uses the LiteSpeed technology for seamless and optimized serving of PHP pages. It is a drop-in replacement for FCGID, SuPHP, RUID2 and ITK and offers superior performance, better compatibility and low memory footprint as compared to PHP-FPM.  Also, it knows directives from .htaccess files."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>HTTP/2 <i class="fas fa-info-circle" rel="tooltip" title="It is the latest version of the HTTP protocol that allows browsers to load websites faster. Our servers support HTTP/2 and it means that people who browse your website using a HTTP/2 ready browser will get website loaded in just few seconds."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>PHP 4.4, 5.1, 5.2, 5.3, 5.4, 5.5, 5.6, 7.0, 7.1, 7.2, 7.3, 7.4 <i class="fas fa-info-circle" rel="tooltip" title="These are the scripting language versions installed on our servers. You can select the PHP version from your control panel."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>MySQL 5.x.x <i class="fas fa-info-circle" rel="tooltip" title="This is the MySQL database version installed on our servers."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>CGI <i class="fas fa-info-circle" rel="tooltip" title="Common Gateway Interface (CGI) creates a way so that the web server can interact with external content-generating programs. CGI scripts are usually written in Python scripts, Perl, or similar such languages and the program run by it can be any type of executable file."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Javascript <i class="fas fa-info-circle" rel="tooltip" title="It is a client-side scripting language that is run in a web browser and allows using .js files."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>SSI <i class="fas fa-info-circle" rel="tooltip" title="A server-side scripting language, SSI is used to create dynamic web pages by including the content of one file into another."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>ImageMagick 6.9.x <i class="fas fa-info-circle" rel="tooltip" title="It is a set of tools and libraries for reading, writing and manipulating an image in different image formats. We offer this version of ImageMagick on our servers."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Zend Optimiser <i class="fas fa-info-circle" rel="tooltip" title="It is a free application that enables PHP to run Zend Guard encoded files to offer enhanced performance of PHP applications."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>GD <i class="fas fa-info-circle" rel="tooltip" title="It is an open source PHP library that enables programs to modify graphics."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Perl 5.10 <i class="fas fa-info-circle" rel="tooltip" title="This is the version of Perl installed on our servers. Perl is an open-source, general-use and cross platform programming language used for web development, system administration, Graphical User Interface development, web programming, etc."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Ruby on Rails <i class="fas fa-info-circle" rel="tooltip" title="It is the most popular open-source web application framework that is built on top of Ruby.  Basically, it is a set of code libraries that offer a ready-made solution for recurring tasks such as developing forms, tables or menus on the website."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Free SSH <i class="fas fa-info-circle" rel="tooltip" title="We enable secure account-restricted access with our free in-house developed SSH. This establishes a secure connection to your account that makes it easy to manage large files as well as databases. Administrators and web developers can securely enable direct access to their hosting account."></i></td>
<td><i class="fas fa-times"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>SFTP Access <i class="fas fa-info-circle" rel="tooltip" title="SFTP (SSH File Transfer Protocol) is a secure file transfer SSH protocol. It ensures complete security of SSH by protecting against password sniffing and other attacks. It authenticates the server and user by using encryptions and cryptographic hash functions."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>WAF Protection <i class="fas fa-info-circle" rel="tooltip" title="WAF helps to secure web applications by filtering, monitoring, and blocking any HTTP traffic between the web application and the web. It restricts the unauthorized data from leaving the app."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>OPCache <i class="fas fa-info-circle" rel="tooltip" title="It is a PHP script compiled into OpCode that boosts the performance of PHP by acting as an extension. This code is stored by OpCache into memory during the execution life-cycle of PHP and the results of the compilation phase are cached for using it afterwards."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Mod_Expires <i class="fas fa-info-circle" rel="tooltip" title="It is an Apache module that leverages browser caching to offer enhanced website performance. Browser caching stores static files locally in the user's browser and decreases page load times when the page is refreshed or the user switches to a different page of your site."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Cloudflare CDN with Railgun <i class="fas fa-info-circle" rel="tooltip" title="Railgun helps to connect each Cloudflare data center with a web server speedily. Due to this, the requests that can’t be served from the Cloudflare cache get served faster."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Laravel <i class="fas fa-info-circle" rel="tooltip" title="It is an open-source and free web application framework that helps in development of web applications. This eases the common tasks such as routing, authentication, caching and sessions."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>CodeIgnitor <i class="fas fa-info-circle" rel="tooltip" title="A PHP-driven framework that offers out of the box libraries to connect to the database and perform different operations such as uploading files, managing sessions, sending emails, and more."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>PHP artisan <i class="fas fa-info-circle" rel="tooltip" title="It is a default command line interface in Laravel that comprises of various commands to develop a Laravel application easily."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Cloudflare <i class="fas fa-info-circle" rel="tooltip" title="Cloudflare caches your website content and distributes it over multiple data centers by enabling your website to load faster in any location of the world. When a visitor accesses your website from any location of the world, Cloudflare delivers the content rapidly from the datacenter near to him. It also identifies and blocks malicious traffic to your website to secure it."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Railgun <i class="fas fa-info-circle" rel="tooltip" title="A WAN optimization technology developed by Cloudflare that helps to rapidly connect Cloudflare datacenter to the web server."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Cron Optimization <i class="fas fa-info-circle" rel="tooltip" title="It is the optimization of scheduled tasks occurring at predefined intervals or times on the server. Basically, a series of simple tasks that are run from a script file is called a cron job."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>CSS & JavaScript Merging <i class="fas fa-info-circle" rel="tooltip" title="Their merging helps in reducing the number of requests to your server and boosts your page load time."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Leverage Browser Caching <i class="fas fa-info-circle" rel="tooltip" title="It enables you to specify the interval for which your web browsers should keep CSS, JS and images stored locally."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Gzip Compression <i class="fas fa-info-circle" rel="tooltip" title="A method of compressing files that reduces the size of resources and delivers smaller file sizes that load speedily for your website users."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Expires Headers <i class="fas fa-info-circle" rel="tooltip" title="These inform the browsers if they should request a particular file from the server or fetch it from the cache of the browser. When these are set for a resource such as all JPEG images, the browser stores them in its cache."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Disabling Logging <i class="fas fa-info-circle" rel="tooltip" title="It allows you to restrict the amount of data that is stored."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>KeepAlive <i class="fas fa-info-circle" rel="tooltip" title="It is a communication between the server and the browser that enables you to transfer multiple files in a single connection."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Mod_rewrite for SEO URLs <i class="fas fa-info-circle" rel="tooltip" title="You can use custom Search Engine Friendly URLs on your website. The path of files on the server is masked by it with a custom good looking name that one can remember easily."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>WP-CLI <i class="fas fa-info-circle" rel="tooltip" title="It is a set of command-line tools to manage the WordPress installation. It can be used for configuring multisite installs, updating plugins and more without the help of web browser."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
</tbody>
<tbody>
<tr class="plntbl-rwmrk">
<td colspan="4">Database Features</td>
</tr>
<tr>
<td>MySQL Databases <i class="fas fa-info-circle" rel="tooltip" title="An open-source relational database management system for storing and retrieving all your blog information and is used by CMS-software. We offer unlimited databases with our Swift and Turbo plans."></i></td>
<td>3</td>
<td>Unlimited</td>
<td>Unlimited</td>
</tr>
<tr>
<td>MySQL DB Size  <i class="fas fa-info-circle" rel="tooltip" title="Represents the data size that can exist on the databases. It may vary from the actual size of MySQL data on the disk."></i></td>
<td>256MB per DB</td>
<td>1GB per DB</td>
<td>1GB per DB</td>
</tr>
<tr>
<td>PHP MyAdmin <i class="fas fa-info-circle" rel="tooltip" title="A free and open-source tool in control panel that is used to manage MySQL databases."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Remote MySQL <i class="fas fa-info-circle" rel="tooltip" title="It enables the user to connect to the database with the help of third party software that doesn’t run on the server."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
</tbody>
<tbody>
<tr class="plntbl-rwmrk">
<td colspan="4">Email Features</td>
</tr>
<tr>
<td>Email Accounts <i class="fas fa-info-circle" rel="tooltip" title="The total number of email accounts you can create for all the domains and subdomains in your hosting account."></i></td>
<td>10</td>
<td>Unlimited</td>
<td>Unlimited</td>
</tr>
<tr>
<td>Email Forwarders <i class="fas fa-info-circle" rel="tooltip" title="It enables you to forward any email’s copy that was sent to one of your email accounts, to some other email address. We allow you to forward unlimited email copies."></i></td>
<td>Unlimited</td>
<td>Unlimited</td>
<td>Unlimited</td>
</tr>
<tr>
<td>Email Autoresponders <i class="fas fa-info-circle" rel="tooltip" title="Any type of automated email/s that can be written once, scheduled and sent automatically. We allow you to set unlimited email autoresponders."></i></td>
<td>Unlimited</td>
<td>Unlimited</td>
<td>Unlimited</td>
</tr>
<tr>
<td>Attachment Limit <i class="fas fa-info-circle" rel="tooltip" title="The attachment limit that can be sent with an email."></i></td>
<td>50 MB</td>
<td>50 MB</td>
<td>50 MB</td>
</tr>
<tr>
<td>Webmail (Horde & RoundCube) <i class="fas fa-info-circle" rel="tooltip" title="It is a type of email account that helps you to access emails from any location with a web browser and Internet connection. We offer Horde and RoundCube clients with our webmail."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>SMTP, POP3, IMAP <i class="fas fa-info-circle" rel="tooltip" title="These are the standard protocols for sending and receiving emails."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>SpamAssassin <i class="fas fa-info-circle" rel="tooltip" title="It is an email-spam filtering tool installed on our servers that can be easily enabled and configured via your Control Panel."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Mailing Lists <i class="fas fa-info-circle" rel="tooltip" title="It enables you to create a list of email addresses and send them an email via a single address. We allow you to create a list of unlimited email addresses."></i></td>
<td>Unlimited</td>
<td>Unlimited</td>
<td>Unlimited</td>
</tr>
<tr>
<td>Catch-all Emails <i class="fas fa-info-circle" rel="tooltip" title="It enables you to receive all emails sent to an incorrect address to the email address specified in your control panel."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Email Aliases <i class="fas fa-info-circle" rel="tooltip" title="It is an email address related to another destination email address. For example, for an email address user@youstable.com, you create an alias user2@youstable.com and someone emails you on it, you will be able to view the same email by logging in to user@youstable.com."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>SPF and DKIM Support <i class="fas fa-info-circle" rel="tooltip" title="SPF enables the email sender to specify the IP addresses that are permitted to send email for a specific domain. An encryption key and digital signature is offered by DKIM for verifying that an email message wasn’t altered or forged."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Domain Keys <i class="fas fa-info-circle" rel="tooltip" title="An email authentication mechanism that enables checking of incoming mail against the server from which it was sent to confirm that email hasn’t been modified."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>BoxTrapper <i class="fas fa-info-circle" rel="tooltip" title="It forces all the email senders that are not on your Whitelist to reply to a verification email prior to receiving the email from them to keep your inbox secured from spam."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Individual Mailbox Storage <i class="fas fa-info-circle" rel="tooltip" title="Given storage is the maximum space that can be allotted to each mail account."></i></td>
<td>500 MB</td>
<td>5 GB</td>
<td>5 GB</td>
</tr>
<tr>
<td>Overall Mailbox Storage <i class="fas fa-info-circle" rel="tooltip" title="Given storage is the maximum mailbox size per domain. You can utilize it as a total space for your overall mailbox."></i></td>
<td>1 GB</td>
<td>50 GB</td>
<td>50 GB</td>
</tr>
<tr>
<td>Email Sends Per Hour <i class="fas fa-info-circle" rel="tooltip" title="Maximum number of emails that can be sent per domain."></i></td>
<td>300</td>
<td>300</td>
<td>300</td>
</tr>
<tr>
<td>CSV Import (Email & Forwarders) <i class="fas fa-info-circle" rel="tooltip" title="It enables you to use .csv files for creating multiple email addresses or email forwarders for your account at the same time."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Mobile Compatibility <i class="fas fa-info-circle" rel="tooltip" title="It allows you to set up emails on all types of mobile devices such as Android, iPhone, Blackberry, Linux, Windows, etc."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Email Calendar <i class="fas fa-info-circle" rel="tooltip" title="You can schedule meetings with anyone who can’t view your email calendar and email a copy of your calendar so that others know when you can be available to them."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Webmail in Gmail <a href="https://youtu.be/QTzM-CYrGLc" target="_blank" rel="nofollow"><i class="fas fa-info-circle" rel="tooltip" title="You can configure webmail in Gmail. Watch the video to know more <a style='color:#fff;' href='https://youtu.be/QTzM-CYrGLc'>https://youtu.be/QTzM-CYrGLc</a>"></i></a></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Outlook / Thunderbird / Mac Mail <i class="fas fa-info-circle" rel="tooltip" title="These are the email applications we offer to send and receive emails."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
</tbody>
<tbody>
<tr class="plntbl-rwmrk">
<td colspan="4">Install Popular Software with 1-Click</td>
</tr>
<tr>
<td>WordPress  <i class="fas fa-info-circle" rel="tooltip" title="WordPress is a popular, free, and open source content management system that helps you to create your own website or blog in an easy way."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Joomla <i class="fas fa-info-circle" rel="tooltip" title="Joomla is an open-source and free content management system that helps in building your website in a mobile-friendly and user-friendly way."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>CubeCart <i class="fas fa-info-circle" rel="tooltip" title="A free, powerful, open-source and responsive ecommerce solution that helps merchants to sell digital or physical products online globally."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>phpBB <i class="fas fa-info-circle" rel="tooltip" title="An open-source and free bulletin board software for PHP that allows you to connect with a group of people or run your complete website."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>SMF <i class="fas fa-info-circle" rel="tooltip" title="SMF stands for Simple Machines Forum and is an open-source, free and easy to use software that enables you to create an online forum or community instantly."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Drupal <i class="fas fa-info-circle" rel="tooltip" title="Drupal is an open-source and free web content management system for building websites and applications used daily."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Blogs <i class="fas fa-info-circle" rel="tooltip" title="A list of software that help you to create a blog just in a few minutes."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Portals <i class="fas fa-info-circle" rel="tooltip" title="A list of CMSs or portals that you can install on your website in few minutes."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Content Management System <i class="fas fa-info-circle" rel="tooltip" title="A software application that enables you to create, manage as well as modify the content on a website."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Customer Support <i class="fas fa-info-circle" rel="tooltip" title="A list of customer support applications that offer different types of contact options for your clients to connect with you."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Discussion Boards <i class="fas fa-info-circle" rel="tooltip" title="A list of online discussion sites where people can have discussions on a topics by posting messages."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>E-Commerce <i class="fas fa-info-circle" rel="tooltip" title="Software that enables buying and selling of products or services on the Internet."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>FAQ <i class="fas fa-info-circle" rel="tooltip" title="Free and database-driven web-based application that allows you to create and maintain Frequently Asked Questions (FAQs) on your site."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Hosting Billing <i class="fas fa-info-circle" rel="tooltip" title="Complete billing and invoicing applications designed for web hosting resellers that help them to manage orders, clients, invoices, notes and help desk."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Image Galleries <i class="fas fa-info-circle" rel="tooltip" title="List of software that enable uploading images to the website. Images are stored on a server and individual different codes get displayed for others to view images."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Polls and Surveys <i class="fas fa-info-circle" rel="tooltip" title="List of software that allows you to create questionnaire and checklist forms."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Project Management <i class="fas fa-info-circle" rel="tooltip" title="List of software that enable you to manage your contacts, privileges, time card, calendar, chat, projects, forum, mail client, request tracker, notes, files, bookmarks, reminder, to-do list, voting and language support."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Site Builders <i class="fas fa-info-circle" rel="tooltip" title="The software that helps in building, maintaining and managing your personal or business website."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Wiki <i class="fas fa-info-circle" rel="tooltip" title="List of software that enable you to create and edit unlimited interlinked web pages via a web browser with the help of a simple markup language or a WYSIWYG text editor."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
</tbody>
<tbody>
<tr class="plntbl-rwmrk">
<td colspan="4">Statistic / Web Stats</td>
</tr>
<tr>
<td>Awstats <i class="fas fa-info-circle" rel="tooltip" title="It displays the information about who is accessing your website in the form of graphs and tables distributed into hourly, daily and monthly averages."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Raw Logs Statistics <i class="fas fa-info-circle" rel="tooltip" title="Text files that include your visitors’ information as well as the content they have access on your website are called as raw access logs."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Error Logs <i class="fas fa-info-circle" rel="tooltip" title="It displays the last 300 errors occurred on your website and this data can be used for searching and fixing the broken links and web applications that are incorrectly configured."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Bandwidth Stats <i class="fas fa-info-circle" rel="tooltip" title="Shows the information of bandwidth usage and enables you to check the high traffic intervals for your website. You can also make a decision for your bandwidth requirement with these stats."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Disk Usage Viewer <i class="fas fa-info-circle" rel="tooltip" title="Displays the information about your disk space usage."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Latest Visitors <i class="fas fa-info-circle" rel="tooltip" title="Exhibits the information related to the last 300 visitors on your website."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Google Analytics Compatible <i class="fas fa-info-circle" rel="tooltip" title="It is compatible with Google Analytics."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Google Webmaster Compatible <i class="fas fa-info-circle" rel="tooltip" title="It is fully compatible with Google Webmaster Tools."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
</tbody>
<tbody>
<tr class="plntbl-rwmrk">
<td colspan="4">Security Solutions</td>
</tr>
<tr>
<td>Network Firewall <i class="fas fa-info-circle" rel="tooltip" title="Prohibits or mitigates unauthorized users from accessing a private network that is linked to the Internet."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Web Application Firewall <i class="fas fa-info-circle" rel="tooltip" title="Secures your website from malicious attacks such as Cross-site Scripting (XSS), SQL injection, Local File Include, XML-RPC DDoS Attack, Remote File Include, etc."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Brute-force Protection <i class="fas fa-info-circle" rel="tooltip" title="Protects your website from the brute-force attack in which repeated attempts are made to identify your username and password to gain unauthorized access to your website."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Exploits and Malware Protect <i class="fas fa-info-circle" rel="tooltip" title="Secures your website from exploits and malware by blocking malware’s attempts to exploit the vulnerabilities."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Malware Scan and Reports <i class="fas fa-info-circle" rel="tooltip" title="Our malware scan tool scans your website for the malware and provides a report for the same."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Two-Factor Authentication (2FA) <i class="fas fa-info-circle" rel="tooltip" title="It is a two-step verification process in which two different authentication factors are offered for verifying the user’s identity."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>BitNinja Server Security <i class="fas fa-info-circle" rel="tooltip" title="It secures your website from XSS, malware, enumeration, script injection, brute force and other automated attacks on all major protocols."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Account Isolation <i class="fas fa-info-circle" rel="tooltip" title="All web hosting accounts are isolated from the other hosting accounts on the server. This helps to improve your data security and provides an additional security layer to the hosting environment."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>CageFS Security <i class="fas fa-info-circle" rel="tooltip" title="A virtualized file system, comprising of a set of tools that encapsulate each user in its own “cage”. You will get a fully functional package without any restriction."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>CloudLinux Servers <i class="fas fa-info-circle" rel="tooltip" title="CloudLinux OS helps in enhancing the server stability and security by fully isolating user accounts and allocating resources. With this, you get the feel of a virtual server on a shared server."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Power / Network / Hardware Redundancy <i class="fas fa-info-circle" rel="tooltip" title="Our datacenters offer high quality electricity redundancy along with multiple failover options that comprise of enterprise-class UPS technology, multiple power feeds and own power generators. There is also a provision of on-site replacement parts for each server hardware model in operation to promote instant reaction when there is a hardware issue."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
</tbody>
<tbody>
<tr class="plntbl-rwmrk">
<td colspan="4">Account Resources</td>
</tr>
<tr>
<td>CPU Core <i class="fas fa-info-circle" rel="tooltip" title="A CPU’s processor is called a CPU core. It is the number of CPU cores offered with our web hosting plans."></i></td>
<td>1</td>
<td>2</td>
<td>3</td>
</tr>
<tr>
<td>RAM <i class="fas fa-info-circle" rel="tooltip" title="The amount of memory offered with each hosting plan."></i></td>
<td>1 GB</td>
<td>2 GB</td>
<td>3 GB</td>
</tr>
<tr>
<td>Concurrent connections (EP) <i class="fas fa-info-circle" rel="tooltip" title="These are the number of connections that happen simultaneously as another connection."></i></td>
<td>20</td>
<td>40</td>
<td>60</td>
</tr>
<tr>
<td>Number of processes (nPROC) <i class="fas fa-info-circle" rel="tooltip" title="These are the maximum number of processes run on the system."></i></td>
<td>40</td>
<td>80</td>
<td>120</td>
</tr>
<tr>
<td>IO Limit <i class="fas fa-info-circle" rel="tooltip" title="Represents the disk input and output limit."></i></td>
<td>1 MBPS</td>
<td>1 MBPS</td>
<td>5 MBPS</td>
</tr>
<tr>
<td>File (Inode) Limit <i class="fas fa-info-circle" rel="tooltip" title="The amount of inodes (files, folders, and emails) that you can store in your hosting account."></i></td>
<td>200000</td>
<td>400000</td>
<td>600000</td>
</tr>
</tbody>
<tbody>
<tr class="plntbl-rwmrk">
<td colspan="4">Our Guarantees</td>
</tr>
<tr>
<td>30 Day Money Back Guarantee <i class="fas fa-info-circle" rel="tooltip" title="If you aren’t satisfied with our services, you can ask for the refund within 30 days of the service purchase."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>99.95% Uptime Guarantee <i class="fas fa-info-circle" rel="tooltip" title="The maximum uptime that helps to keep your website up and running."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Upgrade on Pro Rata Basis <i class="fas fa-info-circle" rel="tooltip" title="Upgrade plans at any time and immediately start using the new features. You are charged for the upgrades based on the time interval for which the service was active."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>24/7/365 Expert Support <i class="fas fa-info-circle" rel="tooltip" title="We are available 24/7/365 to resolve your issues instantly as well as professionally."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Free Website Migration  <i class="fas fa-info-circle" rel="tooltip" title="We don’t charge you for website migration to our servers. You can migrate your website at any time."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Instant Setup <i class="fas fa-info-circle" rel="tooltip" title="We setup your hosting account immediately after your payment is verified."></i></td>
<td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td> <td><i class="fas fa-check"></i></td>
</tr>
<tr>
<td>Free Video Tutorials</td>
<td colspan="3"><a style="color:#408de4;text-decoration:underline;" href="https://www.youtube.com/user/youstableHosting/videos" rel="nofollow" target="_blank"><i class="fab fa-youtube"></i> View Free Video Tutorials</a></td>
</tr>
</tbody>
</table>
</div>
<div class="plntbl-togl">
<span class="plntbl-tglbtn plntbl-tglbtn-show">View More <i class="fas fa-plus-circle"></i></span>
</div>
<div class="jnst-mncntnr jnst-mncntnr-plntbl">
<div class="jnst-mnwrapr">
<div class="jnst-mncol jnst-mncol-a">
<span>Have Questions? Call</span> <a class="text-red blink-soft" class="text-red blink-soft" href="tel:9616782253" target="_blank"><i class="fas fa-phone-square"></i> +919616782253</a>
</div>
<div class="jnst-mncol jnst-mncol-b">
<span>or start a Sales</span> <a href="javascript:void(Tawk_API.toggle())"><i class="fas fa-comment-alt"></i> Chat Now!</a>
</div>
</div>
</div>
</div>
<div class="prrty-mncntnr prime1-mncntnr90">
<div class="bgnm-wrapr bgnm-wrapr-prrty">
<div class="bgnm-txt bgnm-txt-prrty bgnm-txt-24x7x365"><div></div></div>
</div>
<h2 class="htwo-prime1 prrty-hdrttl">You're Our Priority 24x7x365</h2>
<div class="prrty-mndsc"><p>We are not just another web hosting company. We strive to provide outstanding, brisk and steadfast hosting services which is backed by our technical experts round the clock.</p></div>
<div class="prrty-mnwrapr">
<div class="prrty-mncol prrty-mncol1">
<div class="prrty-main-icon"><img src="support.svg" alt="24x7x365 Web Hosting Support"></div>
</div>
<div class="prrty-mncol prrty-mncol2">
<div class="prrty-featr-row prrty-featr-row1">
<div class="prrty-featr-ico prrty-featr-ico1"><div></div></div>
<div class="prrty-featr-content">
<div class="prrty-featr-hdrttl">99.9% Uptime</div>
<div class="prrty-featr-txt"><p>We ensure that you have a great experience while hosting your websites on our server. Whenever you need any technical assistance, our experts are available 24/7/365 to fix your technical issues.</p></div>
</div>
</div>
<div class="prrty-featr-row prrty-featr-row2">
<div class="prrty-featr-ico prrty-featr-ico2"><div></div></div>
<div class="prrty-featr-content">
<div class="prrty-featr-hdrttl">Dual-Shield Security</div>
<div class="prrty-featr-txt"><p>We offer you the dual-shield security for a smooth, affordable and secure hosting service. Dual-shield firewall enables to block intruders without even knowing them. You can block and report them right from your dashboard!</p></div>
</div>
</div>
<div class="prrty-featr-row prrty-featr-row3">
<div class="prrty-featr-ico prrty-featr-ico3"><div></div></div>
<div class="prrty-featr-content">
<div class="prrty-featr-hdrttl">24x7 Support</div>
<div class="prrty-featr-txt"><p>While you manage your site, we ensure that you have the best web hosting experience with cPanel. You can call, chat, raise a ticket or even email for resolving your queries. For a seamless experience, it’s important to guide you through each step, RIGHT?</p></div>
</div>
</div>
</div>
</div>
</div>
<div class="jnst-mncntnr">
<div class="jnst-mnwrapr">
<div class="jnst-mncol jnst-mncol-a">
<span>Call &nbsp;</span> <a class="text-red blink-soft" class="text-red blink-soft" href="tel:9616782253" target="_blank"><i class="fas fa-phone-square"></i> +919616782253</a>

</div>
<div class="jnst-mncol jnst-mncol-b">
<span>or start a Sales</span> <a href="javascript:void(Tawk_API.toggle())"><i class="fas fa-comment-alt"></i> Chat Now!</a>
</div>
</div>
</div>
<div class="custrt-mncntnr prime1-mncntnr70">
<div class="bgnm-wrapr bgnm-wrapr-custrt">
<div class="bgnm-txt bgnm-txt-custrt bgnm-txt-ratings"><div></div></div>
</div>
<div class="htwo-prime1 custrt-hdrttl">The Highest Rated Web Hosting Company</div>
<div class="sbttl-prime1">Checkout our ratings on the most popular review platforms.</div>
<div class="hirat-mnwrapr">
<div class="hirat-box">
<div class="hirat-col hirat-col-a">
4.8 <span>/ 5</span>
</div>
<div class="hirat-col hirat-col-b">
<span class="hirat-logo">
<img src="img-assets/star-ratings/review-logo-google.png" src="img-assets/star-ratings/reviews-stars.svg" alt="trustpilot" />
</span>
<div class="hirat-star"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
</div>
</div>
<div class="hirat-box">
<div class="hirat-col hirat-col-a">
4.8 <span>/ 5</span>
</div>
<div class="hirat-col hirat-col-b">
<span class="hirat-logo">
<img src="img-assets/star-ratings/review-logo-trustpilot.png" src="img-assets/star-ratings/reviews-stars.svg" alt="trustpilot" />
</span>
<div class="hirat-star"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
</div>
</div>
<div class="hirat-box">
<div class="hirat-col hirat-col-a">
4.5 <span>/ 5</span>
</div>
<div class="hirat-col hirat-col-b">
<span class="hirat-logo">
<img src="img-assets/star-ratings/review-logo-hostsearch.png" src="img-assets/star-ratings/reviews-stars.svg" alt="hostsearch" />
</span>
<div class="hirat-star"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
</div>
</div>
<div class="hirat-box">
<div class="hirat-col hirat-col-a">
9 <span>/ 10</span>
</div>
<div class="hirat-col hirat-col-b">
<span class="hirat-logo">
<img src="img-assets/star-ratings/review-logo-hostadvice.png" src="img-assets/star-ratings/reviews-stars.svg" alt="hostadvice" />
</span>
<div class="hirat-star"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
</div>
</div>
<div class="hirat-box">
<div class="hirat-col hirat-col-a">
4.6 <span>/ 5</span>
</div>
<div class="hirat-col hirat-col-b">
<span class="hirat-logo">
<img src="img-assets/star-ratings/review-logo-searchen.png" src="img-assets/star-ratings/reviews-stars.svg" alt="searchen" />
</span>
<div class="hirat-star"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
</div>
</div>
<div class="hirat-box">
<div class="hirat-col hirat-col-a">
4.6 <span>/ 5</span>
</div>
<div class="hirat-col hirat-col-b">
<span class="hirat-logo">
<img src="img-assets/star-ratings/review-logo-hostreview.png" src="img-assets/star-ratings/reviews-stars.svg" alt="hostreview" />
</span>
<div class="hirat-star"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
</div>
</div>
</div>
</div>
<script type="b480f5482bacb4a692933e4a-text/javascript">
	$(window).bind('scroll', function () {
		if ($(window).scrollTop() > 25 ) {
			$('[data-hirat-logo]').each(function(){
				var logo_hirat = $(this).attr('data-hirat-logo');
				$(this).attr('src', logo_hirat);
			});
		}
	});
</script>
<div class="awdnv-cntnr">
<div class="awdnv-wrapr">
<div class="awdnv-col awdnv-col-graphic">
<div class="awdnv-award"><div></div></div>
</div>
<div class="awdnv-col awdnv-col-content">
<div class="awdnv-title">An Award-Winning Web Host</div>
<div class="awdnv-info">
<p>Get the high-quality and best web hosting at Youstable. We are always appreciated and awarded for Performance, Reliability, Security and Customer Service. Customers trust us because they believe that they are in safe hands with Youstable and that’s the reason we have earned </p> </div>
</div>
</div>
</div>
<div class="_f3b-font-pr1m t1ksh-mncntnr">
<h2 class="_hedtl-t1ksh _f3b-h2-pr1m">With All Hosting Plans You Get</h2>
<ul class="t1ksh-list t1ksh-list-4col">
<li rel="tooltipvfeb21" title="The support team of our company is available 24/7 at any given instance to deal with all web hosting issues. The in-time responsive nature of the company is making customers attract to various hosting plans. The longing of our support team to respond to critical issues within a few minutes is making the company proud in all sense.">
24/7/365 Support</li>
<li rel="tooltipvfeb21" title="The availability and reliability of your webpages on the internet, measured in percentage. Without any difficulty, you can use the website at a 99.95% uptime guarantee.">
99.95% Uptime Guarantee</li>
<li rel="tooltipvfeb21" title="Our shared server is fully optimized for running WordPress websites, that gives you optimum speed and performance.">
WordPress Optimized</li>
<li rel="tooltipvfeb21" title="Using the backup tool of the control panel you can manually take the backup of your website files and download them on your local machine. <br style='margin-bottom:10px'>We also provide paid backup service, wherein the backup is taken on a separate server automatically. If you accidentally delete your website data or have forgotten to take the manual backup you can restore your important website files with the help of a backup stored at our server. In this, your database backup will be taken daily in which we will have the backup of your data for the last 7 days. While your website content backup will be taken once in every 7 days. <br style='margin-bottom:10px'>You can opt-in for our automatic website backup service during checkout.">
Backups</li>
<li rel="tooltipvfeb21" title="The easiest way to create a professional website within minutes. Setup your business online with a simple website builder tool, free templates and simple drag-and-drop functionality.">
Website Builder</li>
<li rel="tooltipvfeb21" title="Easily install popular apps like WordPress, Laravel, PrestaShop and over 400+ such applications with just a single click.">
1-Click Installer</li>
<li rel="tooltipvfeb21" title="It is a type of email account that helps you to access emails from any location with a web browser and Internet connection. We offer Horde and RoundCube clients with our webmail.">
Webmail Access</li>
<li rel="tooltipvfeb21" title="Create and delete CNAME and A records for your subdomain names.">
DNS Management</li>
<li rel="tooltipvfeb21" title="You can create unlimited FTP user accounts with MilesWeb. It allows you to add and remove FTP accounts. It is also possible to change their passwords.">
FTP Over SSL</li>
<li rel="tooltipvfeb21" title="The amount of inodes (files, folders, and emails) that you can store in your hosting account.">
File Manager</li>
<li rel="tooltipvfeb21" title="Apart from the default PHP version, our web hosting plans also support other PHP versions: 5.2, 5.3, 5.4, 5.5, 5.6, 7.0, 7.1, 7.2, 7.3, 7.4">
Multiple PHP versions</li>
<li rel="tooltipvfeb21" title="All PHP settings can be managed with your local php.ini file. For example, check if global variables are turned on or the default directory for uploading files to while writing upload scripts.">
PHP Configuration</li>
<li rel="tooltipvfeb21" title="It enables the user to connect to the database with the help of third party software that doesn’t run on the server.">
Remote MySQL</li>
<li rel="tooltipvfeb21" title="A free and open-source tool in control panel that is used to manage MySQL databases.">
phpMyadmin</li>
<li rel="tooltipvfeb21" title="The InnoDB helps in data compression, encryption, caching, back-up and much more. We have the InnoDB storage engine for your MySQL and MariaDB DBMS to take care of data.">
InnoDB</li>
<li rel="tooltipvfeb21" title="Cache Manager enhances your website loading speed. Our web servers have built-in caching, improving the web browsing experience. If you are using WordPress the cache plugin will get installed by default.">
Cache Manager</li>
<li rel="tooltipvfeb21" title="These are scheduled tasks that occur at on the server specific times or intervals. Usually, it is a series of simple tasks run from a script file.">
Cronjobs</li>
<li rel="tooltipvfeb21" title="CloudLinux OS helps in enhancing the server stability and security by fully isolating user accounts and allocating resources. With this, you get the feel of a virtual server on a shared server.">
Cloudlinux</li>
<li rel="tooltipvfeb21" title="Your files, folders, libraries are secure as curl performs SSL certificate verification on incoming and outgoing website data.">
Curl and Curl SSL</li>
<li rel="tooltipvfeb21" title="It doesn’t allow other websites to link to the files on your website directly (e.g. restricts your website’s images from getting displayed on other websites). But if you don’t specify the file type, other websites will be able to link to that file type.">
Hotlink Protection</li>
<li rel="tooltipvfeb21" title="Our LiteSpeed server saves resources without sacrificing performance, safety, compatibility, or convenience. LiteSpeed Web Server (LSWS) is compatible with multiple features including mod_rewrite, .htaccess, and mod_security. ">
LiteSpeed</li>
<li rel="tooltipvfeb21" title="We support an easy-to-use framework, especially for web developers to develop web-based apps rapidly using PHP. The CodeIgniter, working on the Model-View-Controller framework that will help you develop flexible and responsive web and mobile apps.">
CodeIgniter</li>
<li rel="tooltipvfeb21" title="A sub-domain helps you to create a separate website under your main domain and host a complete new website on it. For example, blog.milesweb.com is the sub-domain of milesweb.com domain.">
Unlimited Sub Domains</li>
<li rel="tooltipvfeb21" title="Parked domains are domains linked to your account but point to the primary domain name. This helps in increasing the website’s visibility on the Internet. For example, registering milesweb.net and milesweb.us and pointing them to milesweb.com.">
Unlimited Parked Domains</li>
<li rel="tooltipvfeb21" title="Keep a track of visitors that are logged into your site via FTP. It also allows you to terminate FTP connections for restricting the file access to unauthorized users.">
Unlimited FTP Accounts</li>
<li rel="tooltipvfeb21" title="Cloudflare caches your website content and distributes it over multiple data centers by enabling your website to load faster in any location of the world. When a visitor accesses your website from any location of the world, Cloudflare delivers the content rapidly from the datacenter near to him. It also identifies and blocks malicious traffic to your website to secure it.">
Cloudflare</li>
<li rel="tooltipvfeb21" title="These are the standard protocols for sending and receiving emails.">
SMTP, POP3, IMAP</li>
<li rel="tooltipvfeb21" title="All web hosting accounts are isolated from the other hosting accounts on the server. This helps to improve your data security and provides an additional security layer to the hosting environment.">
Account Isolation</li>
</ul>
</div>





<div class="thro4-mncntnr">
<div class="thro4-mnwrapr">
<div class="thro4-mncol thro4-col-graphic">
<div class="thro4-image"><img src="ezgif.com-gif-maker.png"></div>
</div>
<div class="thro4-mncol thro4-col-content">
<div class="thro4-content">
<div class="thro4-title">GET Ready to step into the Digital World</div>
<div class="thro4-info"><p>As you embark your journey with the best web hosting company - YouStable, suit up yourself to enjoy seamless experience while operating your website. We are here to guide you at each step of the web hosting experience.</p></div>
<div class="thro4-button"><a style="background:#12ae36; color:#fff;text-align:center; text-transform:uppercase;"class="btn-blu1-clr" href="#plansToJmp">Grab Deal Now</a></div>
</div>
</div>
</div>
<div class="thro4-footbg"><div></div></div>
</div>
<div class="vidsc76-mncntnr">
<div class="vidsc76-mnwrapr">
<div class="vidsc76-mncol vidsc76-content-col">
<div class="vidsc76-content">
<div class="vidsc76-title">Cheap Web Hosting with Free Domain in India</div>
<div class="vidsc76-info"><p style="text-align:left;">It is painful to purchase a Domain Name and web hosting separately. Hence, we provide both of these together*. Whether you want the best web hosting for unlimited websites or small business, we make sure that your needs do not go unnoticed. We have categorized our plans accordingly for making available best and cheap web hosting for you. Every web hosting plan includes a .in domain for free. Moreover, we customise it for the hassle-free experience. No huss and fuss! You only have to purchase and place your website.</p></div>
</div>
</div>
<div class="vidsc76-mncol vidsc76-graphic-col">
<div class="vidsc76-image" data-ytsl-embed="xutRk896xnY" id="playtbVid1"><img src="youstable-thumb.jpg"></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<script type="b480f5482bacb4a692933e4a-text/javascript">
		$(window).bind('scroll', function () {
			if ($(window).scrollTop() > 15) {
				var primeVidThumb = $('[data-prime-thumbnail]').attr('data-prime-thumbnail');
				$('.nwvds-jumbo-thumbnail').css({'background':'url("'+primeVidThumb+'")no-repeat','background-size':'100%'});
			}
		});
		$(document).on('click', '[data-nwvds-embed]', function(e) {
			$('html, body').delay(250).animate({scrollTop: $('[data-nwvds-main-wrapper]').offset().top +80}, 400);
			$('.nwvds-video-item').removeClass('nwvds-video-item-active');
			$(this).addClass('nwvds-video-item-active');
			var nwvds_vid_id = $(this).attr('data-nwvds-embed');
			var nwvds_vid_title = $(this).attr('data-nwvds-title');
			$('[data-jumbo-screen-video]').html('<div class="nwvds-jumbo-video-frame"><iframe src="https://www.youtube.com/embed/'+nwvds_vid_id+'?rel=0&autoplay=1" allowfullscreen=""></iframe></div>');
			$('[data-jumbo-screen-title]').html(nwvds_vid_title);
		});
</script>
<div class="mnml90-mncntnr">
<div class="mnml90-hdrttl">Ready to take your step into the digital world?</div>
<div class="mnml90-buttons">
<a class="btn-blu1-clr" href="#plansToJmp">View Plans</a>
<a style="background:#12ae36; color:#fff" class="btn-blu1-clr" href="javascript:void(Tawk_API.toggle())">Chat now</a>

</div>
</div>
<div class="tstmnl-mncntnr prime1-mncntnr70" id="customerStories">
<div class="bgnm-wrapr bgnm-wrapr-tstmnl">
<div class="bgnm-txt bgnm-txt-tstmnl bgnm-txt-customers"><div></div></div>
</div>
<div class="_f3b-font-pr1m kr0b3-mncntnr">
<h3 class="_hedtl-kr0b3 _f3b-h2-pr1m">User-Friendly Control Panel</h3>
<div class="kr0b3-mnwrapr">
<div class="kr0b3-mncol kr0b3-mncol-quals">
<div class="kr1b4-box">
<div class="kr1b4-box-body">
<div class="">
<div class="kr1b4-image _mw-overlay">
<img src="wordpress.svg" alt="Cheapest Web Hosting">
</div>
<div class="kr1b4-title">WordPress</div>
</div>
<div class="kr1b4-box_back">
<div class="kr1b4-image _mw-overlay">
<img src="1-click-installer.svg" alt="Cheap Web Hosting">
</div>
<div class="kr1b4-title">1 Click Installer</div>
</div>
</div>
</div>
<div class="kr1b4-box">
<div class="kr1b4-box-body">
<div class="">
<div class="kr1b4-image _mw-overlay">
<img src="email-accounts.svg" alt="Web Hosting">
</div>
<div class="kr1b4-title">Email Accounts</div>
</div>
<div class="kr1b4-box_back">
<div class="kr1b4-image _mw-overlay">
<img src="webmail.svg" alt="Web Hosting Plans">
</div>
<div class="kr1b4-title">Webmail</div>
</div>
</div>
</div>
<div class="kr1b4-box">
<div class="kr1b4-box-body">
<div class="">
<div class="kr1b4-image _mw-overlay">
<img src="mysql-management.svg" alt="Affordable Web Hosting">
</div>
<div class="kr1b4-title">MySQL Management</div>
</div>
<div class="kr1b4-box_back">
<div class="kr1b4-image _mw-overlay">
<img src="phpmyadmin.svg" alt="Best Cheap Web Hosting">
</div>
<div class="kr1b4-title">phpMyAdmin</div>
</div>
</div>
</div>
<div class="kr1b4-box">
<div class="kr1b4-box-body">
<div class="">
<div class="kr1b4-image _mw-overlay">
<img src="select-php-version.svg" alt="Web Hosting India">
</div>
<div class="kr1b4-title">Select PHP Version</div>
</div>
<div class="kr1b4-box_back">
<div class="kr1b4-image _mw-overlay">
<img src="php-settings.svg" alt="Web Hosting India">
</div>
<div class="kr1b4-title">PHP Settings</div>
</div>
</div>
</div>
<div class="kr1b4-box">
<div class="kr1b4-box-body">
<div class="">
<div class="kr1b4-image _mw-overlay">
<img src="ssl-certificates.svg" alt="Best Cheap Web Hosting">
</div>
<div class="kr1b4-title">SSL Certificates</div>
</div>
<div class="kr1b4-box_back">
<div class="kr1b4-image _mw-overlay">
<img src="password-protected-directories.svg" alt="Web Hosting Providers">
</div>
<div class="kr1b4-title">Password Protected Directories</div>
</div>
</div>
</div>
<div class="kr1b4-box">
<div class="kr1b4-box-body">
<div class="">
<div class="kr1b4-image _mw-overlay">
<img src="site-builder.svg" alt="Best Web Hosting Company">
</div>
<div class="kr1b4-title">Site Builder</div>
</div>
<div class="kr1b4-box_back">
<div class="kr1b4-image _mw-overlay">
<img src="ftp-management.svg" alt="Best Web Hosting India">
</div>
<div class="kr1b4-title">FTP Management</div>
</div>
</div>
</div>
<div class="kr1b4-box">
<div class="kr1b4-box-body">
<div class="">
<div class="kr1b4-image _mw-overlay">
<img src="file-manager.svg" alt="Best Web Hosting Providers">
</div>
<div class="kr1b4-title">File Manager</div>
</div>
<div class="kr1b4-box_back">
<div class="kr1b4-image _mw-overlay">
<img src="dns-management.svg" alt="Best Web Hosting Service">
</div>
<div class="kr1b4-title">DNS Management</div>
</div>
</div>
</div>
<div class="kr1b4-box">
<div class="kr1b4-box-body">
<div class="">
<div class="kr1b4-image _mw-overlay">
<img src="domain-setup.svg" alt="Web Hosting Plans India">
</div>
<div class="kr1b4-title">Domain Setup</div>
</div>
<div class="kr1b4-box_back">
<div class="kr1b4-image _mw-overlay">
<img src="sub-domain-manage.svg" alt="Website Hosting Plan India">
</div>
<div class="kr1b4-title">Manage Sub Domain</div>
</div>
</div>
</div>
<div class="kr1b4-box">
<div class="kr1b4-box-body">
<div class="">
<div class="kr1b4-image _mw-overlay">
<img src="cron-jobs.svg" alt="Web Hosting Plans">
</div>
<div class="kr1b4-title">Cron Jobs</div>
</div>
<div class="kr1b4-box_back">
<div class="kr1b4-image _mw-overlay">
<img src="auto-responders.svg" alt="Best Cheap Hosting">
</div>
<div class="kr1b4-title">Auto Responders</div>
</div>
</div>
</div>
</div>
<div class="kr0b3-mncol kr0b3-mncol-feats" data-kr0b3-carousel="550">
<div class="kr9b8-r0w">
<div class="kr9b8-mncol kr9b8-mncol-one">
<div class="kr9b8-icon _mw-overlay">
<img src="https://www.milesweb.in/img-assets/hosting-offers/best-web-hosting/icon12-super-easy.svg" alt="Best Web Hosting Plans">
</div>
<div class="kr9b8-title">Super-Easy</div>
</div>
<div class="kr9b8-mncol kr9b8-mncol-two">
<p>Get a control panel that is easy to use as it requires no certain experience in hosting, domain &amp; email management.</p>
</div>
</div>
<div class="kr9b8-r0w">
<div class="kr9b8-mncol kr9b8-mncol-one">
<div class="kr9b8-icon _mw-overlay">
<img src="https://www.milesweb.in/img-assets/hosting-offers/best-web-hosting/icon12-robust.svg" alt="Best Web Hosting Provider">
</div>
<div class="kr9b8-title">Powerful</div>
</div>
<div class="kr9b8-mncol kr9b8-mncol-two">
<p>We offer you the dual-shield security for a smooth, affordable and secure hosting service. Dual-shield firewall enables to block intruders without even knowing them.</p>
</div>
</div>
<div class="kr9b8-r0w">
<div class="kr9b8-mncol kr9b8-mncol-one">
<div class="kr9b8-icon _mw-overlay">
<img src="https://www.milesweb.in/img-assets/hosting-offers/best-web-hosting/icon12-one-click-install.svg" alt="Best Web Hosting Service Provider">
</div>
<div class="kr9b8-title">24x7 Support</div>
</div>
<div class="kr9b8-mncol kr9b8-mncol-two">
<p>While you manage your site, we ensure that you have the best web hosting experience with cPanel. You can call, chat, raise a ticket or even email for resolving your queries.</p>
</div>
</div>
</div>
</div>
</div>


<div class="container my-5">
  <div class="text-center mb-5">
    
    <h1 class="text-capitalize font-weight-bold">Frequently Asked <span style="color: #1f6599;">Questions</span></h1>
  </div>

  <div class="row">
    
    <!-- 
      div:hover {
        background-color: #9B5DE5;
      }
     -->

    <div class="col-md-6">
      <div class="my-3 p-4 bg-light">
        <div class="d-flex align-items-start">
          <div class="mr-3 bg-dark text-white rounded-circle">
<i class="fa fa-question-circle" aria-hidden="true"></i>

          </div>
          <div class="">
            <a href="#" class="text-dark stretched-link"><h6 class="mb-3" style="font-weight: 600; font-size: 1.1rem;">What is web hosting?</h6></a>
            <p class="text-secondary" style="line-height: 2;">In simple words, Web Hosting is the process of placing your website on the web servers. These servers are specifically designed to handle the traffic load and uptime of your website. Web hosting companies like YouStable makes sure that your website goes live on the Internet without any downtime and with the fastest speed possible.</p>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="my-3 p-4 bg-light">
        <div class="d-flex align-items-start">
          <div class="mr-3 bg-dark text-white rounded-circle">
<i class="fa fa-question-circle" aria-hidden="true"></i>

          </div>
          <div class="">
            <a href="#" class="text-dark stretched-link"><h6 class="mb-3" style="font-weight: 600; font-size: 1.1rem;">Which web hosting should I choose?
</h6></a>
            <p class="text-secondary" style="line-height: 2;">For the best web hosting services, we prefer our customers to assess the website needs.
For starters like blogging and small businesses, we recommend you to purchase the DA Start or YouStart plan.
If you want to expand the business or blogging website, we will prefer you going for YouProfessional or DA Professional plan.
Further, for powerhouse websites, we prefer you going for the You Elite or the DA Elite plan.</p>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="my-3 p-4 bg-light">
        <div class="d-flex align-items-start">
          <div class="mr-3 bg-dark text-white rounded-circle">
<i class="fa fa-question-circle" aria-hidden="true"></i>

          </div>
          <div class="">
            <a href="#" class="text-dark stretched-link"><h6 class="mb-3" style="font-weight: 600; font-size: 1.1rem;">What is the meaning of SEO hosting? </h6></a>
            <p class="text-secondary" style="line-height: 2;">YouStable provides you the SEO Hosting plans, in which we optimise our servers for the best SEO Hosting services.
For example, we host your website simultaneously on multiple servers. It makes sure your site visitor goes live on the nearest server for a faster and smoother experience.
Moreover, if your site goes down on one server, your visitor gets transferred onto the next nearest hosting server. Thus, your site never goes down ensuring that traffic keeps coming from it.</p>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="my-3 p-4 bg-light">
        <div class="d-flex align-items-start">
          <div class="mr-3 bg-dark text-white rounded-circle">
<i class="fa fa-question-circle" aria-hidden="true"></i>

          </div>
          <div class="">
            <a href="#" class="text-dark stretched-link"><h6 class="mb-3" style="font-weight: 600; font-size: 1.1rem;">Does hosting affect SEO? </h6></a>
            <p class="text-secondary" style="line-height: 2;">YES, YouStable web hosting affects your SEO positively.
The biggest game-changer of YouStable web hosting is its superfast web hosting and 99.9% uptime guarantee.
By getting them, you already know that your site will rank above every competitor.</p>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>








<!-- <script type="b480f5482bacb4a692933e4a-text/javascript">
	
	if($(window).width() <= 600){
		$(window).bind('scroll', function () {
			if ($(window).scrollTop() > 300) { $('.fbtm-mncntnr').slideDown(); }
			else { $('.fbtm-mncntnr').slideUp(); }
		});
	}
</script> -->
<script type="b480f5482bacb4a692933e4a-text/javascript">
	$(window).bind('scroll', function () {
		if ($(window).scrollTop() > 1 ) {
			$('.igfm-mncntnr').addClass('igfm-mncntnr-scrolled');
		}
		else{
			$('.igfm-mncntnr').removeClass('igfm-mncntnr-scrolled');
		}
	});
	
	
	$('.igfm-nav-ham').click(function(){
		if ($(this).hasClass('igfm-nav-ham-active')) {
			$('.igfm-nav-ham').removeClass('igfm-nav-ham-active');
			$('.spectr-mncntnr').removeClass('spectr-mncntnr-active');
			$('body').css({'height':'auto','overflow':'auto','overflowX':'hidden'});
		}
		else if (!$(this).hasClass('igfm-nav-ham-active')) {
			$('.igfm-nav-ham').addClass('igfm-nav-ham-active');
			$('.spectr-mncntnr').addClass('spectr-mncntnr-active');
			$('body').css({'height':'100vh','overflow':'hidden'});
		}
	});

	$('.igfm-hamb').click(function(){
		$('.igfm-nav-ham').addClass('igfm-nav-ham-active');
		$('.spectr-mncntnr').addClass('spectr-mncntnr-active');
		$('body').css({'height':'100vh','overflow':'hidden'});
	});
	$('.spectr-close').click(function(){
		$('.igfm-nav-ham').removeClass('igfm-nav-ham-active');
		$('.spectr-mncntnr').removeClass('spectr-mncntnr-active');
		$('body').css({'height':'auto','overflow':'auto','overflowX':'hidden'});
	});

	$('.skyfl-box').slideUp(0);

	$('.skyfl-wrapper').click(function(){
		if ($(this).hasClass('skyfl-wrapper-link')) { }	
		else if (!$(this).hasClass('skyfl-wrapper-active')) {
		$('.skyfl-wrapper').not(this).slideUp(300);
		$('.skyfl-wrapper').removeClass('skyfl-wrapper-active');
		$(this).addClass('skyfl-wrapper-active');
		$(this).slideDown(300);
		$('.spectr-goback').addClass('spectr-goback-active');
		$('.skyfl-box').slideUp(300);
		$(this).children('.skyfl-box').delay(600).slideDown(500);
		}
	});

	$('.spectr-goback').click(function(){
		$(this).removeClass('spectr-goback-active');
		$('.skyfl-wrapper').removeClass('skyfl-wrapper-active');
		$('.skyfl-wrapper').delay(300).slideDown(300);
		$('.skyfl-box').slideUp(500);
	});

	$( document ).on( 'keydown', function ( e ) {
		if ( e.keyCode === 27 ) { // ESC key
			$('.spectr-mncntnr').removeClass('spectr-mncntnr-active');
			$('.igfm-nav-ham').removeClass('igfm-nav-ham-active');
		}
	});
	
	$(document).click(function(event) { 
	  $target = $(event.target);
	  if( !$target.closest('.spectr-mncntnr').length && !$target.closest('.igfm-hamb').length && !$target.closest('.igfm-nav-ham-active').length ) {
			$('.spectr-mncntnr').removeClass('spectr-mncntnr-active');
			$('.igfm-nav-ham').removeClass('igfm-nav-ham-active');
			$('.igfm-nav-ham').removeClass('igfm-nav-ham-active');
			$('.spectr-mncntnr').removeClass('spectr-mncntnr-active');
			$('body').css({'height':'auto','overflow':'auto','overflowX':'hidden'});
	  }
	});
	
	//var heightRevStories = $('#customerStories').offset().top - 450;
	$(window).bind('scroll', function () {
		if ($(window).scrollTop() > 20 ) {
			$('.strv9-avatar-box img').each(function(){
				var valOne = $(this).attr('title');
				$(this).attr('src', valOne);
				$(this).removeAttr('title');
			});
		}
	}); 
	jQuery('.strv9-mncntnr').slick({
		autoplay:true,autoplaySpeed:5000,arrows:true,
		slidesToShow: 1,slidesToScroll: 1,
		prevArrow: $('.strv9-nav-prev'),nextArrow: $('.strv9-nav-next'),
	});

</script>
<script type="b480f5482bacb4a692933e4a-text/javascript">
	$('.gloot-cta').click(function(){
		$('.aprl-mncntnr').toggle(300);
	});
	$('.aprl-close').click(function(){
		$('.aprl-mncntnr').hide(300);
	})
	$( document ).on( 'keydown', function ( e ) {
		if ( e.keyCode === 27 ) { // ESC key
			$('.aprl-mncntnr').hide(300);
		}
	});
</script>
<script type="b480f5482bacb4a692933e4a-text/javascript">
	function gatfkRemovUp(){
		$('.gatfk-lnk-cntnr').removeClass('gatfk-lnk-cntnr-active');
		$('.satfl-mncntnr').slideUp(200);
	}
	$(document).click(function(event) { 
	  $target = $(event.target);
	  if(!$target.closest('.gatfk-lnk-cntnr').length && 
	  $('.gatfk-lnk-cntnr').is(':visible')) { gatfkRemovUp(); }
	});
	$(document).on('keydown', function(event) {
	   if (event.key == 'Escape') { gatfkRemovUp(); }
	});

	$('.gatfk-lnk-cntnr').click(function(){
		 $('.gatfk-lnk-cntnr').not(this).each(function(){
			 $(this).removeClass('gatfk-lnk-cntnr-active');
			$(this).siblings('.satfl-mncntnr').slideUp(200);
		 });
		$(this).toggleClass('gatfk-lnk-cntnr-active');
		$(this).siblings('.satfl-mncntnr').slideToggle(200);
	});

	$('.satfl-row').click(function(){
		$disL_dur = $(this).children('.p1-dur').html();
		$disL_striked = $(this).children('.p1-prc-strik').children('div').html();
		$disL_discounted = $(this).children('.p1-prc-fnl').children('div').html();
		$disL_save = $(this).children('.p1-save').html();
		$(this).closest('.gatfk-mncntnr').find('.s1-save-wrapr').html($disL_save);
		if($disL_save==''){
			$(this).closest('.gatfk-mncntnr').find('.s1-save-wrapr').hide();
		}else{
			$(this).closest('.gatfk-mncntnr').find('.s1-save-wrapr').show();
		}
		$(this).closest('.gatfk-mncntnr').find('.kl_price_duration-free-trial').removeClass('kl_price_duration-free-trial');
		$(this).closest('.gatfk-mncntnr').find('.s1-prc-fnl-free-trial').removeClass('s1-prc-fnl-free-trial');
		$(this).closest('.gatfk-mncntnr').find('.s1-prc-strik-free-trial').removeClass('s1-prc-strik-free-trial');
		
		$(this).closest('.gatfk-mncntnr').find('.kl_price_duration').html($disL_dur);
		$(this).closest('.gatfk-mncntnr').find('.kl_price_sriked').html($disL_striked);
		if($disL_striked==''){
			$(this).closest('.gatfk-mncntnr').find('.s1-prc-strik').hide();
		}else{
			$(this).closest('.gatfk-mncntnr').find('.s1-prc-strik').show();
		}
		$(this).closest('.gatfk-mncntnr').find('.kl_price_discounted').html($disL_discounted);
		//billing cycle
		var disBtnBilCycle = $(this).attr('data-ultn-bilcycle');
		if( $(this).closest('.gatfk-mncntnr').siblings('.ultwn-btn-wrapr').children('.ultwn-btn').hasClass('ultwn-btn-free-trial') ){
			var disBtnUrlFetch = $(this).closest('.gatfk-mncntnr').siblings('.ultwn-btn-wrapr').children('.ultwn-btn').attr('data-frtrl-main-url');
			$(this).closest('.gatfk-mncntnr').siblings('.ultwn-btn-wrapr').children('.ultwn-btn').html( $(this).closest('.gatfk-mncntnr').siblings('.ultwn-btn-wrapr').children('.ultwn-btn').attr('data-frtrl-main-text') );
			$(this).closest('.gatfk-mncntnr').siblings('.ultwn-btn-wrapr').children('.ultwn-btn').removeClass('ultwn-btn-free-trial');
		}else{
			var disBtnUrlFetch = $(this).closest('.gatfk-mncntnr').siblings('.ultwn-btn-wrapr').children('.ultwn-btn').attr('href');
		}
		var disBtnUrlRemv = disBtnUrlFetch.indexOf('&billingcycle');
		var disBtnUrlFinal = disBtnUrlFetch.substring(0, disBtnUrlRemv);
		var disBtnUrlFinal = disBtnUrlFinal+'&billingcycle='+disBtnBilCycle;
		var disAnjlBtnTarget = $(this).closest('.gatfk-mncntnr').siblings('.ultwn-btn-wrapr').children('.ultwn-btn').attr('data-utm-extend');
		if( disAnjlBtnTarget !== undefined ){ var utmExtend = disAnjlBtnTarget; disBtnUrlFinal = disBtnUrlFinal+utmExtend; }
		$(this).closest('.gatfk-mncntnr').siblings('.ultwn-btn-wrapr').children('.ultwn-btn').attr('href',disBtnUrlFinal);
	});
	
	$('.satfl-row-free-trial').click(function(){
		var header_text = $(this).children('.satfl-frtrl-hdr').html();
		var main_text = $(this).children('.satfl-frtrl-mn').html();
		
		$(this).closest('.gatfk-mncntnr').find('.kl_price_duration').addClass('kl_price_duration-free-trial');
		$(this).closest('.gatfk-mncntnr').find('.s1-prc-fnl').addClass('s1-prc-fnl-free-trial');
		$(this).closest('.gatfk-mncntnr').find('.s1-prc-strik').addClass('s1-prc-strik-free-trial');
		
		$(this).closest('.gatfk-mncntnr').find('.kl_price_duration').html('');
		$(this).closest('.gatfk-mncntnr').find('.kl_price_sriked').html(header_text);
		$(this).closest('.gatfk-mncntnr').find('.kl_price_discounted').html(main_text);
		
		$(this).closest('.gatfk-mncntnr').siblings('.ultwn-btn-wrapr').children('.ultwn-btn').addClass('ultwn-btn-free-trial');
		
		var disBtnUrlFinal = $(this).closest('.gatfk-mncntnr').siblings('.ultwn-btn-wrapr').children('.ultwn-btn').attr('data-btn-free-trial-url');
		
		$(this).closest('.gatfk-mncntnr').siblings('.ultwn-btn-wrapr').children('.ultwn-btn').html( $(this).closest('.gatfk-mncntnr').siblings('.ultwn-btn-wrapr').children('.ultwn-btn').attr('data-btn-free-trial-text') );
		$(this).closest('.gatfk-mncntnr').siblings('.ultwn-btn-wrapr').children('.ultwn-btn').attr('href',disBtnUrlFinal);
		
	});
</script>
<script type="b480f5482bacb4a692933e4a-text/javascript">
	$('[data-tbodyhid-tr]').siblings().hide();
	$(document).on('click', '[data-tbodyhid-tr]', function(e) {
		$(this).siblings().slideToggle(300);
		$(this).toggleClass('data-tbodyhid-tr-active');
	});
</script>
<script type="b480f5482bacb4a692933e4a-text/javascript">
		jQuery('.maoy-mnslider').slick({
			autoplay:true,autoplaySpeed:5000,arrows:false,dots: true,
			slidesToShow: 1,slidesToScroll: 1,
		});
		
		var a = 0;
		$(window).scroll(function() {
		  var oTop = $('#countCustomersTrust').offset().top - window.innerHeight;
		  if (a == 0 && $(window).scrollTop() > oTop) {
			$('#countCustomersTrust').each(function() {
			  var $this = $(this), countTo = $this.attr('data-count');
			  $({
				countNum: $this.text()
			  }).animate({ countNum: countTo },
				{
				  duration: 2000, easing: 'swing',
				  step: function() {
					$this.text(Math.floor(this.countNum));
				  },
				  complete: function() { $this.text(this.countNum); }
				});
			});
			a = 1;
		  }
		});
	</script>


<script type="b480f5482bacb4a692933e4a-text/javascript">
		if($(window).width() > 600){
			$(document).on('mouseover', '.riod-box', function(e) {	
				$('.riod-box').removeClass('riod-box-active');
				$(this).addClass('riod-box-active');
				$('.riod-screen-title').html( $('.riod-box-active .riod-title').html() );
				$('.riod-screen-info').html( $('.riod-box-active .riod-info').html() );
			});
			$('.riod-box').click(function(){
				var dis_riod_url = $(this).children('.riod-button').children('a').attr('href');
				document.location.href = 'https://www.youstable.in/'+dis_riod_url;
			});
		}
		if($(window).width() <= 600){
			$('.riod-box').click(function(){
				$('.riod-box').removeClass('riod-box-active');
				$(this).addClass('riod-box-active');
				$('.riod-info').slideUp(200);
				$('.riod-button').slideUp(200);
				$('.riod-box-active .riod-info').slideDown(300);
				$('.riod-box-active .riod-button').slideDown(300);
			});
		}
	</script>
<script type="b480f5482bacb4a692933e4a-text/javascript">
		$('.popstr-clos-btn').click(function(){
			$('.popstr-mncntnr').fadeOut(100);
			$('body').css({'height':'auto','overflow':'auto'});
		});
		$(document).on('click', '[data-indpopr]', function(e) {
			var disIndpoprClass = $(this).attr('data-indpopr');
			$('.'+disIndpoprClass+'').fadeIn(200);
			$('body').css({'height':'100vh','overflow':'hidden'});
		});
	</script>






<!-- <script src="cdn.jsdelivr.net/npm/typeit%407.0.4/dist/typeit.min.js" type="b480f5482bacb4a692933e4a-text/javascript"></script>
<script type="b480f5482bacb4a692933e4a-text/javascript">
		new TypeIt("#element", {
		speed: 50,
		loop: false
		}).go();
</script> -->
<div class="tlp0p8-window">
<div class="tlp0p8-mnrntnr">
<div class="tlp0p8-mnwrapr">
<div class="tlp0p8-close" data-t8p0p-close-btn="CLOSE"><b></b><b></b></div>
<div class="tlp0p8-title"></div>
<div class="tlp0p8-txt"></div>
</div>
</div>
</div>
<script type="b480f5482bacb4a692933e4a-text/javascript">
		$(document).on('click', '[data-t8p0p-popup]', function(e) {
			if( $(this).attr('data-t8p0p-popup') == "YES" ){
				var dis_t8p0p_title = $(this).attr('data-t8p0p-title');
				var dis_t8p0p_text = $(this).attr('data-t8p0p-content');
				$('.tlp0p8-window').addClass('tlp0p8-window_active');
				$('.tlp0p8-window').slideDown(200);
				$('.tlp0p8-title').html(dis_t8p0p_title);
				$('.tlp0p8-txt').html(dis_t8p0p_text);
				
				$('html').css({'overflow-y':'hidden'});
			}
		});
		
		$(document).on('click', '[data-t8p0p-close-btn]', function(e) {
			t8p0pClose();
		});
		$(document).keyup(function(e) {
			if( e.key === "Escape" && $('.tlp0p8-window').hasClass('tlp0p8-window_active') ) {
				t8p0pClose();
			}
		});
		function t8p0pClose(){
			$('.tlp0p8-window').slideUp(200);
			$('.tlp0p8-window').removeClass('tlp0p8-window_active');
			$('html').css({'overflow-y':'visible'});
			
		}
</script>

<!-- <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script> -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<style>
@media (max-width: 320px) {
 .abh1r-mncol-graphic {
margin-top:24px;
}
.abh1r-btn-pr1m{
    padding: 15px 24px;
}
.sknva-cntnr{
    display:none;
}
.abh1r-image{
    display:none;
}
.ultrn-mncol{
	width: 640px;
}
.slick-track{
width: 640px;
}
.trplt-mncntnr{
	background: #f9ca0e;
}
.igfm-logo>img{
	margin-top: auto!important;
	margin-right: auto!important;
}
#desktop{
 	display: none;
 }
 .igfm-wrapper {
 	display: block;
    width: 80%;
 }
}
@media (min-width: 321px ) and (max-width: 375px) {
 .abh1r-mncol-graphic {
margin-top:24px;
}
.abh1r-btn-pr1m{
    padding: 15px 24px;
}
.sknva-cntnr{
    display:none!important;
}
.abh1r-image{
    display:none;
}
.ultrn-mncol{
	width: 640px;
}
.slick-track{
width: 640px;
}
.trplt-mncntnr{
	background: #f9ca0e;
}
.igfm-logo>img{
	margin-top: auto!important;
	margin-right: auto!important;
}
/*#mobile{
 	display: none;
 } */
 #desktop{
 	display: none;
 }
 .igfm-wrapper {
 	display: block;
    width: 80%;
 }
}

@media (min-width: 376px ) and (max-width: 425px) {
 .abh1r-mncol-graphic {
margin-top:24px;
}
.abh1r-btn-pr1m{
    padding: 15px 24px;
}
.sknva-cntnr{
    display:none;
}
.abh1r-image{
    display:none;
}
.ultrn-mncol{
	width: 640px;
}
.slick-track{
width: 640px;
}
.trplt-mncntnr{
	background: #f9ca0e;
}
.igfm-logo>img{
	margin-top: auto!important;
	margin-right: auto!important;
}
#desktop{
 	display: none;
 } 
 .igfm-wrapper {
 	display: block;
    width: 60%;
 }
}

@media (min-width: 426px ) and (max-width: 768px) {
  .abh1r-mncol-graphic {
    margin-top: 41px;
    text-align: right;
    
}
.abh1r-image{
    display:none;
}
.ultrn-mncol{
	width: 640px;
}
.trplt-mncntnr{
	background: #f9ca0e;
}
#desktop{
 	display: none;
 } 
 .igfm-wrapper {
 	display: block;
    width: 60%;
 }
}

@media (min-width: 769px ) and (max-width: 1024px) {
 .abh1r-mncol-graphic {
    margin-top: 41px;
    text-align: right;
    margin-left: -22px;
} 
#desktop{
 	display: none;
 } 
 .igfm-wrapper {
 	display: block;
    width: 40%;
 }
}

@media (min-width: 1025px ) and (max-width: 1440px) {
  #mobile{
 	display: none;
 } 
.text-h{
 	font-size: 32px;
 }
}

@media (min-width: 1441px ) and (max-width: 2560px) {
  #mobile{
 	display: none;
 } 
 .text-h{
 	font-size: 32px;
 }
}

@media (min-width: 2561px ) {
 #mobile{
 	display: none;
 } 
 .text-h{
 	font-size: 32px;
 }
}
</style>



</body>

</html>