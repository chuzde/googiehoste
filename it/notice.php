<!doctype html>
<html class="no-js" xmlns="http://www.w3.org/1999/xhtml">
  <head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<meta http-equiv="Content-Language" content="it" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <title>La tua nota DMCA è stata generata</title>
<meta name="description" content="Se qualcuno ha copiato il tuo articolo o qualsiasi proprietà digitale, questo strumento ti aiuterà a generare un avviso DMCA e a prendere nota la tua proprietà dal web.">
<meta name="Keywords" CONTENT="DMCA, DMCA free, materiale soggetto a copyright di rimozione, DMCA Notice Generator">
<meta name="author" content="GoogieHost.com">
<script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"Organization","name":"GoogieHost","legalName":"GoogieHost","foundingDate":"2012","logo":"https:\/\/googiehost.com\/images\/logo.png","image":"https:\/\/googiehost.com\/images\/logo.png","url":"https:\/\/googiehost.com","address":{"@type":"PostalAddress","streetAddress":"4/453, Vibhav Khand, Gomti Nagar, Lucknow ","addressLocality":"Uttar Pradesh","postalCode":"226010","addressCountry":"India"},"contactPoint":{"@type":"ContactPoint","telephone":"+91-9616782253","areaServed":"IN","availableLanguage":"English","email":"mailto:support@googiehost.com","contactType":"customer service"},"sameAs":["https:\/\/www.facebook.com\/GoogieHost","https:\/\/plus.google.com\/+GoogiehostFreeHosting","https:\/\/twitter.com\/GoogieHost","https:\/\/www.youtube.com\/user\/GoogieHostFree"]}</script>
<script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"WebSite","name":"GoogieHost","alternateName":"YouStable Technologies Pvt Ltd","url":"https:\/\/googiehost.com","image":{"@type":"ImageObject","url":"https:\/\/googiehost.com\/images\/logo.png"},"sameAs":["https:\/\/www.facebook.com\/GoogieHost","https:\/\/plus.google.com\/+GoogiehostFreeHosting","https:\/\/twitter.com\/GoogieHost","https:\/\/www.youtube.com\/user\/GoogieHostFree"]}</script>
<script type="application/ld+json">
        {"@context":"http:\/\/schema.org\/","@type":"Product","name":"Your DMCA Notice has been generated","description":"Se qualcuno ha copiato il tuo articolo o qualsiasi proprietà digitale, questo strumento ti aiuterà a generare un avviso DMCA e a prendere nota la tua proprietà dal web.","brand":{"@type":"Brand","name":"GoogieHost"},"aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "4.7",
    "reviewCount": "832"
    },"offers": {
        "@type": "AggregateOffer",
        "priceCurrency": "USD",
        "lowprice": "0",                   
        "seller": {
      "@type": "Organization",
      "name": "GoogieHost"
    }
      } }
    </script>      
<link rel="image_src" href="http://www.googiehost.com/thumb.jpg" />
    <link rel="shortcut icon" href="images/icons/favicon.png" />
     <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title" content="CHANGE-ME">
  <link rel="apple-touch-icon" href="images/icons/touch-icon-iphone.png">
  <link rel="apple-touch-icon" sizes="76x76" href="images/icons/touch-icon-ipad.png">
  <link rel="apple-touch-icon" sizes="120x120" href="images/icons/touch-icon-iphone-retina.png">
  <link rel="apple-touch-icon" sizes="152x152" href="images/icons/touch-icon-ipad-retina.png"> 
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/animate.min.css" />
    <link rel="stylesheet" href="css/morphext.css" />
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <link rel="stylesheet" href="css/slicknav.css">
    <link rel="stylesheet" href="style.css" />
    <link rel="stylesheet" href="css/premium.css">    
    <script src="js/vendor/modernizr.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-54079021-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-54079021-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"nQZkh1aMQV00G7", domain:"googiehost.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="images/atrk.gif" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->
<style>
@media (min-width: 1281px) {
.tablink {
    background-color: white;
    color: black;
    float: center;
    border:1px solid black;
    outline: none;
    cursor: pointer;
    padding: 10px 10px;
    font-size: 17px;
}
.tablink:hover {color:white;}
.tablink:hover .tablink{color:black;}
.tabcontent {
    color: black;
    display: none;
    padding: 10px 20px;
    height: 100%;border:1px solid black;}}

@media (min-width: 1025px) and (max-width: 1280px) {
  .tablink {
    background-color: white;
    color: black;
    float: center;
    border:1px solid black;
    outline: none;
    cursor: pointer;
    padding: 10px 10px;
    font-size: 17px;
    width:10%;}
.tablink:hover {color:white;}
.tablink:hover .tablink{color:black;}
.tabcontent {
    color: black;
    display: none;
    padding: 10px 20px;
    height: 100%;border:1px solid black;}}
@media (min-width: 768px) and (max-width: 1024px) {
.tablink {
    background-color: white;
    color: black;
    float: center;
    border:1px solid black;
    outline: none;
    cursor: pointer;
    padding: 10px 10px;
    font-size: 17px;
    width:15%;}
.tablink:hover {color:white;}
.tablink:hover .tablink{color:black;}
.tabcontent {
    color: black;
    display: none;
    padding: 10px 20px;
    height: 100%;border:1px solid black;}}
@media (min-width: 481px) and (max-width: 767px) {
.tablink {
    background-color: white;
    color: black;
    float: center;
    border:1px solid black;
    outline: none;
    cursor: pointer;
    padding: 10px 10px;
    font-size: 17px;}
.tablink:hover {color:white;}
.tablink:hover .tablink{ color:black;}
.tabcontent {
    color: black;
    display: none;
    padding: 10px 20px;
    height: 100%;border:1px solid black;}}
@media (min-width: 100px) and (max-width: 480px) { 
.tablink {
    background-color: white;
    color: black;
    float: center;
    border:1px solid black;
    outline: none;
    cursor: pointer;
    padding: 10px 10px;
    font-size: 17px;}
.tablink:hover {color:white;}
.tablink:hover .tablink{color:black;}
.tabcontent {
    color: black;
    display: none;
    padding: 10px 20px;
    height: 100%;border:1px solid black;}}
    #text{font-size:16px}
#text li{margin-bottom:10px;}
    #text li dl dt{font-size:16px;font-weight:600}
     #text li dl dd{font-size:16px;}
#styled_text {background-color: white;}
#html {background-color: white;}</style>
  </head>
   <body>
<!--  HEADER -->
<header class="alt-2 header5">
<div class="top">
  <div class="row">
  <div class="small-12 large-3 medium-3 columns">
   <div class="logo">
   <a href="index.html" title=""><img src="images/logo.png" alt="logo" title="Free Domain name registration"/></a>
   </div>
</div>
<div class="small-12 large-9 medium-9 columns">
      <nav class=desktop-menu>
<ul class=sf-menu>
<li><a href=freehosting.html>Hosting gratuito</a></li>
<li>
<span class="nav-top-txt nav-top-red"><span>
99% di sconto</span></span>
<a href="https://youstable.com/manage/cart.php?a=add&pid=21&promocode=POWERUP">Pagato Hosting</a></li>
<li class="dropdown">
<span class="nav-top-txt nav-top-red earn"><span>Risparmia $ 100</span></span><a href="#">Coupon di hosting <i class="fa fa-chevron-down"></i></a>
 <div class="dropdown-content">
      <a href="https://googiehost.com/web-hosting-sale-coupons.html">Web Hosting</a>
      <a href="https://googiehost.com/cheap-dedicated-server.html">Server dedicato</a>
      <a href="https://googiehost.com/cheap-vps-hosting.html">Hosting VPS</a>
    </div>
    </li>
<li><a href="https://client.googiehost.com/clientarea.php?language=italian">Login</a></li>
<li><a href=signup.html>Iscriviti</a></li>
<li><a href=support.html>Supporto</a></li>
</ul>
</nav>
<nav class=mobile-menu>
<ul class="mobile-menu1">
<li><a href=freehosting.html>Hosting gratuito</a></li>
<li><a href="https://youstable.com/manage/cart.php?a=add&pid=21&promocode=POWERUP">Pagato Hosting <span class="paid">
99% di sconto</span></a> </li>
 <li class="dropdown menu-item1"><a class="dropbtn">Coupon di hosting <i class="fa fa-chevron-down"> </i><span>Risparmia $ 100</span></a>
      <div class="dropdown-content">
     <a href="https://googiehost.com/web-hosting-sale-coupons.html">Web Hosting</a>
      <a href="https://googiehost.com/cheap-dedicated-server.html">Server dedicato</a>
      <a href="https://googiehost.com/cheap-vps-hosting.html">Hosting VPS</a>
    </div></li>
<li><a href="https://client.googiehost.com/clientarea.php?language=italian">Login</a></li>
<li><a href=signup.html>Iscriviti</a></li>
<li><a href=support.html>Supporto</a></li>
</ul>
</nav>
  <!--  END OF MOBILE MENU AREA -->
  </div>
  </div>
  </div>
  <div class="content-block1 quote-box text-center message" style="padding:0">
<div class="container box">
<div class="row"><!--<div class="columns large-1 hide-for-small-only hide-for-medium-only"><span style="opacity:0">testing</span></div> -->
<div class="columns large-8 medium-9 med small-12"> <h1 class="text-xs-center text-md-left">Web Hosting  </h1><p class="hide1">Offriamo cPanel Web Hosting Fornito da:</p><br>
<div class="row">
<div class="columns small-12 large-6">
<ul  style="list-style-type:none;"><li><i class="fa fa-check" aria-hidden="true"></i> Unità SSD e LiteSpeed</li><li><i class="fa fa-check" aria-hidden="true"></i> Certificato SSL Gratuito</li><li><i class="fa fa-check" aria-hidden="true"></i> CDN CloudFlare gratuito</li></ul>
</div>
<div class="columns small-12 large-6">
<ul  style="list-style-type:none;"><li><i class="fa fa-check" aria-hidden="true"></i> Uptime del server al 99,95%</li><li><i class="fa fa-check" aria-hidden="true"></i> Potente Backbone Hardware</li><li><i class="fa fa-check" aria-hidden="true"></i> Supporto chat e ticket 24x7</li></ul>
</div>
  </div>
 <br/> 
</div>
<div class="columns large-4 medium-3  med1 small-12">
  <div class="offer">
  <center> <div ><span class="savings">POWERUP</span></div> 
    <div class="arrow"></div>
    <div class="price">
<p><span class="price4">
  <span class="sign">$</span><span class="int" style="font-size:120px;vertical-align:top;">0</span><span class="sub"><span class="float">.18</span><span class="terms">/month</span></span>
</span></p>
<center><a class="custom button button--moema button--inverted button--text-thick button--size-s" style="width:220px;text-align:center;margin-bottom:35px" href="https://youstable.com/manage/cart.php?a=add&pid=21&promocode=POWERUP">Ordina adesso</a></center>
  </div>
  </center>
  </div>
</div>
  </div>
  </div>
</div>
</header>
<!--  END OF HEADER -->
<section  >
<div class="content" style="padding: 30px 0 65px 0;position: relative;">
    <div class="row">
<div class="small-12 medium-12 large-12 columns">
       <div style="text-align:center"><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- pages -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3431396059777650"
     data-ad-slot="9163787323"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
</div>
</div><script src="js/ads.js" type="text/javascript"></script>
<div class="row">
  <div class="columns large-1 medium-1 hide-for-small-only"><span style="opacity:0">test</span></div>
<div class="columns large-10 medium-10 small-12"><br/><div id="banner-12346">
   <a href="disable-adblock.html" style="color:#333;text-decoration:none"><b>Disable Ad Block!!</b></a>&nbsp;&nbsp;Comprendiamo che odi gli annunci, ma questa è l'unica opzione per recuperare il costo del nostro server. Ti chiediamo di disattivare il blocco degli annunci per mantenere attivo questo servizio.</div></div>
<div class="columns large-1 medium-1 hide-for-small-only"></div></div>
<script type="text/javascript">
    if ( !document.getElementById('test-block') ) {
        document.getElementById('banner-12346').style.display='block';
    }
</script>
<div class="row">
<div class="small-12 columns">
<?php
    if(isset($_POST['SubmitButton'])){
        $name = $_POST["dmca_full_name"];
        $address = $_POST["dmca_address"];
       $phone_no = $_POST["dmca_tel"];
         $email = $_POST["dmca_email"];
           $infringement_name = $_POST["dmca_infringement_name"];
             $original_urls = $_POST["dmca_original_urls"];
               $infringing_urls = $_POST["dmca_infringing_urls"];     
    }
 ?>
        <div id="notice" >
<h2  style="font-size:24px;margin-top:40px;margin-bottom:30px;font-weight:800;color:black">Il tuo avviso DMCA! Invia questo avviso al Dipartimento di abuso della società di hosting</h2>
<div class="generated_content_wrapper styled">
<button class="tablink" onclick="openPage('styled_text ', this, 'gainsboro','black')" id="defaultOpen">Testo in stile</button>
<button class="tablink" onclick="openPage('html', this, 'gainsboro','black')" >HTML</button>
<div id="styled_text " class="tabcontent">
   <h4 style="font-weight:600">Avviso di rimozione ai sensi del Digital Millennium Copyright Act del 1998</h4>
<br/>
<p>Per chi è coinvolto,<br>         
Si tratta di una notice in conformità con il Digital Millennium Copyright Act del 1998 (DMCA) che richiede la cessazione immediata di fornire l'accesso a materiale protetto da copyright. Desidero segnalare un'istanza di violazione del copyright, in cui il materiale illecito viene visualizzato su un sito Web di cui l'utente è l'host.</p>
<ol id="text">
        <li>
          <p style="margin-bottom:0px">Il materiale illecito, che io sostengo appartiene a me, è il seguente:</p>
         <?php echo $infringement_name;?> </li>
        <li>
          <p style="margin-bottom:0px">Il materiale originale si trova sul mio sito web ai seguenti URL:</p>
         <?php echo $original_urls;?>  </li>
        <li>
          <p style="margin-bottom:0px">Il materiale illecito si trova ai seguenti URL:</p>
         <?php echo $infringing_urls;?>  </li>
        <li>
          <p>Le mie informazioni di contatto sono:</p>
          <dl>
            <dt>Nome:</dt>
            <dd>
              <?php echo $name;?>
            </dd>
            <dt>Indirizzo di posta:
</dt>
            <dd> <?php echo $address ;?></dd>
                          <dt>Numero di telefono:</dt>
              <dd> <?php echo $phone_no;?></dd>
                        <dt>Indirizzo email:</dt>
            <dd> <?php echo $email;?></dd>
          </dl>

        </li>
        <li>
          <p>Ritengo in buona fede che l'utilizzo del materiale descritto nel modo in cui è stato denunciato non sia autorizzato dal proprietario del copyright, dal suo agente o dalla legge.</p>
        </li>
        <li>
          <p>Le informazioni contenute in questo avviso sono accurate e io sono il proprietario del copyright o sono autorizzato ad agire per conto del proprietario del copyright.</p>
        </li>
      </ol>
      <p>Dichiaro in base alle leggi sullo spergiuro degli United States of America che questa notifica è vera e corretta.</p>
      <dl>
        <dt style="font-size:16px;font-weight:600">Firmato:</dt>
        <dd style="font-size:16px;"> <?php echo $name;?></dd>
        <dt style="font-size:16px;font-weight:600">Data:</dt>
        <dd style="font-size:16px;"><?php     echo  date("Y-m-d"); ?></dd>
      </dl>
      <hr>

      <p style="font-size:16px">Avvertenza: il contenuto di questo sito Web non è inteso a costituire una consulenza legale e gli utenti del generatore di richieste di rimozione DMCA takedown interamente responsabili delle proprie rivendicazioni sul copyright.</p>
</div>
<div id="html" class="tabcontent generated_content"  contenteditable="true">
 <?php
$str = '<h4>Avviso di rimozione ai sensi del Digital Millennium Copyright Act del 1998</h4>
<br/>
      <p>Per chi è coinvolto,,<br>
        Si tratta di una notice in conformità con il Digital Millennium Copyright Act del 1998 (DMCA) che richiede la cessazione immediata di fornire l accesso a materiale protetto da copyright. Desidero segnalare un istanza di violazione del copyright, in cui il materiale illecito viene visualizzato su un sito Web di cui l utente è l host.</p>
      <ol id="text">
        <li>
          <p style="margin-bottom:0px">Il materiale illecito, che io sostengo appartiene a me, è il seguente:</p>';
echo htmlentities($str);
        echo $infringement_name;
          $str2 =' </li>
       <li>
          <p style="margin-bottom:0px">Il materiale originale si trova sul mio sito web ai seguenti URL:</p>';

echo htmlentities($str2);
  echo $original_urls;
         
           $str3 ='</li>
          <li>
          <p style="{margin-bottom:0px">Il materiale illecito si trova ai seguenti URL:</p>';
      echo htmlentities($str3);
   echo $infringing_urls;
         $str4=' </li>
           <li>
          <p>Le mie informazioni di contatto sono:</p>
          <dl>
            <dt>Nome:</dt>
            <dd> ';
            echo htmlentities($str4);
              echo $name; 
              $str5=' </dd>
            <dt>Indirizzo di posta:</dt>
            <dd>';
            echo htmlentities($str5);
             echo $address ;
             $str6='</dd>
            <dt>Numero di telefono:</dt>
            <dd>';
            echo htmlentities($str6); echo $phone_no; 
            $str7=' </dd>
           <dt>Indirizzo email:</dt>
            <dd>';
            echo htmlentities($str7); echo $email; 
            $str8='</dd>
            </dl> </li>
        <li>
          <p>Ritengo in buona fede che lutilizzo del materiale descritto nel modo in cui è stato denunciato non sia autorizzato dal proprietario del copyright, dal suo agente o dalla legge.</p>
        </li>
        <li>
          <p>Le informazioni contenute in questo avviso sono accurate e io sono il proprietario del copyright o sono autorizzato ad agire per conto del proprietario del copyright.</p>
        </li>
      </ol>
      <p>Dichiaro in base alle leggi sullo spergiuro degli United States of America che questa notifica è vera e corretta.</p>
      <dl>
        <dt>Firmato:</dt>
        <dd>'; echo htmlentities($str8); 
        echo $name;
         $str9='</dd>
        <dt>Data:</dt>
        <dd>'; echo htmlentities($str9);
        echo date("Y-m-d");
        $str10='</dd>
      </dl>
      <hr>
      <p style="font-size:16px">Avvertenza: il contenuto di questo sito Web non è inteso a costituire una consulenza legale e gli utenti del generatore di richieste di rimozione DMCA takedown interamente responsabili delle proprie rivendicazioni sul copyright.</p>';
echo htmlentities($str10);
?>
</div>
</div>
<script>
function openPage(pageName,elmnt,color,color1) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].style.backgroundColor = "";
    }
    document.getElementById(pageName).style.display = "block";
    elmnt.style.backgroundColor = color;
     elmnt.style.color = color1;
}
document.getElementById("defaultOpen").click();
</script>
</div>
</div></div></div></section>
<!--  FOOTER  -->
<footer>
<div class=row>
<div class="small-12 columns">
<div class=footerlinks>
<div class="small-12 large-3 medium-3 columns border-right">
<h2>Chi GoogieHost</h2>
<ul>
<li><a href=about.html>Riguardo a noi</a></li>
<li><a href=team.html>I nostri team Geeks</a></li>
<li><a href=contact.html title="Contact Us">Contattaci Noi</a></li> <li><a href="https://status.googiehost.com" title="Server Uptime" target="_blank">Server Uptime</a></li>
<li><a href=terms.html>Condizioni d'uso</a></li>
<li><a href=privacy.html>Politica Sulla Riservatezza</a></li>
</ul>
</div>
<div class="small-12 large-3 medium-3 columns border-right">
<h2>GoogieHost International</h2>
<ul>
<li><a href=dmca-notice.php>Genera avviso DMCA</a></li>
<li><a href="https://googiehost.com/hi">Hindi</a></li>
<li><a href="https://googiehost.com/es">Español</a></li>
<li><a href="https://googiehost.com/id">Indonesia</a></li>
<li><a href="https://googiehost.com/it">Italiano</a></li>
</ul>
</div>
<div class="small-12 large-3 medium-3 columns border-right">
<h2>Hosting gratuito</h2>
<ul><li><a href="referral.html">Referral</a></li>
<li><a href="freehostingreview.html">Recensioni GoogieHost</a></li>
<li><a href="https://googiehost.com/best-hosting-for-wordpress.html" target="_blank">Miglior hosting per WordPress</a></li>
<li><a href=seo.html>Ottimizzazione del motore di ricerca</a></li>
<li><a href="https://googiehost.com/blog/">Supporto Blog</a></li>
</ul>
</div>
<div class="small-12 large-3 medium-3 columns border-left">
<h2>Perché scegliere GoogieHost?</h2>
<ul>
<li><a href=freephphosting.html>Hosting gratuito di Php</a></li>
<li><a href=freedomains.html>Nome di dominio gratuito</a></li>
<li><a href=freewebsitebuilder.html>Costruttore di siti web gratuito</a></li>
<li><a href=freewordpresshosting.html>Hosting gratuito di WordPress</a></li>
<li><a href=freeautoinstaller.html>Gratuito Auto Installer del sito Web</a></li>
</ul>
</div>
</div>
</div>
</div>
<br/>
<center><a href="https://www.facebook.com/GoogieHost" target="_blank"><img src="../images/fb-icon.png" alt="facebook"></a> <a href="https://www.instagram.com/googiehost/" target="_blank"><img src="../images/instagram.png" alt="instagram"></a> <a href="https://twitter.com/GoogieHost" target="_blank"><img src="../images/twitter-icon.png" alt="twitter"></a> <a href="https://www.youtube.com/user/GoogieHostFree" target="_blank"><img src="../images/youtube.png" alt="youtube"></a></center>
<p class=copyright>© COPYRIGHT 2011-2021 GOOGIEHOST, TUTTI I DIRITTI RISERVATI </p>
<div align=center>
<div style="color:#a4a4a4;">Valutato <span>4.7</span>/5 sulla base di <span>832 </span> recensioni su <a href="https://www.google.co.in/search?q=googiehost&oq=googiehost&aqs=chrome..69i57j69i60l3j69i61.2096j0j1" target=_blank style="color:#a4a4a4;">Google</a></div>
</div>
</footer>
<a href="#top" id=back-to-top><i class="fa fa-angle-up"></i></a>
<script src="js/vendor/jquery.js"></script>
<script src="js/foundation.min.js"></script>
<script src="js/vendor/hoverIntent.js"></script>
<script src="js/vendor/superfish.min.js"></script>
<script src="js/vendor/morphext.min.js"></script>
<script src="js/vendor/wow.min.js"></script>
<script src="js/vendor/jquery.slicknav.min.js"></script>
<script src="js/vendor/waypoints.min.js"></script>
<script src="js/vendor/jquery.animateNumber.min.js"></script>
<script src="js/vendor/owl.carousel.min.js"></script>
<script src="js/vendor/jquery.slicknav.min.js"></script>
<script src="js/custom.js"></script>
</body>
</html>